const initialState = {
	chats: [],
	activeChat:'',
	activeChatTitle:'',
	activeChatMessages:[]	
};

const ChatReducer = (state = initialState, action) => {

  if(action.type == 'setChatList') {
    return {...state, chats:action.payload.chats};
  }  

  if(action.type == 'setActiveChatMessage') {
    return {...state, activeChatMessages:action.payload.activeChatMessages};
  }    

	return state;
}

export default ChatReducer;

