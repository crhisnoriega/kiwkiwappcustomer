import validator from 'validator';

const initialState = {
	name: 'Aline Silveira',
	email: 'aline.silveira@gmail.com',
	user: 'alinesilveira',
	password: '12345678',
	tel: '(11) 98989.0000',
	aniversario:'21/11/1990',
	genero: 'feminino',
	cep: '',
	rua: '',
	numero: 0,
	complemento: '',
	bairro: '',
	cidade: '',
	uf: ''
};

const ConfigReducer = (state = initialState, action) => {
    switch(action.type) {
        case "changeName":
            return {...state, name:action.payload.name};
        case "changeUser":
            return {...state, user:action.payload.user};            
        case "changeEmail":
            return {...state, email:action.payload.email};
        case "changePass":
            return {...state, password:action.payload.password};
        case "changeTel":
            return {...state, tel:action.payload.tel};
        case "changeGenero":
            return {...state, genero:action.payload.genero};
        case "changeCep":
            return {...state, cep:action.payload.cep};
        case "changeRua":
            return {...state, rua:action.payload.rua};
        case "changeNumero":
            return {...state, numero:action.payload.numero};
        case "changeComplemento":
            return {...state, complemento:action.payload.complemento};
        case "changeBio":
            return {...state, bio:action.payload.bio};            
        case "changeBairro":
            return {...state, bairro:action.payload.bairro};
        case "changeCidade":
            return {...state, cidade:action.payload.cidade};
        case "changeUf":
            return {...state, uf:action.payload.uf};
        case "changeAniver":
            return {...state, aniversario:action.payload.aniversario}
    }

	return state;
};

export default ConfigReducer;

