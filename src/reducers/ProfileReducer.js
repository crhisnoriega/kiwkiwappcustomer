import validator from 'validator';

const initialState = {
  verify: false,
  usuario: true,
  name: 'Aline Silveira',
  qtdRecebidos: 89,
  qtdEnviados: 20,
  username: 'alinesilveria',
  bio: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
  signo: 'Escorpião',
  focoatual: 'Trabalho',
  generosa: 'Muito',
  romantica: 'Um Pouco',
  musical: 'Rock',
  like: true  
};

const ProfileReducer = (state = initialState, action) => {

  if(action.type == 'changeUsuario') {
    return {...state, usuario:action.payload.usuario};
  }  

	return state;
}

export default ProfileReducer;