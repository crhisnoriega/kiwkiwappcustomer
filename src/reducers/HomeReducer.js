const initialState = {
    list: [],
    search: []
};

const HomeReducer = (state = initialState, action) => {
    switch(action.type) {
        case "handlerData":
            return { ...state, list: action.payload.data };         
    }

    return state;
};

export default HomeReducer;