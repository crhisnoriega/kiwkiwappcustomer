import validator from 'validator';

const initialState = {
	cep: '9089-000',
	rua: 'Rua Felipe de Azevedo',
	numero: 121,
	complemento: '202',
	bairro: 'Anchieta',
	cidade: 'São Paulo',
	uf: 'SP'
};

const CheckoutReducer = (state = initialState, action) => {

  if(action.type == 'changeUsuario') {
    return {...state, usuario:action.payload.usuario};
  }  

	return state;
}

export default CheckoutReducer;