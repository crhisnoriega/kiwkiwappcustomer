const initialState = {
    _id: "",
    login: "",
    password: "",
    name: "",
    user:"",
    genero: "key0",
    email: "",
    account_id: "",
    createdAt: "",
    updatedAt: "",
    __v: 0,
    token: "",
    token_firebase: "",
    spirit: {
        focus_activity: [],
        music_style: [],
        interests: [],
        _id: "",
        biography: "",
        personality: "",
        horoscopo: "",
        generous: "",
        romance: "",
        genre: "",
        fone1: "dsadas",
        birth: ""
    }
};

const MyProfileReducer = (state = initialState, action) => {
    switch(action.type) {
        case "handlerId":
            return { ...state, _id: action.payload.id };
        case "handlerGenero":
            return { ...state, genero: action.payload.genero };            
        case "handlerLogin":
            return { ...state, login: action.payload.login };
        case "handlerPassword":
            return { ...state, password: action.payload.password };
        case "handlerName":
            return { ...state, name: action.payload.name };
        case "handlerEmail":
            return { ...state, email: action.payload.email };
        case "handlerUser":
            return { ...state, user: action.payload.user };            
        case "handlerAccount":
            return { ...state, account_id: action.payload.account_id };
        case "handlerCreatedAt":
            return { ...state, createdAt: action.payload.createdAt };
        case "handler_v":
            return { ...state, __v: action.payload.__v };
        case "handlerToken":
            return { ...state, token: action.payload.token };
        case "handlerTokenFirebase":
            return { ...state, token_firebase: action.payload.token_firebase  };
        case "handlerSpirit":
            return { ...state, spirit: action.payload.spirit };
        case "handlerBio":
            let newState = state.spirit;
            newState.biography = action.payload.bio;
            return { ...state, spirit: newState };
        case "handlerPerso":
            let StatePerso = state.spirit;
            StatePerso.personality = action.payload.personality;
            return { ...state, spirit: StatePerso };            
        case "handlerHoro":
            let StateHoro = state.spirit;
            StateHoro.horoscopo = action.payload.horoscopo;
            return { ...state, spirit: StateHoro };            
        case "handlerGene":
            let StateGene = state.spirit;
            StateGene.generous = action.payload.generous;
            return { ...state, spirit: StateGene }; 
        case "handlerRoma":
            let StateRoma = state.spirit;
            StateRoma.romance = action.payload.romance;
            return { ...state, spirit: StateRoma };  
        case "handlerGenre":
            let StateGenre = state.spirit;
            StateGenre.genre = action.payload.genre;
            return { ...state, spirit: StateGenre };                                         
        case "handlerFone":
            let StateFone = state.spirit;
            StateFone.fone1 = action.payload.fone1;
            return { ...state, spirit: StateFone };  
        case "handlerBirth":
            let StateBirth = state.spirit;
            StateBirth.birth = action.payload.birth;
            return { ...state, spirit: StateBirth };                                        
    }      

    return state;
};

export default MyProfileReducer;
