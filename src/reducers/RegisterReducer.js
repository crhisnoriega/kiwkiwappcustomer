import validator from 'validator';

const initialState = {
    name: "",
    email: "",
    username: "",
    password: "",    
    successName: false,
    errorName: false,
    styleName: {color: 'black'},        
    successEmail: false,
    errorEmail: false,
    styleEmail: {color: 'black'},
    successUser: false,
    errorUser: false,
    styleUser: {color: 'black'},        
    successPass: false,
    errorPass: false,
    styleSenha: {color: 'black'},
    showPass: true
};

const RegisterReducer = (state = initialState, actions) => {
    switch (actions.type) {
        case "HandlerName":
            state.styleName = {color: 'red', fontSize: 15};
            state.errorName = true;
            if (state.name.length > 6) {
                state.errorName = false;
                state.successName = true;
                state.styleName = {color: 'green', fontSize: 15};
            }
            return {...state, name: actions.payload.name}

        case "HandlerEmail":
            state.errorEmail = true;
            if (validator.isEmail(state.email)) {
                state.errorEmail = false;
                state.successEmail = true;
                state.styleEmail = {color: 'green', fontSize: 15};
            } else {
                state.successEmail = false;
                state.errorEmail = true;
                state.styleEmail = {color: 'red', fontSize: 15};
            }
            return {...state, email: actions.payload.email}

        case "HandlerUsername":
            state.errorUser = true;
            state.styleUser = {color: 'red', fontSize: 15};
            if (state.username.length > 3) {
                state.errorUser = false;
                state.successUser = true;
                state.styleUser = {color: 'green', fontSize: 15};
            }
            return {...state, username: actions.payload.username}

        case "HandlerPassword":
            state.errorPass = true;
            state.styleSenha = {color: 'red', fontSize: 15};
            if (state.password.length > 6) {
                state.errorPass = false;
                state.successPass = true;
                state.styleSenha = {color: 'green', fontSize: 15};
            }
            return {...state, password: actions.payload.password}
    }
    return state;
};

export default RegisterReducer;