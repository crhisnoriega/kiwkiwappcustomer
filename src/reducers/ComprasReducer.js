import validator from 'validator';

const initialState = {
    rastreio: 'PYTH5656G',
    status: 'Finalizado',
    price: 'R$19,90',
    date: '12/02/2018'
};

const ComprasReducer = (state = initialState, action) => {

  if(action.type == 'changeUsuario') {
    return {...state, usuario:action.payload.usuario};
  }  

	return state;
}

export default ComprasReducer;

