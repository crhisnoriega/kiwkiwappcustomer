import validator from 'validator';

const initialState = {
    status: 0,
    id: 1,
    email: '',
    password: '',
    successEmail: false,
    errorEmail: false,
    styleEmail: {color: 'black'},
    successPass: false,
    errorPass: false,
    PassHide: true
};

const AuthReducer = (state = initialState, action) => {
    switch (action.type) {
        case "changeEmail":
            state.errorEmail = true;

            if (validator.isEmail(state.email) || state.email.length > 3) {
                state.errorEmail = false;
                state.successEmail = true;
                state.styleEmail = {color: '#FA807C', fontSize: 15};
            }
            else {
                state.successEmail = false;
                state.errorEmail = true;
                state.styleEmail = {color: 'red', fontSize: 15};
            }
            return {...state, email: action.payload.email};
        case "changePassword":
            state.errorPass = true;

            if (state.password.length > 6) {
                state.errorPass = false;
                state.successPass = true;
            }
            else {
                state.errorPass = true;
                state.successPass = false;
            }
            return {...state, password: action.payload.password};
        case "PassHide":
            return {...state, PassHide: action.payload.PassHide};
        case "changeStatus":
            state.email = '';
            state.password = '';
            return {...state, status: action.payload.status};
    }

    return state;
};

export default AuthReducer;