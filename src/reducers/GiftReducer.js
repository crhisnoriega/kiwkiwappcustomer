import validator from 'validator';

const initialState = {
  msg: '',
  name: '',
  genero: '',
  idade: '',
};

const GiftReducer = (state = initialState, action) => {

  if(action.type == 'changeMsg') {
    return {...state, msg:action.payload.msg};
  }  

  if(action.type == 'changeName') {
    return {...state, name:action.payload.name};
  } 

  if(action.type == 'changeGenero') {
    return {...state, genero:action.payload.genero};
  }    

  if(action.type == 'changeIdade') {
    return {...state, idade:action.payload.idade};
  }   

	return state;
}

export default GiftReducer;