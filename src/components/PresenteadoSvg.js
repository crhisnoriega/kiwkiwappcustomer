import React, { Component } from 'react';
import { View } from 'react-native';
import Svg, { Path } from 'react-native-svg';

export default class PresenteadoSvg extends Component {
	render() {
		return (
			<View>
			  <Svg viewBox="0 0 52 52" width={15} height={15}>
			    <Path
			      d="M26 0C11.664 0 0 11.663 0 26s11.664 26 26 26 26-11.663 26-26S40.336 0 26 0zm12.5 28H28v11a2 2 0 0 1-4 0V28H13.5a2 2 0 0 1 0-4H24V14a2 2 0 0 1 4 0v10h10.5a2 2 0 0 1 0 4z"
			      data-original="#000000"
			      className="prefix__active-path"
			      data-old_color="#325eed"
			      fill="#325eed"
			    />
			  </Svg>
			</View>
		);
	}
}