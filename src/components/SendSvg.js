import React from 'react';
import { View } from 'react-native';
import Svg, { Circle, Path } from 'react-native-svg';

export default class SendSvg extends React.Component {

  render() {

    return (
      <View>
        <Svg
          id="prefix__Camada_1"
          x={0}
          y={0}
          viewBox="0 0 24 24"
          xmlSpace="preserve"
          width={24}
          height={24}
        >
          <Circle cx={12} cy={12} r={11.5} fill="#2453eb" />
          <Path
            className="prefix__st1"
            d="M16.9 7.1l-6.5 6.4M16.9 7.1l-4.1 11.7-2.4-5.3-5.2-2.3z"
          />
        </Svg>
      </View>    
    );

  }

}
