import React, { Component } from "react";
import { View, StyleSheet } from "react-native";
import {
  ListItem,
  Left,
  Text,
  Right,
  Body,
  Thumbnail,
  Item,
  Icon
} from "native-base";

export default class PickerProductItem extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={{ justifyContent: "flex-start" }}>Nome do produto</Text>       
        <Item rounded style={{ justifyContent: "flex-end" }}>
          <Text> + </Text>
          <Text> 2 </Text>
          <Text> - </Text>
        </Item>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {    
    height: 50,
    width: "100%",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "white",
    flexDirection: "row"
  }
});
