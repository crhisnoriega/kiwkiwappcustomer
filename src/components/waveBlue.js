import React, { Component } from 'react';
import { View } from 'react-native';
import Svg, { Path } from 'react-native-svg'

export default class waveBlue extends Component {

    render() {
        return (
            <View>
              <Svg width={109} height={131}>
                <Path
                  d="M1.03 129.822c31.882 6.122 47.235-12.799 63.303-29.624 15.469-16.2 32.06-29.613 41.333-59.74C124.568-20.962 59.446-65 2.43-65c-35.36 0-87.977 15.588-107.219 43.027-11.783 16.802-.529 38.531 3.984 62.431 11.882 62.928 39.256 77.35 101.835 89.364z"
                  fill="#6868D8"
                  fillRule="evenodd"
                  opacity={0.1}
                />
              </Svg>
            </View>           
        );
    }

}

