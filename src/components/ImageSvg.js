import React from 'react';
import Svg, { G, Circle, Path } from 'react-native-svg';
import { View } from 'react-native';
import { Icon } from 'native-base';

export default class ImageSvg extends React.Component {

  render() {

    return (
      <View>
        <Icon type="Feather" name="image" style={{color: '#325eed'}} />
      </View>    
    );

  }

}
