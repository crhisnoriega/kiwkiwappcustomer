import React, { Component } from 'react';
import { View } from 'react-native';
import Svg, { G, Circle, Path } from 'react-native-svg';

export default class cancelSvg extends Component {

  render() {
    return (
    	<View>
		  <Svg width={22} height={22}>
		    <G
		      transform="translate(1 1)"
		      stroke="#B5BBDF"
		      strokeWidth={1.5}
		      fill="none"
		      fillRule="evenodd"
		      strokeLinecap="round"
		      strokeLinejoin="round"
		    >
		      <Circle cx={10} cy={10} r={10} />
		      <Path d="M13 7l-6 6M7 7l6 6" />
		    </G>
		  </Svg>
		</View>
    );
 }
}