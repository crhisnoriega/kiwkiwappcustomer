import React, { Component } from 'react';
import { View } from 'react-native';
import Svg, { G, Path } from 'react-native-svg';

export default class giftSvg extends Component {

  render() {
    return (
   	<View>
	  <Svg width={22} height={22}>
	    <G
	      stroke="#445EE9"
	      strokeWidth={2}
	      fill="none"
	      fillRule="evenodd"
	      strokeLinecap="round"
	      strokeLinejoin="round"
	    >
	      <Path d="M19 11v10H3V11M1 6h20v5H1zM11 21V6M11 6H6.5a2.5 2.5 0 0 1 0-5C10 1 11 6 11 6zM11 6h4.5a2.5 2.5 0 1 0 0-5C12 1 11 6 11 6z" />
	    </G>
	  </Svg>
	</View>
    );
 }
}