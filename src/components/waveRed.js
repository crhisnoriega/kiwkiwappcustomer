import React, { Component } from 'react';
import { View } from 'react-native';
import Svg, { Path } from 'react-native-svg';

export default class waveRed extends Component {

    render() {
        return (
            <View>
              <Svg width={159} height={328}>
                <Path
                  d="M220.538 341.657c63.329 12.18 93.824-25.467 125.74-58.947 30.726-32.233 63.682-58.923 82.099-118.87C465.924 41.626 336.57-46 223.321-46 153.083-46 48.57-14.983 10.35 39.615c-23.406 33.433-1.05 76.669 7.913 124.225C41.865 289.055 96.24 317.75 220.538 341.657z"
                  fill="#FF3C36"
                  fillRule="evenodd"
                  opacity={0.1}
                />
              </Svg>
            </View>           
        );
    }

}

