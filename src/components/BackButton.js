import React, { Component } from "react";
import { TouchableOpacity, StyleSheet, Platform } from "react-native";
import { Icon } from "native-base";

export default class BackButton extends Component {
  render() {
    return Platform.select({
      ios: () => (
        <TouchableOpacity
          onPress={() => this.props.return()}
          style={style.backButton}
        >
          <Icon type="Feather" name="arrow-left" style={style.backText} />
        </TouchableOpacity>
      ),
      android: () => <View />
    });
  }
}

const style = StyleSheet.create({
  backButton: {
    alignSelf: "flex-start"
  },
  backText: {
    width: 26,
    height: 26,
    margin: 5,
    color: "#CCC"
  }
});
