import React from 'react';
import Svg, { G, Path, Circle } from 'react-native-svg';

export default class PercentSvg extends React.Component {
  render() {
    return (
      <View>
        <Svg width={18} height={18}>
          <G
            transform="translate(1 1)"
            stroke="#445EE9"
            strokeWidth={1.5}
            fill="none"
            fillRule="evenodd"
            strokeLinecap="round"
            strokeLinejoin="round"
          >
            <Path d="M15 1L1 15" />
            <Circle cx={2.5} cy={2.5} r={2.5} />
            <Circle cx={13.5} cy={13.5} r={2.5} />
          </G>
        </Svg>
      </View>
    );
  }
}