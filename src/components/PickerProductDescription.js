import React, { Component } from "react";
import { View, StyleSheet, TouchableOpacity } from "react-native";
import {
  ListItem,
  Left,
  Text,
  Right,
  Body,
  Thumbnail,
  Item,
  Icon
} from "native-base";

export default class PickerProductItem extends Component {
  constructor(props) {
    super(props);

    this.state = {
      counter: 0
    };
  }
  render() {
    return (
      <View style={styles.container}>
        <Text style={{ justifyContent: "flex-start" }}>
          {this.props.product.name}
        </Text>
        <Item rounded style={{ justifyContent: "flex-end" }}>
          <TouchableOpacity
            onPress={e => {
              var c = this.state.counter - 1;
              this.setState({ counter: c });
              this.props.onCounterChange(this.props.product, c);
            }}
          >
            <Icon name="ios-remove" style={{ color: "#FA807C" }} />
          </TouchableOpacity>

          <Text>{this.state.counter}</Text>

          <TouchableOpacity
            onPress={e => {
              var c = this.state.counter + 1;
              this.setState({ counter: c });
              this.props.onCounterChange(this.props.product, c);
            }}
          >
            <Icon name="ios-add" style={{ color: "#FA807C" }} />
          </TouchableOpacity>
        </Item>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: 50,
    width: "100%",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "white",
    flexDirection: "row"
  }
});
