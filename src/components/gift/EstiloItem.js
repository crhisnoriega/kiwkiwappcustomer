import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, Text } from 'react-native';

export default class EstiloItem extends Component {

	constructor(props) {
		super(props);

		this.state = {
			bEstiloGift: '#FFF',
			colorIdade: '#3e4a59'			
		};
		this.select = this.select.bind(this);
	}

	select() {
		this.props.selected(this.props.data);
	}



	render() {
		return(
			<TouchableOpacity onPress={this.select} style={[styles.tagItem, {backgroundColor: this.props.data.key == this.props.extraData ? '#3e4a59' : '#FFF'}]}>
				<Text style={[styles.tagText, {color: this.state.cEstiloGift}]}>{this.props.data.name}</Text>
			</TouchableOpacity>					
		);
	}
}

const styles = StyleSheet.create({
	tagItem:{
		width: 99,
		height: 35,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#FFFFFF',
		borderRadius: 20,
		borderWidth: 1,
		borderColor: '#b5bbdf',
		marginRight: 4,
		marginBottom: 10
	},
	tagText:{
		fontWeight: '300',
		fontSize: 12,
		paddingBottom: 10,
		paddingLeft: 13,
		paddingRight: 13,
		paddingTop: 10		

	}	
});