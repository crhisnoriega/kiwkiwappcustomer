import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';
import { Icon } from 'native-base';
import Verifysvg from '../Verifysvg';
import LikeOn from '../likeOn';
import LikeOff from '../likeOff';
import { NavigationActions } from 'react-navigation';

export default class FeedItemStore extends Component {

	constructor(props) {
		super(props);

		this.state = {
			selected: 'transparent',
			width: 0,
			select: false
		};

		this.selectCard = this.selectCard.bind(this);
	}

	selectCard() {
		if(this.state.select == false) {
			this.setState({selected: '#0000FF', width: 1, select: true});
			//alert(JSON.stringify(this.props.data.id));	
			this.props.showStyles();
		} else {
			this.setState({selected: 'transparent', width: 0, select: false});
			this.props.RemoveStyles();
		}
		
	}

	render() {

		return(
			<TouchableOpacity onPress={this.selectCard}>
					<View style={[styles.card, {borderWidth: this.state.width, borderColor: this.state.selected}]}>
						<View style={styles.header}>
							<Image source={require('../../assets/img.png')} style={styles.img} />
							<View style={{position: 'absolute', top: 7, left: 7 }}>
								<Verifysvg />
							</View>							
						</View>
						<View style={styles.footer}>
							<View>
								<Text style={styles.name}>{this.props.data.name}</Text>
								<Text style={styles.username}>{this.props.data.username}</Text>
							</View>
							{this.props.data.like &&
								<TouchableOpacity onPress={() => alert('tirar like')}>
									<LikeOn />							
								</TouchableOpacity>
							}

							{this.props.data.like == false &&
								<TouchableOpacity onPress={() => alert('dar like')}>
									<LikeOff />							
								</TouchableOpacity>
							}
						</View>
					</View>
			</TouchableOpacity>
		);

	}
}

const styles = StyleSheet.create({
	container:{
		width: 192,
		height: 190,
		backgroundColor: '#fdfdfd',
		alignItems: 'center'
	},
	card:{
		margin: 10,
		width: '88%',
		height: 160,
		backgroundColor: '#FFF',
		borderRadius: 10,
	    shadowColor: "#000",
	    shadowOffset: { width: 0, height: 2 },
	    shadowOpacity: 1,
	    shadowRadius: 5.5,
	    elevation: 10		
	},
	header:{
		backgroundColor: '#fdfdfd',
		width: '100%',
		height: 120,
		borderTopLeftRadius: 10,
		borderTopRightRadius: 10,
	},
	img:{
		borderTopLeftRadius: 10,
		borderTopRightRadius: 10,
		width: 143
	},
	footer:{
		flexDirection: 'row',
		justifyContent: 'space-around',
		alignItems: 'center',
		marginTop: 3
	},
	name:{
		fontWeight: 'bold',
		fontSize: 11
	},
	username:{
		color: '#9898e3',
		fontSize: 11
	}
});