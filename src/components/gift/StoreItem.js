import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

export default class StoreItem extends Component {

	constructor(props) {
		super(props);

		this.state = {

		};

		this.openModal = this.openModal.bind(this);
	}

	openModal() {
		this.props.extraData(this.props.data);
	}

	render() {

		return(
			<TouchableOpacity onPress={this.openModal}>
				<View style={styles.card}>
					<Text style={styles.cardText}>{this.props.data.title}</Text>
					<TouchableOpacity style={styles.cardButton} onPress={() => this.props.extraData(this.props.data)}>
						<Text style={styles.cardButtonText}>Visualizar</Text>
					</TouchableOpacity>
				</View>
			</TouchableOpacity>
		);
	}
} 


const styles = StyleSheet.create({
	card:{
		width: 141,
		height: 68,
		alignItems: 'center',
		justifyContent: 'center',
		borderRadius: 20,
		borderWidth: 1,
		borderColor: '#445ee9',
		marginRight: 10
	},
	cardText:{
		color: '#445ee9',
		fontWeight: 'bold',
		fontSize: 14,
		marginBottom: 5
	},
	cardButton:{
		backgroundColor: '#3e4a59',
		borderRadius: 20		
	},
	cardButtonText:{
		paddingBottom: 4,
		paddingTop: 4,
		paddingRight: 8,
		paddingLeft: 8,
		color: '#FFFFFF',
		fontWeight: '500',
		fontSize: 12
	}
});
