import React, { Component } from 'react';
import { View, Image, Modal, StyleSheet, TouchableOpacity } from 'react-native';
import { Icon } from 'native-base';

export default class ImgItem extends Component {

	constructor(props) {
		super(props);

		this.state = {
			modalVisible:false
		};

		this.abrirModal = this.abrirModal.bind(this);
	    this.fecharModal = this.fecharModal.bind(this);
	    this.handleCloseModal = this.handleCloseModal.bind(this);
	}

	abrirModal() {
		let s = this.state;
		s.modalVisible = true;
		this.setState(s);
	}

	fecharModal() {
		let s = this.state;
		s.modalVisible = false;
		this.setState(s);
	}

	handleCloseModal () {
		this.setState({ showModal: false });
	}	

	render() {
		return(
			<View style={{marginRight: 10}}>
				<TouchableOpacity onPress={this.abrirModal}>
					<Image source={require('../../assets/imgStore.png')} style={{borderRadius: 20}} />
				</TouchableOpacity>
				<Modal animationType="slide" visible={this.state.modalVisible} onRequestClose={this.handleCloseModal}>
					<TouchableOpacity onPress={this.fecharModal}>
						<View>
							<Icon type="Feather" name="arrow-left" style={styles.buttonAbsolute} />
							<Image resizeMode="contain" style={styles.modalImage} source={require('../../assets/imgStore.png')} />
						</View>
					</TouchableOpacity>					
				</Modal>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	buttonAbsolute:{
		position: 'absolute',
		top: 0,
		left: 0,
		color: '#CCC'
	},
	modalImage:{	
		width: '100%',
		height: '100%'
	}	
});