import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

export default class AnimeItem extends Component {
	render() {
		return (
			<TouchableOpacity onPress={() => this.props.extraData(this.props.data)} style={this.props.data.name.length > 6 ? styles.tagItemBig : styles.tagItem }>
				<Text style={[styles.tagText, {color: '#4d5866'}]}>{this.props.data.name}</Text>
			</TouchableOpacity>	
		);
	}
}

const styles = StyleSheet.create({
	tagItem:{
		width: 99,
		height: 35,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#FFFFFF',
		borderRadius: 20,
		borderWidth: 1,
		borderColor: '#b5bbdf',
		marginRight: 4,
		marginBottom: 10
	},
	tagItemBig:{
		height: 35,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#FFFFFF',
		borderRadius: 20,
		borderWidth: 1,
		borderColor: '#b5bbdf',
		marginRight: 4,
		marginBottom: 10
	},
	tagText:{
		fontWeight: '300',
		fontSize: 12,
		paddingBottom: 10,
		paddingLeft: 13,
		paddingRight: 13,
		paddingTop: 10		

	}
});