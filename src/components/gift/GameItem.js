import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

export default class GameItem extends Component {
	render() {
		return (
    		<TouchableOpacity onPress={() => this.props.extraData(this.props.data)} style={[{backgroundColor: '#FFFFFF'}, styles.buttonChoose2]}>
    			<Text style={[styles.buttonChooseText, {color: '#3e4a59'}]}>{this.props.data.name}</Text>
    		</TouchableOpacity>	
		);
	}
}

const styles = StyleSheet.create({
	buttonChoose2:{
		borderTopLeftRadius: 20,
		borderTopRightRadius: 20,
		borderBottomLeftRadius: 20,
		borderWidth: 1, 
		borderColor: '#3e4a59'			
	},
	buttonChooseText:{
		paddingBottom: 10,
		paddingLeft: 13,
		paddingRight: 13,
		paddingTop: 10,
		fontWeight: '300',
		fontSize: 12		
	}	
});