import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, StyleSheet } from 'react-native';
import { Icon } from 'native-base';
import ButtonPerso from './buttonPerso';

export default class ChatList extends Component {

	constructor(props) {
		super(props);

		this.state = {};

		this.onClick = this.onClick.bind(this);
	}

	onClick() {
		this.props.onPress(this.props.data);
	}

	render() {
		return (
			<TouchableOpacity onPress={this.onClick}>
				<View style={styles.container}>
					<Text style={styles.date}>{this.props.data.date}</Text>
					<View style={styles.card}>
						<Image style={{borderRadius: 10, marginRight: 20, marginLeft: 20}} source={require('../assets/contact.png')} />
						<Text style={styles.cardName}>{this.props.data.otherName}</Text>
						<View style={{marginRight: 10}}>
							<Image source={require('../assets/seta.png')} style={{width: 19, height: 19}} />
						</View>					
					</View>
				</View>
			</TouchableOpacity>
		);
	}
}

const styles = StyleSheet.create({
	container:{
	    justifyContent: 'space-around',
	    alignItems: 'center'
	},
	card:{
		backgroundColor: '#FFF',
		borderRadius: 10,
	    shadowColor: "#000",
	    shadowOffset: { width: 0, height: 2 },
	    shadowOpacity: 1,
	    shadowRadius: 5.5,
	    elevation: 5,
	   	width: '80%',
	    height: 60,
	    flexDirection: 'row',
	    justifyContent: 'center',
	    alignItems: 'center',	    
	    alignSelf: 'center',
	    marginBottom: 30			
	},
	date:{
		alignSelf: 'flex-start', 
		marginLeft: 37, 
		marginBottom: 10,
		fontSize: 12,
		fontWeight: '500',
		color: '#3e4a59'		
	},
	cardName:{
		fontSize: 16,
		color: '#3e4a59',
		fontWeight: 'bold',
		flex: 1
	}
});