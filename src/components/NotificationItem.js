import React, { Component } from 'react';
import { View, TouchableOpacity, Text, StyleSheet, Image } from 'react-native';
import { Icon } from 'native-base';
import GiftSvg from './giftSvg';
import CancelSvg from './cancelSvg';

export default class NotificationItem extends Component {
	render() {
		return (
			<View style={{marginBottom: 10}}>
				<Text style={styles.cardDate}>12 de Outubro de 2018</Text>
				<TouchableOpacity>
					<View style={[styles.boxArea, {elevation: this.props.data.length == 1 ? 30 : 8}]}>
						<TouchableOpacity>
							<Image source={require('../assets/gift-blue.png')} style={{width: 22, height: 22}} />
						</TouchableOpacity>
						<Text style={styles.boxText}>{this.props.data.title}</Text>
						<TouchableOpacity>
							<CancelSvg />
						</TouchableOpacity>
					</View>
				</TouchableOpacity>
			</View>				
		);
	}
}

const styles = StyleSheet.create({
	cardDate:{
		marginLeft: 15,
		marginBottom: 15
	},
	boxArea:{
		width: '98%',
		height: 60,
		backgroundColor: '#FFF',
		flexDirection: 'row',
		justifyContent: 'space-around',
		alignItems: 'center',
		borderRadius: 20,
		alignSelf: 'center',
		shadowColor: "#000",
	    shadowOffset: { width: 0, height: 5 },
	    shadowOpacity: 1,
	    shadowRadius: 20
	},
	boxText:{
		fontSize: 10,
		fontWeight: '500'
	}
});