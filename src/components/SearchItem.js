import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';
import { Icon } from 'native-base';
import { NavigationActions } from 'react-navigation';

export default class SearchItem extends Component {

	constructor(props) {
		super(props);

		this.state = {};

		this.goProfile = this.goProfile.bind(this);
	}

	goProfile() {
		this.props.extraData(this.props.data);
	}

	render() {

		return(
			<TouchableOpacity onPress={this.goProfile}>
					<View style={styles.card}>
						<View style={styles.header}>
							<Image source={require('../assets/img.png')} style={styles.img} />
							<View style={{position: 'absolute', top: 7, left: 7 }}>
								<Image source={require('../assets/perfil/verify.png')} style={{width: 16, height: 16}} />	
							</View>							
						</View>
						<View style={styles.footer}>
							<View>
								<Text style={styles.name}>{this.props.data.name}</Text>
								<Text style={styles.username}>{this.props.data.login}</Text>
							</View>
							{this.props.data.like &&
								<TouchableOpacity onPress={() => alert('tirar like')}>
									<Image source={require('../assets/perfil/heart-ativo.png')} style={{width: 21, height: 18}} />							
								</TouchableOpacity>
							}

							{this.props.data.like == false &&
								<TouchableOpacity onPress={() => alert('dar like')}>
									<Image source={require('../assets/perfil/heart.png')} style={{width: 21, height: 18}} />
								</TouchableOpacity>
							}
						</View>
					</View>
			</TouchableOpacity>
		);

	}
}

const styles = StyleSheet.create({
	container:{
		width: 192,
		height: 190,
		backgroundColor: '#fdfdfd',
		alignItems: 'center'
	},
	card:{
		margin: 10,
		width: '88%',
		height: 160,
		backgroundColor: '#FFF',
		borderRadius: 10,
	    shadowColor: "#000",
	    shadowOffset: { width: 0, height: 2 },
	    shadowOpacity: 1,
	    shadowRadius: 5.5,
	    elevation: 20		
	},
	header:{
		backgroundColor: '#fdfdfd',
		width: '100%',
		height: 120,
		borderTopLeftRadius: 10,
		borderTopRightRadius: 10,
	},
	img:{
		borderTopLeftRadius: 10,
		borderTopRightRadius: 10,
		width: 143
	},
	footer:{
		flexDirection: 'row',
		justifyContent: 'space-around',
		alignItems: 'center',
		marginTop: 3
	},
	name:{
		fontWeight: 'bold',
		fontSize: 11
	},
	username:{
		color: '#9898e3',
		fontSize: 11
	}
});