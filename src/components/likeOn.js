import React from 'react';
import Svg, { Path } from 'react-native-svg';

export default class likeOn extends React.Component {
  render() {
    return (
      <Svg width={21} height={18}>
        <Path
          d="M18.535 2.415A5.091 5.091 0 0 0 14.999 1a5.091 5.091 0 0 0-3.536 1.415l-.963.93-.964-.93C7.583.529 4.417.529 2.465 2.415a4.71 4.71 0 0 0 0 6.827l.963.93L10.5 17l7.071-6.828.964-.93A4.744 4.744 0 0 0 20 5.828c0-1.28-.527-2.508-1.465-3.413z"
          fill="#FB3B35"
          stroke="#FF6584"
          fillRule="evenodd"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
      </Svg>
    );
  }
}