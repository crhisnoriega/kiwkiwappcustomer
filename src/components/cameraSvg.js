import React from 'react';
import Svg, { G, Circle, Path, Ellipse } from 'react-native-svg';
import { View } from 'react-native';

export default class cameraSvg extends React.Component {
  render() {
    return (
      <View>
        <Svg width={29} height={29}>
          <G fill="none" fillRule="evenodd">
            <Circle fill="#445EE9" fillRule="nonzero" cx={14.5} cy={14.5} r={14.5} />
            <G
              transform="translate(7 8)"
              stroke="#FFF"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={1.5}
            >
              <Path d="M16 11.556A1.45 1.45 0 0 1 14.545 13H1.455A1.45 1.45 0 0 1 0 11.556V3.61a1.45 1.45 0 0 1 1.455-1.444h2.909L5.818 0h4.364l1.454 2.167h2.91A1.45 1.45 0 0 1 16 3.61v7.945z" />
              <Ellipse cx={8} cy={7.222} rx={2.909} ry={2.889} />
            </G>
          </G>
        </Svg>        
      </View>
    );
  }
}
