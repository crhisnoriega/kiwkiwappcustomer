import React, { Component } from 'react';
import { View } from 'react-native';
import Svg,{
    Circle,
    Ellipse,
    G,
    Text,
    TSpan,
    TextPath,
    Path,
    Polygon,
    Polyline,
    Line,
    Rect,
    Use,
    Image,
    Symbol,
    Defs,
    LinearGradient,
    RadialGradient,
    Stop,
    ClipPath,
    Pattern,
    Mask,
} from 'react-native-svg';

export default class waveLogin extends Component {

	render() {
		return(
			<View>
				<Svg width="278px" height="380px" viewBox="0 0 278 380">
				    <Text>Oval</Text>
				    <TSpan>Created with Sketch.</TSpan>
				    <G id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" opacity="0.100000001">
				        <G id="2.-Login" transform="translate(-97.000000, 0.000000)" fill="#C5CCD6">
				            <Path d="M317.538139,377.656987 C380.866977,389.837337 411.362353,352.189582 443.277263,318.709806 C474.003581,286.476901 506.959903,259.787338 525.376992,199.839852 C562.923601,77.6259213 433.570115,-10 320.320592,-10 C250.082648,-10 145.571449,21.0170005 107.350584,75.6145449 C83.9452901,109.048418 106.300643,152.283917 115.264193,199.839852 C138.865229,325.054671 193.238988,353.749915 317.538139,377.656987 Z" id="Oval"></Path>
				        </G>
				    </G>
				</Svg>			
			</View>
		);
	}

}