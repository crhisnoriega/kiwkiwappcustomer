import React from 'react';
import Svg, { G, Circle, Path } from 'react-native-svg';
import { View } from 'react-native';

export default class buttonPerso extends React.Component {

  render() {

    return (
      <View>
        <Svg width={19} height={19}>
          <G fill="none" fillRule="evenodd">
            <Circle fill="#445EE9" fillRule="nonzero" cx={9.5} cy={9.5} r={9.5} />
            <Path
              d="M8 13l4-4-4-4"
              stroke="#FFF"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={1.5}
            />
          </G>
        </Svg>  
      </View>    
    );

  }

}
