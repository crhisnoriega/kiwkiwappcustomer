import React from 'react';
import { View } from 'react-native';
import Svg, { G, Circle, Path } from 'react-native-svg';

export default class Verifysvg extends React.Component {

  render() {

    return (
      <View>
        <Svg width={16} height={16}>
          <G fill="none" fillRule="evenodd">
            <Circle fill="#FFF" fillRule="nonzero" cx={8} cy={8} r={8} />
            <Path
              d="M11.273 6l-5 5L4 8.727"
              stroke="#7ED321"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
            />
          </G>
        </Svg>
      </View>
    );

  }

}

