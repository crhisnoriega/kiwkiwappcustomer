import React from 'react';
import { View, StyleSheet, Image, TouchableHighlight, Text } from 'react-native';

export default class MensagemItem extends React.Component {
	constructor(props) {
	  super(props);
	
	  let bgColor = '#EEEEEE';
	  let align = 'flex-end';
	  let txtAlign = 'right';
	  let borderTopLeft = 15;
	  let borderTopRight= 15;
	  let borderBottonLeft= 15;
	  let borderBottonRight= 0; 
	  let marginLeft = 0;
	  let marginRight = 5; 
	  let color = '#000';

	  if(this.props.data.uid == this.props.me) {
	  	bgColor = '#E57066';
	  	align = 'flex-start';
	  	txtAlign = 'left';
	  	borderTopLeft = 0
		borderTopRight = 15
		borderBottonLeft = 15
		borderBottonRight = 15
		marginLeft = 14	
		color = '#FFF'  	
	  }


	  this.state = {
	  	bgColor:bgColor,
	  	align:align,
	  	txtAlign:txtAlign,
	  	borderTopLeft: borderTopLeft,
		borderTopRight: borderTopRight,
		borderBottonLeft: borderBottonLeft,
		borderBottonRight: borderBottonRight,
		marginLeft: marginLeft,
		marginRight: marginRight,
		color: color
	  };  

	  this.imageClicked = this.imageClicked.bind(this);
	}

	imageClicked() {
		this.props.onImagePress(this.props.data.imgSource);
	}

	render() {
		return (
				<View style={[MensagemItemStyles.area, 
					{
					alignSelf:this.state.align, 
					backgroundColor:this.state.bgColor,
					borderTopLeftRadius: this.state.borderTopLeft,
					borderTopRightRadius: this.state.borderTopRight,
					borderBottomLeftRadius: this.state.borderBottonLeft,
					borderBottomRightRadius: this.state.borderBottonRight,	
					marginBottom: 10			
				}
				]}>
					{this.props.data.msgType == 'text' &&
						<Text 
						style={
							{textAlign:this.state.txtAlign,
							backgroundColor: this.state.bgColor,
							alignSelf: this.state.align, 
							color: this.state.color,
							fontSize: 16, 
							fontWeight: '300'}}>{this.props.data.m}</Text>
					}
					{this.props.data.msgType == 'image' &&
							<TouchableHighlight onPress={this.imageClicked}>
								<Image style={MensagemItemStyles.image} source={{uri:this.props.data.imgSource}} />
							</TouchableHighlight>
					}									
				</View>
		);
	}
}

const MensagemItemStyles = StyleSheet.create({
	area:{
		marginLeft:10,
		marginTop:10,
		marginTop: 5,
		marginRight: 5,
		padding:10,
		maxWidth: '80%'
	},
	dateTxt:{
		fontSize:11,
		textAlign:'right'
	},
	image:{
		width: 100,
		height: 100
	},
	imageFull:{
		width: 100,
		height: 100
	}
});