import React from 'react';
import { View } from 'react-native';
import Svg, {
  Defs,
  LinearGradient,
  Stop,
  G,
  Path,
  Circle,
} from 'react-native-svg';

export default class Instagram extends React.Component {
  render() {
    return (
      <View>
        <Svg width={55} height={55}>
          <Defs>
            <LinearGradient x1="50%" y1="99.709%" x2="50%" y2=".777%" id="prefix__a">
              <Stop stopColor="#E09B3D" offset="0%" />
              <Stop stopColor="#C74C4D" offset="30%" />
              <Stop stopColor="#C21975" offset="60%" />
              <Stop stopColor="#7024C4" offset="100%" />
            </LinearGradient>
            <LinearGradient
              x1="50%"
              y1="146.099%"
              x2="50%"
              y2="-45.16%"
              id="prefix__b"
            >
              <Stop stopColor="#E09B3D" offset="0%" />
              <Stop stopColor="#C74C4D" offset="30%" />
              <Stop stopColor="#C21975" offset="60%" />
              <Stop stopColor="#7024C4" offset="100%" />
            </LinearGradient>
            <LinearGradient
              x1="50%"
              y1="658.141%"
              x2="50%"
              y2="-140.029%"
              id="prefix__c"
            >
              <Stop stopColor="#E09B3D" offset="0%" />
              <Stop stopColor="#C74C4D" offset="30%" />
              <Stop stopColor="#C21975" offset="60%" />
              <Stop stopColor="#7024C4" offset="100%" />
            </LinearGradient>
          </Defs>
          <G fill="none" fillRule="evenodd">
            <Path
              d="M38.548 0H16.356C7.337 0 0 7.337 0 16.356v22.192c0 9.018 7.337 16.356 16.356 16.356h22.192c9.018 0 16.356-7.338 16.356-16.356V16.356C54.904 7.337 47.566 0 38.548 0zM49.38 38.548c0 5.982-4.85 10.832-10.832 10.832H16.356c-5.983 0-10.833-4.85-10.833-10.832V16.356c0-5.983 4.85-10.833 10.833-10.833h22.192c5.982 0 10.832 4.85 10.832 10.833v22.192z"
              fill="url(#prefix__a)"
              fillRule="nonzero"
            />
            <Path
              d="M27.452 13.252c-7.83 0-14.2 6.37-14.2 14.2 0 7.83 6.37 14.2 14.2 14.2 7.83 0 14.2-6.37 14.2-14.2 0-7.83-6.37-14.2-14.2-14.2zm0 22.877a8.677 8.677 0 1 1 0-17.354 8.677 8.677 0 0 1 0 17.354z"
              fill="url(#prefix__b)"
              fillRule="nonzero"
            />
            <Circle fill="url(#prefix__c)" cx={41.679} cy={13.358} r={3.403} />
          </G>
        </Svg>
      </View>
    );
  }
}


