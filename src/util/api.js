import axios from 'axios';

const request = axios.create({
    baseURL: "http://18.228.58.12/v1",
    headers: {"Content-Type": "application/json"},
    timeout: 15000
});

//token: "$2a$04$q8Fa0cgSFleixq0XMlmEyuC0VuIgI10PhJF1zifRw3M9Q3pGY.oSa"

export const get = async (path, options, callback) => {
    await request.get(path, options).then(res => {
        callback(res);
    }).catch(err => {
        switch(err.response.status) {
            case 400:
                callback({
                    data: {
                        success: false
                    }
                });
                break;
        }
    });
};

export const post = async (path, data, options, callback) => {
    await request.post(path, data, options).then(res => {
        callback(res);
    }).catch(err => {
        switch(err.response.status) {
            case 400:
                callback({
                    data: {
                        success: false
                    }
                });
                break;
        }
    });
};