import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Image, ScrollView, KeyboardAvoidingView, Platform} from 'react-native';
import {connect} from 'react-redux';
import {Item, Label, Input, Form, Content, Icon} from 'native-base';
import {HandlerName, HandlerEmail, HandlerUsername, HandlerPassword, submitRegister} from '../actions/RegisterActions';
import {showPass} from '../actions/AuthActions';
import {NavigationActions, StackActions} from 'react-navigation';
import BackButton from "../components/BackButton";
import firebase from 'react-native-firebase';


class Register extends Component {

    constructor(props) {
        super(props);

        this.state = {};

        this.verifyStatus = this.verifyStatus.bind(this);
        this.requestRegister = this.requestRegister.bind(this);
    }

    verifyStatus() {
        if (this.props.status === 1) {
            this.props.navigation.dispatch(StackActions.reset({
                index: 0,
                actions: [
                    NavigationActions.navigate({routeName: 'HomeTab'})
                ]
            }));
        }
    }

    requestRegister() {
        firebase.messaging().getToken().then((fireToken) => {
            this.props.submitRegister(this.props.name, this.props.email, this.props.username, this.props.password, fireToken);
     });
    }    

    componentDidUpdate() {
        this.verifyStatus();
    }

    render() {
        const AreaBehavior = Platform.OS === 'ios' ? 'padding' : null;
        const keyboardVerticalOffset = Platform.OS === 'ios' ? 40 : null;

        return (
            <ScrollView style={{backgroundColor: '#FFF'}}>
             <View style={styles.container}>
                <View>
                    <Image source={require('../assets/wave-grey-cadastro.png')} style={styles.bgImage}/>
                </View>
                <BackButton return={() => this.props.navigation.goBack()}/>
                <View style={styles.logoArea}>
                    <View>
                        <Image source={require('../assets/auth/enigmou-logo-black.png')} style={{width: 206, height: 53 }} />
                    </View>
                </View>
                <View style={styles.formArea}>
                    
                    <Item rounded floatingLabel style={styles.formItem} success={this.props.successName}
                          error={this.props.errorName}>
                        <Label style={styles.formLabel}>Nome Completo</Label>
                        <Input
                            value={this.props.name}
                            onChangeText={this.props.HandlerName}
                            autoCapitalize="none"
                            blurOnSubmit={false}
                            getRef={input => {
                                this.firstNameRef = input;
                            }}
                            onSubmitEditing={() => {
                                this.emailRef._root.focus();
                            }}
                            returnKeyType={"next"}
                        />
                        <Icon
                            type="FontAwesome"
                            name={this.props.name != "" ? "check-circle" : ""}
                            style={this.props.styleName}/>
                    </Item>

                    <Item floatingLabel style={styles.formItem} success={this.props.successEmail}
                          error={this.props.errorEmail}>
                        <Label style={styles.formLabel}>E-mail</Label>
                        <Input
                            value={this.props.email}
                            onChangeText={this.props.HandlerEmail}
                            autoCapitalize="none"
                            blurOnSubmit={false}
                            getRef={input => {
                                this.emailRef = input;
                            }}
                            onSubmitEditing={() => {
                                this.userRef._root.focus();
                            }}
                            returnKeyType={"next"}
                        />
                        <Icon
                            type="FontAwesome"
                            name={this.props.email != "" ? "check-circle" : ""}
                            style={this.props.styleEmail}/>
                    </Item>
                    <Item floatingLabel style={styles.formItem} success={this.props.successUser}
                          error={this.props.errorUser}>
                        <Label style={styles.formLabel}>Usuario</Label>
                        <Input
                            value={this.props.username}
                            onChangeText={this.props.HandlerUsername}
                            autoCapitalize="none"
                            blurOnSubmit={false}
                            getRef={input => {
                                this.userRef = input;
                            }}
                            onSubmitEditing={() => {
                                this.passRef._root.focus();
                            }}
                            returnKeyType={"next"}
                        />
                        <Icon
                            type="FontAwesome"
                            name={this.props.username != "" ? "check-circle" : ""}
                            style={this.props.styleUser}/>
                    </Item>
                    <Item floatingLabel style={styles.formItem} error={this.props.errorPass}
                          success={this.props.successPass}>
                        <Label style={styles.formLabel}>Senha</Label>
                        <Input
                            value={this.props.password}
                            onChangeText={this.props.HandlerPassword}
                            secureTextEntry={this.props.PassHide}
                            autoCapitalize="none"
                            getRef={input => {
                                this.passRef = input;
                            }}
                            onSubmitEditing={this.requestRegister}
                        />
                        <Icon
                            type="Feather"
                            name={this.props.PassHide == true ? "eye-off" : "eye"}
                            onPress={() => this.props.showPass(this.props.PassHide)}
                            style={{color: '#CCCCCC', fontSize: 15}}/>
                    </Item>
                </View>

                <View style={styles.buttonArea}>
                    <View style={styles.buttonContent}>
                        <TouchableOpacity
                            onPress={this.requestRegister}
                            style={[styles.button, styles.buttonBg]}>
                            <Text style={styles.buttonText}>Cadastrar</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.buttonContent}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Login')}
                                          style={styles.signupButton}>
                            <Text style={styles.signupText}>Já possui conta? Acesse já!</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
            </ScrollView>
        );

    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FFF',
    },
    logoArea: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 100
    },
    formArea: {
        width: 315,
        height: 275,
        margin: 10
    },
    formLabel: {
        fontSize: 10
    },
    formItem: {
        marginTop: 12
    },
    forgetText: {
        marginTop: 10,
        textAlign: 'right',
        color: '#3b5998'
    },
    buttonArea: {
        width: '100%',
        marginTop: 20
    },
    buttonContent: {
        marginTop: 5
    },
    button: {
        width: 315,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        borderRadius: 6,
        marginBottom: 5
    },
    buttonText: {
        fontWeight: 'bold',
        fontSize: 15,
        color: '#FFF'
    },
    signupButton: {
        marginTop: 30,
        marginBottom: 20
    },
    signupText: {
        fontWeight: 'bold',
        color: '#a8a9ab',
        textAlign: 'center'
    },
    buttonBg: {
        backgroundColor: '#ff3c36'
    },
    backButton: {
        alignSelf: 'flex-start'
    },
    backText: {
        width: 26,
        height: 26,
        margin: 5,
        color: '#CCC'
    },
    bgImage: {
        position: 'absolute',
        top: 0,
        right: 0
    }
});

const mapStateToProps = (state) => {
    return {
        email: state.register.email,
        password: state.register.password,
        name: state.register.name,
        username: state.register.username,
        successName: state.register.successName,
        errorName: state.register.errorName,
        styleName: state.register.styleName,
        successEmail: state.register.successEmail,
        errorEmail: state.register.errorEmail,
        styleEmail: state.register.styleEmail,
        successUser: state.register.successUser,
        errorUser: state.register.errorUser,
        styleUser: state.register.styleUser,
        successPass: state.register.successPass,
        errorPass: state.register.errorPass,
        styleSenha: state.register.styleSenha,
        PassHide: state.auth.PassHide,
        status: state.auth.status
    };
};

const RegisterConnect = connect(mapStateToProps, {
    HandlerName,
    HandlerEmail,
    HandlerUsername,
    HandlerPassword,
    submitRegister,
    showPass
})(Register);
export default RegisterConnect;
