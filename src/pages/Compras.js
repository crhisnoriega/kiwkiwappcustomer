import React, { Component } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, Image, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import { NavigationActions, StackActions } from 'react-navigation';
import { Icon } from 'native-base';

class Compras extends Component {

	constructor(props) {
		super(props);

		this.state = {
			modalVisible:false
		};	

		this.back = this.back.bind(this);		
	}

	back() {
		const navigateAction = NavigationActions.navigate({
		  routeName: 'HomeTab',

		  action: NavigationActions.navigate({ routeName: 'Profile' }),
		});

		this.props.navigation.dispatch(navigateAction);			
	}	

	render() {


		return (
			<ScrollView style={{backgroundColor: '#FFF'}}>
				<View style={styles.container}>
			        <TouchableOpacity onPress={this.back} style={styles.backButton}>
			          <Icon type="Feather" name="arrow-left" style={styles.backText} />
			        </TouchableOpacity>								
					<View style={styles.content}>
						<Text style={styles.headerText}>Abaixo estão suas compras.</Text>
						<View style={{marginTop: 20}}>
							<View style={styles.card}>
								<View style={styles.cardCol}>
									<View style={{marginBottom: 10}}>
										<Text style={styles.cardText}>Presenteado</Text>
										<Text style={{fontSize: 16, fontWeight: 'bold', color: '#445ee9'}}>{this.props.name}</Text>
									</View>
									<View style={{marginBottom: 10}}>
										<Text style={styles.cardText}>Rastreio</Text>
										<Text style={styles.cardBody}>{this.props.rastreio}</Text>
									</View>
									<View>
										<Text style={styles.cardText}>Status</Text>
										<Text style={styles.cardBody}>{this.props.status}</Text>
									</View>																		
								</View>
								<View style={[styles.cardCol, {alignSelf: 'flex-start'}]}>
									<View style={{marginBottom: 20}}>
										<Text style={styles.cardText}>Valor</Text>
										<Text style={{fontSize: 12, fontWeight: 'bold', color: '#68e781'}}>{this.props.price}</Text>
									</View>
									<View>
										<Text style={styles.cardText}>Data</Text>
										<Text style={styles.cardBody}>{this.props.date}</Text>
									</View>									
								</View>								
							</View>
							<View style={styles.card}>
								<View style={styles.cardCol}>
									<View style={{marginBottom: 10}}>
										<Text style={styles.cardText}>Presenteado</Text>
										<Text style={{fontSize: 16, fontWeight: 'bold', color: '#445ee9'}}>{this.props.name}</Text>
									</View>
									<View style={{marginBottom: 10}}>
										<Text style={styles.cardText}>Rastreio</Text>
										<Text style={styles.cardBody}>{this.props.rastreio}</Text>
									</View>
									<View>
										<Text style={styles.cardText}>Status</Text>
										<Text style={styles.cardBody}>{this.props.status}</Text>
									</View>																		
								</View>
								<View style={[styles.cardCol, {alignSelf: 'flex-start'}]}>
									<View style={{marginBottom: 20}}>
										<Text style={styles.cardText}>Valor</Text>
										<Text style={{fontSize: 12, fontWeight: 'bold', color: '#68e781'}}>{this.props.price}</Text>
									</View>
									<View>
										<Text style={styles.cardText}>Data</Text>
										<Text style={styles.cardBody}>{this.props.date}</Text>
									</View>									
								</View>								
							</View>							
						</View>
					</View>
				</View>
			</ScrollView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#FFFFFF'
	},
	backButton:{
		alignSelf: 'flex-start'
	},
	backText:{
		width: 26,
		height: 26,
		margin: 5,
		color: '#CCC'
	},
	content:{
		margin: 30			
	},
	headerText:{
		marginTop: 10,
		fontSize: 14,
		fontWeight: 'bold',
		color: '#3e4a59',
		marginBottom: 10
	},
	card:{
		backgroundColor: '#FFF',
		borderRadius: 10,
	    shadowColor: "#000",
	    shadowOffset: { width: 0, height: 2 },
	    shadowOpacity: 1,
	    shadowRadius: 5.5,
	    elevation: 10,
	    flexDirection: 'row',
	    alignItems: 'center',
	    justifyContent: 'space-around',
	    marginBottom: 10		
	},
	cardCol:{
		marginTop: 20,
		marginBottom: 20
	},
	cardText:{
		fontSize: 12,
		fontWeight: '500',
		color: '#3e4a59',
		marginBottom: 3
	},
	cardBody:{
		color: '#3e4a59',
		fontWeight: '500',
		fontSize: 14
	}	   	
});

const mapStateToProps = (state) => {
	return {
		name: state.profile.name,
		rastreio: state.compras.rastreio,
		status: state.compras.status,
		price: state.compras.price,
		date: state.compras.date,
	}
}

const ComprasConnect = connect(mapStateToProps, {})(Compras);
export default ComprasConnect;