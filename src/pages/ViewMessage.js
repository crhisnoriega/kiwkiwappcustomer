import React, {Component} from 'react';
import {Text, View, StyleSheet, BackHandler, TouchableOpacity, Image, Button, ScrollView} from 'react-native';
import {connect} from 'react-redux';
import {NavigationActions} from 'react-navigation';

class ViewMessage extends Component {

    constructor(props) {
        super(props);

        this.state = {
            selected: "key0",
            foto: null
        };

        this.backProfile = this.backProfile.bind(this);
  }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.backProfile);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.backProfile);
    }

    backProfile() {
      const navigateAction = NavigationActions.navigate({
        routeName: 'HomeTab',

        action: NavigationActions.navigate({ routeName: 'Message' }),
      });

      this.props.navigation.dispatch(navigateAction); 
      return true;
    }


    render() {
        return (            
              <View style={styles.container}>
                <View style={styles.avatarImage}>
                  <Image source={{uri:"https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg"}} style={{width: 120, height: 120, borderRadius: 10}} />
                  <View>
                    <Text style={styles.avatarName}>{this.props.name}</Text>
                    <Text style={styles.avatarUser}>te presenteou!</Text>
                  </View>
                </View> 
                <Image source={require('../assets/whoSend.png')} style={{width: 150, height: 150, marginBottom: 20}} />
                <View style={styles.buttonArea}>
                  <View style={styles.buttonContent}>  
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Chat')} style={[styles.button, styles.buttonBg]} disabled={this.state.buttonLogin}>
                          <Text style={{color: '#FFF', fontSize: 30, fontWeight: 'bold'}}>Veja sua mensagem</Text>
                    </TouchableOpacity>  
                  </View>
                </View>      
              </View>           
        );
    }
}

const styles = StyleSheet.create({
  centerText: {
    flex: 1,
    fontSize: 18,
    padding: 32,
    color: '#777',
  },
  textBold: {
    fontWeight: '500',
    color: '#000',
  },
  buttonText: {
    fontSize: 100,
    color: '#000',
  },
  buttonTouchable: {
    padding: 16,
  },
  avatarImage:{
      justifyContent: 'center',
      alignItems: 'center'
  },
  avatarName:{
    fontWeight: 'bold',
    color: '#000',
    fontSize: 30,
    marginTop: 20,
    marginBottom: 2
  },
  avatarUser:{
    color: '#3e4a59',
    fontSize: 20,
    textAlign: 'center',
    fontWeight: 'bold',
    marginBottom: 30
  },
  container:{
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  backButton: {
      alignSelf: 'flex-start'
  },
  backText: {
      width: 26,
      height: 26,
      margin: 5,
      color: '#CCC'
  },
  buttonBg:{
      backgroundColor: '#ff3c36'
  },
  buttonArea:{
        width: '100%',
        marginTop: 30
    },
  buttonContent:{
      marginTop: 5
  },
  button:{
      width: 315,
      height: 75,
      alignItems: 'center',
      justifyContent: 'center',
      alignSelf: 'center',
      borderRadius: 6,
      marginBottom: 5
  },
  buttonMedia:{
        width: 315,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        borderRadius: 6,
        backgroundColor: '#1f86f4'
  },
  buttonText:{
      fontWeight: 'bold',
      fontSize: 15,
      color: '#FFF'
  }
});

const mapStateToProps = (state) => {
    return {
      name: state.profile.name
    }
}

const ViewMessageConnect = connect(mapStateToProps, {})(ViewMessage);
export default ViewMessageConnect;	