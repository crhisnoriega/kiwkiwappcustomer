import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  Platform,
  ScrollView,
  KeyboardAvoidingView
} from "react-native";
import { connect } from "react-redux";
import {
  Card,
  Item,
  Label,
  Input,
  Form,
  Content,
  Icon,
  CardItem,
  Body
} from "native-base";
import {
  changeEmail,
  changePassword,
  submitLogin,
  showPass
} from "../actions/AuthActions";
import { NavigationActions, StackActions } from "react-navigation";
import BackButton from "../components/BackButton";
import Spinner from "react-native-spinkit";
// import firebase from 'react-native-firebase';
import {
  requestMyProfile,
  handlerId,
  handlerLogin,
  handlerPassword,
  handlerName,
  handlerEmail,
  handlerAccount,
  handlerCreatedAt,
  handler_v,
  handlerToken,
  handlerTokenFirebase,
  handlerSpirit
} from "../actions/MyProfileAction";

export class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showToast: false,
      loading: false,
      buttonLogin: false,
      firebaseToken: null
    };
    this.verifyStatus = this.verifyStatus.bind(this);
  }

  verifyStatus() {
    if (this.props.status === 1) {
      this.props.navigation.dispatch(
        StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: "HomeTab" })]
        })
      );
    }
  }

  componentDidUpdate() {
    this.verifyStatus();
  }

  requestLogin() {
    this.props.submitLogin(
      this.props.email,
      this.props.password,
      "fasdf",
      callback => {
        if (callback === true) {
          this.setState({
            buttonLogin: true,
            loading: true
          });
        }
      }
    );
  }

  render() {
    let AreaBehavior = Platform.select({ ios: "padding", android: null });
    let AreaOffset = Platform.select({ ios: 64, android: null });

    return (
      <View style={styles.container}>
        <View style={styles.headerArea}>
          <Image
            style={{ width: 50, height: 30 }}
            source={require("../assets/kiwkiw_upper.png")}
          />
        </View>

        <View style={styles.formArea}>
          <Text style={styles.bemvindokiwkiw}>Bem vindo ao KiwKiw</Text>

          <View
            style={{
              flex: 4,
              alignItems: "center",
              justifyContent: "center",
              height: "90%"
            }}
          >
            <Item
              rounded
              success={this.props.successEmail}
              error={this.props.errorEmail}
              style={styles.myinput}
            >
              <Input
                value={this.props.email}
                onChangeText={this.props.changeEmail}
                autoCapitalize="none"
                blurOnSubmit={false}
                getRef={input => {
                  this.firstNameRef = input;
                }}
                onSubmitEditing={() => {
                  // this.passInputRef._root.focus();
                }}
                returnKeyType={"next"}
                placeholder="Login"
                textAlign={"center"}
              />
              <Icon
                type="FontAwesome"
                name={this.props.email != "" ? "check-circle" : ""}
                style={styles.myinputicon}
              />
            </Item>

            <Item
              rounded
              style={styles.formItem}
              error={this.props.errorPass}
              success={this.props.successPass}
              style={styles.myinput}
            >
              <Input
                value={this.props.password}
                tintColor="#FFF"
                onChangeText={this.props.changePassword}
                secureTextEntry={this.props.PassHide}
                autoCapitalize="none"
                getRef={input => {
                  this.passInputRef = input;
                }}
                onSubmitEditing={() => this.requestLogin()}
                placeholder=" Senha"
                textAlign={"center"}
              />
              <Icon
                type="Feather"
                name={this.props.PassHide == true ? "eye-off" : "eye"}
                onPress={() => this.props.showPass(this.props.PassHide)}
                style={{ color: "#CCCCCC", fontSize: 15 }}
              />
            </Item>
          </View>

          <View style={{ flex: 1 }}>
            <View style={styles.buttonContent}>
              <TouchableOpacity
                onPress={() => this.requestLogin()}
                style={[styles.button, styles.buttonBg]}
                disabled={this.state.buttonLogin}
              >
                <Text style={styles.buttonText}>Continuar</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#e5e5e5"
  },

  headerArea: {
    flex: 1,
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 8,
    backgroundColor: "white"
  },

  formArea: {
    flex: 5,
    width: "95%",
    alignItems: "center",
    justifyContent: "center",
    margin: 10,
    borderRadius: 8,
    backgroundColor: "white"
  },

  myinput: {
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#e5e5e5",
    marginLeft: 20,
    width: "80%",
    height: 29,
    fontSize: 20,
    fontWeight: "bold",
    marginTop: 5
  },

  myinputicon: {
    height: 25
  },

  buttonContent: {
    flex: 1
  },

  bemvindokiwkiw: {
    fontSize: 24,
    color: "#FA807C",
    textAlign: "center",
    marginTop: 40,
    marginBottom: 30,
    fontWeight: "bold"
  },

  logoArea: {
    alignItems: "center",
    justifyContent: "center",
    height: 150,
    marginBottom: 18
  },

  formLabel: {
    fontSize: 10
  },
  formItem: {
    marginTop: 15
  },
  forgetText: {
    marginTop: 10,
    textAlign: "right",
    color: "#3b5998"
  },
  buttonArea: {
    width: "100%",
    marginTop: 30
  },

  button: {
    width: 315,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "center",
    borderRadius: 30,
    marginBottom: 5
  },
  buttonMedia: {
    width: 315,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "center",
    borderRadius: 6,
    backgroundColor: "#FA807C"
  },
  buttonText: {
    fontWeight: "bold",
    fontSize: 15,
    color: "#FFF"
  },
  signupButton: {
    marginTop: 20,
    marginBottom: 20
  },
  signupText: {
    fontWeight: "bold",
    color: "#8b9dc3",
    textAlign: "center"
  },
  buttonBg: {
    backgroundColor: "#FA807C"
  },
  backButton: {
    alignSelf: "flex-start"
  },
  backText: {
    width: 26,
    height: 26,
    margin: 5,
    color: "#CCC"
  },
  bgImage: {
    position: "absolute",
    top: 0,
    right: 0
  },

  loginButton: {
    position: "absolute",
    bottom: 0
  },
  bg: {
    width: "50%",
    height: "50%"
  }
});

const mapStateToProps = state => {
  return {
    email: state.auth.email,
    password: state.auth.password,
    successEmail: state.auth.successEmail,
    errorEmail: state.auth.errorEmail,
    successPass: state.auth.successPass,
    errorPass: state.auth.errorPass,
    styleEmail: state.auth.styleEmail,
    styleSenha: state.auth.styleSenha,
    PassHide: state.auth.PassHide,
    status: state.auth.status
  };
};

const LoginConnect = connect(
  mapStateToProps,
  {
    changeEmail,
    changePassword,
    submitLogin,
    showPass,
    requestMyProfile,
    handlerId,
    handlerLogin,
    handlerPassword,
    handlerName,
    handlerEmail,
    handlerAccount,
    handlerCreatedAt,
    handler_v,
    handlerToken,
    handlerTokenFirebase,
    handlerSpirit
  }
)(Login);
export default LoginConnect;
