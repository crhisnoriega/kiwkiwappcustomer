import React, {Component} from 'react';
import {Text, View, StyleSheet, Image, TouchableOpacity, ScrollView, BackHandler, AsyncStorage, KeyboardAvoidingView, Platform} from 'react-native';
import {connect} from 'react-redux';
import {NavigationActions} from 'react-navigation';
import {Item, Label, Input, Form, Content, Icon, Picker, Textarea} from 'native-base';
import {
    changeName,
    changePass,
    changeTel,
    changeGenero,
    changeCep,
    changeBio,
    changeRua,
    changeNumero,
    changeComplemento,
    changeBairro,
    changeEmail,
    changeUser,
    changeCidade,
    changeUf
} from '../../actions/ConfigActions';
import BackButton from '../../components/BackButton';
import ImagePicker from 'react-native-image-picker';
import axios from 'axios';

class AbaEndereco extends Component {

    constructor(props) {
        super(props);

        this.state = {
            selected: "key0",
            foto: null
        };

    }

    onValueChange(value: string) {
        this.setState({
            selected: value
        });
    }

    async getAddressRequest() {
        let request = axios.create({
            headers: { "Content-Type" : "application/json"},
            baseURL: "https://viacep.com.br/ws"
        });

        await request.get(`/${this.props.cep}/json`).then(res => {
            const { logradouro, complemento, bairro, localidade, uf, ibge } = res.data;

            this.props.changeRua(logradouro);
            this.props.changeCidade(localidade);
            this.props.changeBairro(bairro);
            this.props.changeComplemento(complemento);
            this.props.changeUf(uf);
        });
    }

    render() { 
        return (                      
            <ScrollView>
                <View style={styles.container}>
                    <BackButton return={() => this.backProfile()}/>
                    <View style={styles.content}>
                        <View style={styles.textArea}>
                            <Item floatingLabel style={styles.formItem}>
                                <Label style={styles.formLabel}>CEP</Label>
                                <Input
                                    style={styles.formInput}
                                    autoCapitalize="none"
                                    value={this.props.cep}
                                    onChangeText={this.props.changeCep}
                                    onSubmitEditing={() => this.getAddressRequest()}
                                />
                            </Item>
                            <Item floatingLabel style={styles.formItem}>
                                <Label style={styles.formLabel}>Rua</Label>
                                <Input
                                    style={styles.formInput}
                                    autoCapitalize="none"
                                    value={this.props.rua}
                                    onChangeText={this.props.changeRua}
                                />
                            </Item>
                            <View style={styles.colArea}>
                                <Item floatingLabel style={{width: '50%'}}>
                                    <Label style={styles.formLabel}>Número</Label>
                                    <Input
                                        style={styles.formInput}
                                        value={this.props.numero}
                                        onChangeText={this.props.changeNumero}
                                        autoCapitalize="none"
                                    />
                                </Item>
                                <Item floatingLabel style={{width: '50%'}}>
                                    <Label style={styles.formLabel}>Complemento</Label>
                                    <Input
                                        style={styles.formInput}
                                        value={this.props.complemento}
                                        onChangeText={this.props.changeComplemento}
                                        autoCapitalize="none"
                                    />
                                </Item>
                            </View>
                            <Item floatingLabel style={styles.formItem}>
                                <Label style={styles.formLabel}>Bairro</Label>
                                <Input
                                    style={styles.formInput}
                                    value={this.props.bairro}
                                    onChangeText={this.props.changeBairro}
                                    autoCapitalize="none"
                                />
                            </Item>
                            <View style={styles.colArea}>
                                <Item floatingLabel style={{width: '70%'}}>
                                    <Label style={styles.formLabel}>Cidade</Label>
                                    <Input
                                        style={styles.formInput}
                                        value={this.props.cidade}
                                        onChangeText={this.props.changeCidade}
                                        autoCapitalize="none"
                                    />
                                </Item>
                                <Item floatingLabel style={{width: '30%'}}>
                                    <Label style={styles.formLabel}>UF</Label>
                                    <Input
                                        style={styles.formInput}
                                        value={this.props.uf}
                                        onChangeText={this.props.changeUf}
                                        autoCapitalize="none"
                                    />
                                </Item>
                            </View>                                                    
                        </View>
                            <TouchableOpacity onPress={() => {
                            }} style={[styles.button, styles.buttonBg]}>
                                <Text style={styles.buttonText}>Salvar</Text>
                            </TouchableOpacity>
                    </View>                    
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFFFFF'
    },
    backButton: {
        alignSelf: 'flex-start'
    },
    backText: {
        width: 26,
        height: 26,
        margin: 5,
        color: '#CCC'
    },
    content: {
        margin: 30
    },
    headerText: {
        color: '#4b5461',
        marginBottom: 5,
        fontWeight: 'bold'
    },
    avatarArea: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        justifyContent: 'space-between',
        marginTop: 10
    },
    avatarProfile: {
        width: 100,
        height: 100,
        borderRadius: 20
    },
    persoButton: {
        backgroundColor: '#FFFFFF',
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 20,
        paddingRight: 20,
        shadowColor: "#000",
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: 1,
        shadowRadius: 5.5,
        elevation: 3,
        borderRadius: 20
    },
    bio: {
        width: '100%',
        marginTop: 30,
        marginBottom: 30
    },
    titleBio: {
        color: '#656d78',
        fontWeight: 'bold'
    },
    bodyBio: {
        textAlign: 'justify',
        width: 320,
        color: '#8e99a6'
    },
    textArea: {},
    formItem: {
    },
    formLabel: {
        fontSize: 12
    },
    formInput: {
        fontSize: 14,
        color: '#7b838d',
        fontWeight: 'bold'
    },
    colArea: {
        flexDirection: 'row',
        marginTop: 17,
        borderBottomWidth: 1,
        borderBottomColor: '#CCC'
    },
    button: {
        width: 332,
        height: 54,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        borderRadius: 6,
        marginBottom: 20,
        marginTop: 20
    },
    buttonBg: {
        backgroundColor: '#ff3c36'
    },
    buttonText: {
        fontWeight: 'bold',
        fontSize: 20,
        color: '#FFF'
    }
});

const mapStateToProps = (state) => {
    return {
        status: state.auth.status,
        PassHide: state.auth.PassHide,
        name: state.config.name,
        email: state.config.email,
        user: state.config.user,
        password: state.config.password,
        tel: state.config.tel,
        aniversario: state.config.aniversario,
        genero: state.config.genero,
        cep: state.config.cep,
        rua: state.config.rua,
        numero: state.config.numero,
        complemento: state.config.complemento,
        bairro: state.config.bairro,
        cidade: state.config.cidade,
        uf: state.config.uf,
        bio: state.config.bio
    }
}

const AbaEnderecoConnect = connect(mapStateToProps, {
    changeName,
    changePass,
    changeTel,
    changeBio,
    changeEmail,
    changeGenero,
    changeCep,
    changeRua,
    changeNumero,
    changeComplemento,
    changeBairro,
    changeUser,
    changeCidade,
    changeUf
})(AbaEndereco);
export default AbaEnderecoConnect;	