import React, {Component} from 'react';
import {Text, View, StyleSheet, Image, TouchableOpacity, ScrollView, BackHandler, AsyncStorage, KeyboardAvoidingView, Platform} from 'react-native';
import {connect} from 'react-redux';
import {NavigationActions} from 'react-navigation';
import {Item, Label, Input, Form, Content, Icon, Picker, Textarea} from 'native-base';
import {
    saveImage,
    savePerfil
} from '../../actions/ConfigActions';
import { 
    handlerName,
    handlerPassword,
    handlerEmail,
    handlerUser,
    handlerBio,
    handlerPerso,
    handlerHoro,
    handlerGene,
    handlerRoma,
    handlerGenre,
    handlerFone,
    handlerBirth,
    handlerGenero    
 } from '../../actions/MyProfileAction';
import CameraSvg from '../../components/cameraSvg';
import BackButton from '../../components/BackButton';
import ImagePicker from 'react-native-image-picker';
import * as Http from "../../util/api";

class AbaPerfil extends Component {

    constructor(props) {
        super(props);

        this.state = {
            selected: "masculino",
            foto: null,
            image: null,
            loading: false
        };

        this.takePhoto = this.takePhoto.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
    }

     async takePhoto() {
        const options = {
            base64: true,
            allowsEditing: false,
            aspect: [4, 3],
        };   
             
        ImagePicker.launchImageLibrary(options, (r) => {
            if (r.uri) {
                let foto = {uri: r.uri};
                let image = 'data:image/jpeg;base64,' + r.data;

                let state = this.state;
                state.foto = foto;
                this.setState(state);

                this.setState({
                    image: image
                });

                this.props.saveImage(AsyncStorage.getItem("Username"), image, result => {
                    const { success } = result.data;

                    if (success === true) {
                        alert("Imagem salva com sucesso.");
                    }
                });
            }
        });
    }

    onValueChange(value: string) {
        this.setState({
            selected: value
        });
    }

    render() {        
        return (                      
            <ScrollView>
                <View style={styles.container}>
                    <BackButton return={() => this.backProfile()}/>
                    <View style={styles.content}>

                        <View style={styles.avatarArea}>
                            <TouchableOpacity onPress={this.takePhoto}>
                                <Image source={this.state.foto == null ? require('../../assets/imgProfile.png') : this.state.foto} style={styles.avatarProfile}/>
                                <View style={{position: 'absolute', top: -7, right: -7}}>
                                    <Image source={require('../../assets/perfil/camera.png')} style={{width: 29, height: 29}} />
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.bio}>
                            <View style={{alignSelf: 'center'}}>
                                <Text style={styles.titleBio}>Bio (140 caracteres)</Text>
                                <Textarea 
                                    rowSpan={5} 
                                    bordered 
                                    placeholder="Bio" 
                                    value={this.props.bio} 
                                    onChangeText={this.props.handlerBio} 
                                    style={styles.bodyBio}
                                    onSubmitEditing={() => {
                                        this.firstNameRef._root.focus();
                                    }}
                                    returnKeyType={"next"}                                    
                                />
                            </View>
                        </View>

                        <View style={styles.textArea}>
                            <Item floatingLabel style={styles.formItem}>
                                <Label style={styles.formLabel}>Nome Completo</Label>
                                <Input
                                    style={styles.formInput}
                                    value={this.props.name}
                                    onChangeText={this.props.handlerName}
                                    autoCapitalize="none"
                                    blurOnSubmit={false}
                                    getRef={input => {
                                        this.firstNameRef = input;
                                    }}
                                    onSubmitEditing={() => {
                                        this.emailInputRef._root.focus();
                                    }}
                                    returnKeyType={"next"}
                                />
                            </Item>
                            <Item floatingLabel style={styles.formItem}>
                                <Label style={styles.formLabel}>E-mail</Label>
                                <Input
                                    style={styles.formInput}
                                    value={this.props.email}
                                    onChangeText={this.props.handlerEmail}
                                    autoCapitalize="none"
                                    blurOnSubmit={false}
                                    getRef={input => {
                                        this.emailInputRef = input;
                                    }}
                                    onSubmitEditing={() => {
                                        this.userInputRef._root.focus();
                                    }}
                                    returnKeyType={"next"}
                                />
                            </Item>
                            <Item floatingLabel style={styles.formItem}>
                                <Label style={styles.formLabel}>Usuário</Label>
                                <Input
                                    style={styles.formInput}
                                    value={this.props.user}
                                    onChangeText={this.props.handlerUser}
                                    autoCapitalize="none"
                                    blurOnSubmit={false}
                                    getRef={input => {
                                        this.userInputRef = input;
                                    }}
                                    onSubmitEditing={() => {
                                        this.passInputRef._root.focus();
                                    }}
                                    returnKeyType={"next"}
                                />
                            </Item>
                            <Item floatingLabel style={styles.formItem}>
                                <Label style={styles.formLabel}>Senha</Label>
                                <Input
                                    style={styles.formInput}
                                    value={this.props.password}
                                    onChangeText={this.props.handlerPassword}
                                    secureTextEntry={this.props.PassHide}
                                    autoCapitalize="none"
                                    blurOnSubmit={false}
                                    getRef={input => {
                                        this.passInputRef = input;
                                    }}
                                    onSubmitEditing={() => {
                                        this.celInputRef._root.focus();
                                    }}
                                    returnKeyType={"next"}                                    
                                />
                                <Icon
                                    type="Feather"
                                    name={this.props.PassHide == true ? "eye-off" : "eye"}
                                    onPress={() => this.props.showPass(this.props.PassHide)}
                                    style={{color: '#CCCCCC', fontSize: 15}}/>
                            </Item>
                            <Item floatingLabel style={styles.formItem}>
                                <Label style={styles.formLabel}>Celular</Label>
                                <Input
                                    style={styles.formInput}
                                    onChangeText={this.props.handlerFone}
                                    autoCapitalize="none"
                                    blurOnSubmit={false}
                                    getRef={input => {
                                        this.celInputRef = input;
                                    }}           
                                    onSubmitEditing={() => {
                                        this.aniverInputRef._root.focus();
                                    }}
                                    returnKeyType={"next"}                                                               
                                />
                            </Item>
                            <Item floatingLabel style={styles.formItem}>
                                <Label style={styles.formLabel}>Aniversário</Label>
                                <Input
                                    style={styles.formInput}
                                    value={this.props.aniversario}
                                    onChangeText={this.props.handlerBirth}
                                    autoCapitalize="none"
                                    blurOnSubmit={false}
                                    getRef={input => {
                                        this.aniverInputRef = input;
                                    }}                                      
                                />
                            </Item>
                            <View style={{marginTop: 17, marginLeft: -5}}>
                                <Label style={[styles.formLabel, {marginLeft: 8}]}>Gênero</Label>
                                <Picker
                                    itemStyle={{color: 'blue'}}
                                    mode="dropdown"
                                    iosIcon={<Icon name="ios-arrow-down-outline"/>}
                                    selectedValue={this.state.selected}
                                    onValueChange={this.onValueChange}
                                >
                                    <Picker.Item textStyle={{color: '#66bdca'}} label="Feminino"
                                                 value="feminino"/>
                                    <Picker.Item label="Masculino" value="masculino"/>
                                </Picker>
                            </View>                                            
                        </View>
                            <TouchableOpacity onPress={() => 
                                {
                                    this.setState({loading: true});
                                    this.props.savePerfil(this.state.selected, this.props.aniversario, () => {
                                        this.setState({loading: false});
                                    });
                                }
                            } style={[styles.button, styles.buttonBg]} >
                                <Text style={styles.buttonText}>Salvar</Text>
                            </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFFFFF'
    },
    backButton: {
        alignSelf: 'flex-start'
    },
    backText: {
        width: 26,
        height: 26,
        margin: 5,
        color: '#CCC'
    },
    content: {
        margin: 30
    },
    headerText: {
        color: '#4b5461',
        marginBottom: 5,
        fontWeight: 'bold'
    },
    avatarArea: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        justifyContent: 'space-between',
        marginTop: 10
    },
    avatarProfile: {
        width: 100,
        height: 100,
        borderRadius: 20
    },
    persoButton: {
        backgroundColor: '#FFFFFF',
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 20,
        paddingRight: 20,
        shadowColor: "#000",
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: 1,
        shadowRadius: 5.5,
        elevation: 3,
        borderRadius: 20
    },
    bio: {
        width: '100%',
        marginTop: 30,
        marginBottom: 30
    },
    titleBio: {
        color: '#656d78',
        fontWeight: 'bold'
    },
    bodyBio: {
        textAlign: 'justify',
        width: 320,
        color: '#8e99a6'
    },
    textArea: {},
    formItem: {
        marginTop: 17
    },
    formLabel: {
        fontSize: 12
    },
    formInput: {
        fontSize: 14,
        color: '#7b838d',
        fontWeight: 'bold'
    },
    colArea: {
        flexDirection: 'row',
        marginTop: 17,
        borderBottomWidth: 1,
        borderBottomColor: '#CCC'
    },
    button: {
        width: 332,
        height: 54,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        borderRadius: 6,
        marginBottom: 20,
        marginTop: 20
    },
    buttonBg: {
        backgroundColor: '#ff3c36'
    },
    buttonText: {
        fontWeight: 'bold',
        fontSize: 20,
        color: '#FFF'
    }
});

const mapStateToProps = (state) => {
    return {
        status: state.auth.status,
        PassHide: state.auth.PassHide,
        user: state.MyProfile.user,
        name: state.MyProfile.name,
        email: state.MyProfile.email,
        user: state.MyProfile.login,
        password: state.MyProfile.password,
        //fone: state.MyProfile.spirit.fone1,
        aniversario: state.MyProfile.birth,
        genero: state.MyProfile.genero,
        bio: state.MyProfile.spirit.biography,
    }
}

const AbaPerfilConnect = connect(mapStateToProps, {
    handlerName,
    handlerPassword,
    handlerEmail,
    handlerUser,
    saveImage,
    handlerBio,
    handlerPerso,
    handlerHoro,
    handlerGene,
    handlerRoma,
    handlerGenre,
    handlerFone,
    handlerBirth,
    handlerGenero,
    savePerfil
})(AbaPerfil);
export default AbaPerfilConnect;   