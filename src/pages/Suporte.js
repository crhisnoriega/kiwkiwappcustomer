import React, {Component} from 'react';
import {Text, View, StyleSheet, TouchableOpacity, Image, ScrollView, Linking} from 'react-native';
import {connect} from 'react-redux';
import {NavigationActions, StackActions} from 'react-navigation';
import {Icon} from 'native-base';
import Instagram from '../components/Instagram';
import Twitter from '../components/Twitter';

class Suporte extends Component {

    constructor(props) {
        super(props);

        this.state = {};

        this.back = this.back.bind(this);
    }

    openTwitter() {
        let url = "https://twitter.com/enigmouapp";
        Linking.openURL(url);
    }    

    openInsta() {
        let url = "https://www.instagram.com/enigmouapp/";
        Linking.openURL(url);
    }      

    back() {
        const navigateAction = NavigationActions.navigate({
            routeName: 'HomeTab',

            action: NavigationActions.navigate({routeName: 'Profile'}),
        });

        this.props.navigation.dispatch(navigateAction);
    }

    openUrl() {
        Linking.openURL(url);
    }

    render() {
        return (
            <ScrollView style={{backgroundColor: '#FFF'}}>
                <View style={styles.container}>
                    <TouchableOpacity onPress={this.back} style={styles.backButton}>
                        <Icon type="Feather" name="arrow-left" style={styles.backText}/>
                    </TouchableOpacity>
                    <View style={styles.content}>
                        <Text style={styles.textContact}>Entre em Contato</Text>
                        <Text style={styles.textEmail}>contato@enigmou.com.br</Text>
                        <View style={{
                            flexDirection: 'row',
                            marginBottom: 40,
                            justifyContent: 'space-around',
                            alignItems: 'center'
                        }}>
                            <TouchableOpacity onPress={this.openInsta}>
                                <Image source={require('../assets/suporte/instagram.png')} style={{width: 55, height: 55}} />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={this.openTwitter}>
                            <Image source={require('../assets/suporte/twitter.png')} style={{width: 66, height: 55}} />
                            </TouchableOpacity>
                        </View>
                        <Text style={{
                            textAlign: 'center',
                            marginBottom: 5,
                            fontSize: 14,
                            fontWeight: 'bold',
                            color: '#3e4a59'
                        }}>Responderemos em</Text>
                        <Text style={{textAlign: 'center', fontSize: 14, fontWeight: 'bold', color: '#3e4a59'}}>até um
                            dia útil.</Text>
                    </View>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFFFFF',
        justifyContent: 'center'
    },
    backButton: {
        alignSelf: 'flex-start'
    },
    backText: {
        width: 26,
        height: 26,
        margin: 5,
        color: '#CCC'
    },
    content: {
        margin: 30
    },
    textContact: {
        textAlign: 'center',
        marginBottom: 40,
        fontSize: 24,
        fontWeight: 'bold',
        color: '#445ee9'
    },
    textEmail: {
        textAlign: 'center',
        marginBottom: 40,
        fontSize: 14,
        color: '#3e4a59',
        fontWeight: 'bold'
    }
});

const mapStateToProps = (state) => {
    return {
        name: state.profile.name,
        rastreio: state.compras.rastreio,
        status: state.compras.status,
        price: state.compras.price,
        date: state.compras.date,
    }
}

const SuporteConnect = connect(mapStateToProps, {})(Suporte);
export default SuporteConnect;