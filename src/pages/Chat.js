import React, { Component } from 'react';
import { View, StyleSheet, Text, ScrollView, TouchableOpacity, FlatList, TextInput, KeyboardAvoidingView, Image, Platform, Modal, BackHandler } from 'react-native';
import { Icon, Item, Picker, Label, Input } from 'native-base';
import { NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';
import ImageSvg from '../components/ImageSvg';
import SendSvg from '../components/SendSvg';
import MensagemItem from '../components/MensagemItem';
import { sendMessage, monitorChat, sendImage } from '../actions/ChatActions';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'react-native-fetch-blob';

window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest;
window.Blob = RNFetchBlob.polyfill.Blob;

class Chat extends Component {

	constructor(props) {
		super(props);

		this.state = {
			data: this.props.navigation,
			inputText: '',
			foto: null,
			pct:0,
		  	modalVisible: false,
		  	modalImage:null			
		};

		this.back = this.back.bind(this);
		this.enviarMsg = this.enviarMsg.bind(this);
		this.takePhoto = this.takePhoto.bind(this);
	  	this.setModalVisible = this.setModalVisible.bind(this);
	  	this.handleCloseModal = this.handleCloseModal.bind(this);		
		this.imagePress = this.imagePress.bind(this);	
	}

	imagePress(img) {

		let state = this.state;
		state.modalImage = img;
		this.setState(state);

		this.setModalVisible(true);

	}	

	setModalVisible(status) {
		let state = this.state;
		state.modalVisible = status;
		this.setState(state);
	}	

    handleCloseModal () {
    	this.setState({ showModal: false });
	}	

	componentDidMount() {
		this.props.monitorChat(this.state.data.getParam('id'));
	}

	back() {
		const navigateAction = NavigationActions.navigate({
		  routeName: 'HomeTab',

		  action: NavigationActions.navigate({ routeName: 'Message' })
		});

		this.props.navigation.dispatch(navigateAction);				
	}

	enviarMsg() {
		let txt = this.state.inputText;
		let state = this.state;

		this.props.sendMessage('text', txt, this.props.uid, this.state.data.getParam('id'));
		state.inputText = '';
		this.setState(state);
	}	

    takePhoto() {
        const options = {

        };   
             
        ImagePicker.showImagePicker(options, (r) => {
            if(r.uri) {
 				
            	let uri = r.uri.replace('file://', '');

            	RNFetchBlob.fs.readFile(uri, 'base64')
            		.then((data)=>{
            			return RNFetchBlob.polyfill.Blob.build(data, {type:'image/jpeg;BASE64'})
            		})
            		.then((blob)=>{

            			this.props.sendImage(
            				blob,
            				(snapshot) => {
            					let pct = ( snapshot.bytesTransferred / snapshot.totalBytes ) * 100;

            					let state = this.state;
            					state.pct = pct;
            					this.setState(state);

            				},
            			 	(imgName) => {
        					let state = this.state;
        					state.pct = 0;
        					this.setState(state);            			 		
            				this.props.sendMessage('image', imgName, this.props.uid, this.state.data.getParam('id'));
            			}

            		);

            		});

            }
        })
    }	

	render() {		
		let AreaBehavior = Platform.select({ios:'padding', android:null});
		let AreaOffset = Platform.select({ios:64, android:null});

		return (

				<KeyboardAvoidingView style={styles.container} behavior={AreaBehavior} keyboardVerticalOffset={AreaOffset}>
					<View style={styles.titleArea}>
						<TouchableOpacity onPress={this.back}>
							<Icon type="Feather" name="arrow-left" />
						</TouchableOpacity>
				        <Text style={{marginLeft: 10, fontSize: 24, fontWeight: 'bold', color: '#020202'}}>{this.state.data.getParam('title')}</Text>
				        <Icon type="Feather" name="arrow-left" style={{opacity: 0}}/>
				    </View>				
					<View style={styles.chatArea}>
						<FlatList ref={(ref)=>{ this.chatArea = ref }} 
						onContentSizeChange={() => {this.chatArea.scrollToEnd({animated:true})}}
						onLayout={()=>{ this.chatArea.scrollToEnd({animated:true}) }} 
						data={this.props.msgs}						
						renderItem={({item})=><MensagemItem data={item} onImagePress={this.imagePress} me={this.props.uid} />} />						
					</View>
					{this.state.pct > 0 &&
					<View style={styles.imageTmp}>
						<View style={[{width:this.state.pct+'%'}, styles.imageTmpBar]}></View>
					</View>
					}					
					<View style={styles.sendArea}>
						<TouchableOpacity style={styles.imageButton} onPress={this.takePhoto}>
							<ImageSvg />
						</TouchableOpacity>
						<Input value={this.state.inputText} style={styles.sendInput} placeholder="Digite sua mensagem" onChangeText={(inputText)=>this.setState({inputText})} />
						<TouchableOpacity style={styles.sendButton} onPress={this.enviarMsg}>
							<Image style={styles.sendImage} source={require('../assets/send.png')} />
						</TouchableOpacity>
					</View>	

					<Modal animationType="slide" transparent={false} visible={this.state.modalVisible} onRequestClose={this.handleCloseModal}>
							<TouchableOpacity onPress={() => { this.setModalVisible(false) }}>
								<Icon type="Feather" name="arrow-left" style={styles.buttonAbsolute} />
								<Image resizeMode="contain" style={styles.modalImage} source={{uri:this.state.modalImage}} />
							</TouchableOpacity>
					</Modal>					
				</KeyboardAvoidingView>				        
							
		);
	}
}

const styles = StyleSheet.create({
	container:{
		flex: 1,
		backgroundColor: '#FFFFFF'
	},
	backButton:{
		alignSelf: 'flex-start'
	},
	chatArea:{
		flex: 1
	},
	sendArea:{
		height: 50,
		flexDirection: 'row',
		alignItems: 'center',
		borderColor: '#CCC',
		borderWidth: 1
	},
	sendInput:{
		height: 50,
		flex:1
	},
	sendButton:{
		width: 50,
		height: 50,
		justifyContent: 'center',
		alignItems: 'center'
	},
	imageButton:{
		width: 50,
		height: 50,
		justifyContent: 'center',
		alignItems: 'center' 		
	},
	sendImage:{
		width: 25,
		height: 25
	},
	ImageBtnImage:{
		width: 25,
		height: 25
	},
	imageTmp:{
		height: 10
	},	
	imageTmpImage:{
		height: 100,
		width: 100
	},
	imageTmpBar: {
		height: 10
	},
	modalView:{
		backgroundColor: '#000000',
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	},
	modalImage:{	
		width: '100%',
		height: '100%'
	},
	buttonAbsolute:{
		position: 'absolute',
		top: 0,
		left: 0,
		color: '#CCC'
	},
	titleArea:{
		flexDirection: 'row', 
		alignItems: 'center', 
		marginBottom: 20, 
		justifyContent: 'space-between',
		borderBottomWidth: 1, 
		borderBottomColor: '#CCC',
		margin: 5
	}							
});

const mapStateToProps = (state) => {
	return {
		uid: state.auth.id,
		msgs: state.chat.activeChatMessages
	}
}

const ChatConnect = connect(mapStateToProps, { sendMessage, monitorChat, sendImage })(Chat);
export default ChatConnect;