import React, { Component } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import { NavigationActions, StackActions } from 'react-navigation';
import { Icon, Item, Input, Label, Picker } from 'native-base';

class Checkout extends Component {

	constructor(props) {
		super(props);

		this.state = {
			search:'',
			selected: 'key0',
			store:[
				{ key:1,title: 'Geek Store' },
				{ key:1,title: 'Nerd Kart' },
				{ key:1,title: 'Example' }
			],
			list: [
				{id: 1, img:'', name: 'Aline Silveira', username:'@alinesilveira', like: true},
				{id: 2, img:'', name: 'Rodrigo Almeida', username:'@alinesilveira', like: true}
			]			
		};

		this.backGiftMe = this.backGiftMe.bind(this);
	}
	  onValueChange(value: string) {
	    this.setState({
	      selected: value
	    });
	  }	

	backGiftMe() {
		this.props.navigation.goBack();		
	}


	render() {	

		return (
			<ScrollView>
				<View style={styles.container}>
			        <TouchableOpacity onPress={this.backGiftMe} style={styles.backButton}>
			          <Icon type="Feather" name="arrow-left" style={styles.backText} />
			        </TouchableOpacity>
			        <View style={styles.content}>
						<View style={styles.header}>
							<Text style={styles.headerText}>Confirme seu pedido!</Text>
							<Text style={styles.headerName}>Aline Silveira</Text>
						</View>
						<View style={styles.textArea}>
				          <Item floatingLabel style={styles.formItem}>
				            <Label style={styles.formLabel}>CEP</Label>
				            <Input style={styles.formInput} value={this.props.cep} autoCapitalize="none" />
				          </Item>
				          <Item floatingLabel style={styles.formItem}>
				            <Label style={styles.formLabel}>Rua</Label>
				            <Input style={styles.formInput} value={this.props.rua} autoCapitalize="none" />
				          </Item>	
				          <View style={styles.colArea}>
				          	 	<View style={{width: '50%'}}>
						            <Label style={styles.formLabel}>Número</Label>
						            <Input style={styles.formInput} value="121" autoCapitalize="none" />
					          	</View>
					          <View style={{width: '50%'}}>	
					            <Label style={styles.formLabel}>Complemento</Label>
					            <Input style={styles.formInput} value="202" autoCapitalize="none" />				          			          	
					          </View>
				          </View>	
				          <Item floatingLabel style={styles.formItem}>
				            <Label style={styles.formLabel}>Bairro</Label>
				            <Input style={styles.formInput} value={this.props.bairro} autoCapitalize="none" />
				          </Item>
				          <View style={[styles.colArea, {marginLeft: 5}]}>
				          	<View style={{width: '70%'}}>
					            <Label style={styles.formLabel}>Cidade</Label>
					            <Picker
					              mode="dropdown"
					              iosIcon={<Icon name="ios-arrow-down-outline" />}
					              selectedValue={this.state.selected}
					              onValueChange={this.onValueChange.bind(this)}
					            >
					              <Picker.Item label="São Paulo" value="key0" />
					            </Picker>
				            </View>
				          	<View style={{width: '30%'}}>
					            <Label style={styles.formLabel}>UF</Label>
					            <Picker
					              itemStyle={{backgroundColor: '#000000'}}
					              mode="dropdown"
					              iosIcon={<Icon name="ios-arrow-down-outline" />}
					              selectedValue={this.state.selected}
					              onValueChange={this.onValueChange.bind(this)}
					            >
					              <Picker.Item label="SP" value="key0" />
					            </Picker>
				            </View>				            				          	
				          </View>				          				          				          			          
				        </View>
				        <View style={styles.frete}>
				        	<Text style={styles.headerText}>Frete</Text>
				        	<View style={{flexDirection: 'row', alignItems: "center"}}>
					        	<View style={styles.card}>
					        		<Text style={styles.cardTitle}>PAC</Text>
					        		<Text style={styles.cardDays}>15 dias</Text>
					        		<Text style={styles.cardMoney}>R$9,90</Text>
					        	</View>
					        	<View style={styles.card}>
					        		<Text style={styles.cardTitle}>SEDEX</Text>
					        		<Text style={styles.cardDays}>5 dias</Text>
					        		<Text style={styles.cardMoney}>R$19,90</Text>
					        	</View>				        	
				        	</View>
				        </View>
				        <View style={styles.store}>
				        	<Text style={styles.storeTitle}>Geek Store</Text>
				        	<View style={[styles.storeCol, {marginBottom: 10, marginTop: 10}]}>
				        		<Text style={styles.storeColText}>Presente supresa</Text>
				        		<Text style={styles.storeColPrice}>R$ 150,00</Text>
				        	</View>
				        	<View style={[styles.storeCol, {marginBottom: 20}]}>
				        		<Text style={styles.storeColText}>Frete (PAC)</Text>
				        		<Text style={styles.storeColPrice}>R$ 9,90</Text>
				        	</View>
				        	<View style={[styles.storeCol, {marginBottom: 10}]}>
				        		<Text style={styles.storeColText}>Total</Text>
				        		<Text style={styles.storeColPrice}>R$ 159,90</Text>
				        	</View>				        					        	
				        </View>			   
						<TouchableOpacity onPress={() => {}} style={[styles.button, styles.buttonBg]}>
							<Text style={styles.buttonText}>Finalizar</Text>
						</TouchableOpacity>																										        										        			        	
			        </View>
				</View>
			</ScrollView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#FFFFFF'
	},
	button:{
        width: 332,
        height: 54,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        borderRadius: 6,
        marginBottom: 20,
        marginTop: 20
    },
	buttonBg:{
		backgroundColor: '#ff3c36'
	},
	buttonText:{
		fontWeight: 'bold',
		fontSize: 20,
		color: '#FFF'
	},	
	backButton:{
		alignSelf: 'flex-start'
	},
	content:{
		margin: 30
	},
	backText:{
		width: 26,
		height: 26,
		margin: 5,
		color: '#CCC'
	},
	header:{
		marginBottom: 10
	},
	headerText:{
		marginTop: 10,
		fontSize: 14,
		fontWeight: 'bold',
		color: '#3e4a59',
		marginBottom: 10
	},
	headerName:{
		marginTop: 10,
		fontSize: 24,
		fontWeight: 'bold',
		color: '#4760e9'
	},
	textArea:{

	},
    formItem:{
        marginTop: 17
    },	
	formLabel:{
		fontSize: 12
	},
	formInput:{
		fontSize: 14,
		color: '#7b838d',
		fontWeight: 'bold'
	},
	colArea:{
		flexDirection: 'row', 
		marginTop: 17,
		borderBottomWidth: 1,
		borderBottomColor: '#CCC'
	},
	card:{
		width: 141,
		height: 95,
		borderRadius: 20,
	    shadowColor: "#000",
	    shadowOffset: { width: 0, height: 2 },
	    shadowOpacity: 1,
	    shadowRadius: 5.5,
	    elevation: 9,
		justifyContent: 'space-around',
		alignItems: 'center',
		marginRight: 10		
	},
	cardTitle:{
		marginTop: 6,
		fontSize: 16,
		fontWeight: 'bold',
		color: '#3e4a59'
	},
	cardMoney:{
		marginBottom: 3
	},
	store:{
		marginTop: 20
	},
	storeCol:{
		flexDirection: 'row',
		justifyContent: 'space-between'
	},
	storeColText:{
		fontSize: 16,
		fontWeight: 'bold',
		color: '#3e4a59'	
	},
	storeColPrice:{
		fontSize: 14,
		fontWeight: 'bold',
		color: '#68e781'
	},
	storeTitle:{
		fontSize: 24,
		fontWeight: 'bold',
		color: '#3e4a59'
	},
	cumponArea:{
		width: 171,
		height: 50,
		borderRadius: 20,
	    shadowColor: "#000",
	    shadowOffset: { width: 0, height: 2 },
	    shadowOpacity: 1,
	    shadowRadius: 5.5,
	    elevation: 2,
	    flexDirection: 'row',
		justifyContent: 'space-around',
		alignItems: 'center'
	},
	cumponText:{
		
	}
});

const mapStateToProps = (state) => {
	return {
		cep: state.checkout.cep,
		rua: state.checkout.rua,
		numero: state.checkout.numero,
		complemento: state.checkout.complemento,
		bairro: state.checkout.bairro,
		cidade: state.checkout.cidade,
		uf: state.checkout.uf
	}
}

const CheckoutConnect = connect(mapStateToProps, {})(Checkout);
export default CheckoutConnect;