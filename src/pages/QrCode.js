import React, {Component} from 'react';
import {Text, View, StyleSheet, BackHandler, Linking, TouchableOpacity, Modal, Image, Button, ScrollView} from 'react-native';
import {Icon} from 'native-base';
import {connect} from 'react-redux';
import {NavigationActions} from 'react-navigation';
import QRCodeScanner from 'react-native-qrcode-scanner';

class QrCode extends Component {

    constructor(props) {
        super(props);

        this.state = {
            selected: "key0",
            foto: null,
            modalVisible:false,
            camera: true
        };

        this.backProfile = this.backProfile.bind(this);
  }

    onSuccess(e) {
      if(e) {
        this.props.navigation.navigate('ViewMessage', {data: e});
        let state = this.state;
        state.camera = false;
        this.setState(state);
      } else {
        alert('QrCode Invalido');
      }
    }    

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.backProfile);
        let state = this.state;
        state.camera = true;
        this.setState(state);        
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.backProfile);
    }

    backProfile() {
      const navigateAction = NavigationActions.navigate({
        routeName: 'HomeTab',

        action: NavigationActions.navigate({ routeName: 'Message' }),
      });

      this.props.navigation.dispatch(navigateAction); 
      return true;
    }


    render() {
        return (
            <View style={{flex: 1}}>
              {this.state.camera == true &&
              <QRCodeScanner
                cameraStyle={{width: '100%', height: '100%'}}
                showMarker={true}
                onRead={this.onSuccess.bind(this)}
                checkAndroid6Permissions={true}
              />
              }
              {this.state.camera == false &&
                <View></View>
              }
            </View>
        );
    }
}

const styles = StyleSheet.create({
  centerText: {
    flex: 1,
    fontSize: 18,
    padding: 32,
    color: '#777',
  },
  textBold: {
    fontWeight: '500',
    color: '#000',
  },
  buttonText: {
    fontSize: 21,
    color: 'rgb(0,122,255)',
  },
  buttonTouchable: {
    padding: 16,
  },
  avatarImage:{
      justifyContent: 'center',
      alignItems: 'center'
  },
  avatarName:{
    fontWeight: 'bold',
    color: '#3e4a59',
    fontSize: 20,
    marginTop: 2,
    marginBottom: 2
  },
  avatarUser:{
    color: '#9898e3',
    fontSize: 11
  },
  modalContainer:{
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  backButton: {
      alignSelf: 'flex-start'
  },
  backText: {
      width: 26,
      height: 26,
      margin: 5,
      color: '#CCC'
  }
});

const mapStateToProps = (state) => {
    return {

    }
}

const QrCodeConnect = connect(mapStateToProps, {})(QrCode);
export default QrCodeConnect;	