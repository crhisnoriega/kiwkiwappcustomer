import React, { Component } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, ScrollView, Image } from 'react-native';
import { connect } from 'react-redux';
import { NavigationActions, StackActions } from 'react-navigation';
import { Icon, Item, Input, Label, Picker } from 'native-base';
import LikeOn from '../components/likeOn';
import LikeOff from '../components/likeOff';
import Verifysvg from '../components/Verifysvg';

class CheckoutOther extends Component {

	constructor(props) {
		super(props);

		this.state = {
			search:'',
			userCheckOther: this.props.navigation.getParam('back'),
			selected: 'key0',
			store:[
				{ key:1,title: 'Geek Store' },
				{ key:1,title: 'Nerd Kart' },
				{ key:1,title: 'Example' }
			],
			list: [
				// {id: 1, img:'', name: 'Aline Silveira', username:'@alinesilveira', like: true},
				// {id: 2, img:'', name: 'Rodrigo Almeida', username:'@alinesilveira', like: true}
			]			
		};

		this.backGiftMe = this.backGiftMe.bind(this);
	}
	  onValueChange(value: string) {
	    this.setState({
	      selected: value
	    });
	  }	

	backGiftMe() {
		if(this.state.userCheckOther == true){
			const navigateAction = NavigationActions.navigate({
			  routeName: 'HomeTab',

			  action: NavigationActions.navigate({ routeName: 'Gifts' }),
			});

			this.props.navigation.dispatch(navigateAction);				
			this.setState({userCheckOther: false})
		}
		else {
			this.props.navigation.goBack();	
		}
	}


	render() {	

		return (
			<ScrollView>
				<View style={styles.container}>
			        <TouchableOpacity onPress={this.backGiftMe} style={styles.backButton}>
			          <Icon type="Feather" name="arrow-left" style={styles.backText} />
			        </TouchableOpacity>
			        <View style={styles.content}>
						<View style={styles.header}>
							<Text style={styles.headerText}>Confirme seu pedido!</Text>						
						</View>
						<View>
							<TouchableOpacity onPress={() => {}}>
									<View style={styles.cardAvatar}>
										<View style={styles.headerAvatar}>
											<Image source={require('../assets/img.png')} style={styles.img} />
											<View style={{position: 'absolute', top: 7, left: 7 }}>
												<Verifysvg />
											</View>							
										</View>
										<View style={styles.footer}>
											<View>
												<Text style={styles.name}>{this.props.name}</Text>
												<Text style={styles.username}>{this.props.username}</Text>
											</View>
											{this.props.like &&
													<Image source={require('../assets/perfil/heart-ativo.png')} style={{width: 21, height: 18}} />							
											}

											{this.props.like == false &&
													<Image source={require('../assets/perfil/heart.png')} style={{width: 21, height: 18}} />						
											}
										</View>
									</View>
							</TouchableOpacity>							
						</View>
				        <View style={styles.frete}>
				        	<Text style={styles.headerText}>Frete</Text>
				        	<View style={{flexDirection: 'row'}}>
					        	<View style={styles.card}>
					        		<Text style={styles.cardTitle}>PAC</Text>
					        		<Text style={styles.cardDays}>15 dias úteis</Text>
					        		<Text style={styles.cardMoney}>R$9,90</Text>
					        	</View>
					        	<View style={styles.card}>
					        		<Text style={styles.cardTitle}>SEDEX</Text>
					        		<Text style={styles.cardDays}>5 dias úteis</Text>
					        		<Text style={styles.cardMoney}>R$19,90</Text>
					        	</View>				        	
				        	</View>
				        </View>												
				        <View style={styles.store}>
				        	<Text style={styles.storeTitle}>Geek Store</Text>
				        	<View style={[styles.storeCol, {marginBottom: 10, marginTop: 10}]}>
				        		<Text style={styles.storeColText}>Presente supresa</Text>
				        		<Text style={styles.storeColPrice}>R$ 150,00</Text>
				        	</View>
				        	<View style={[styles.storeCol, {marginBottom: 20}]}>
				        		<Text style={styles.storeColText}>Frete (PAC)</Text>
				        		<Text style={styles.storeColPrice}>R$ 9,90</Text>
				        	</View>
				        	<View style={[styles.storeCol, {marginBottom: 10}]}>
				        		<Text style={styles.storeColText}>Total</Text>
				        		<Text style={styles.storeColPrice}>R$ 159,90</Text>
				        	</View>				        					        	
				        </View>			

				        <View style={styles.cumponArea}>
				        	<Text style={{fontSize: 24, color: 'blue', fontWeight: 'bold', marginLeft: 5}}>%</Text>
				        	<Text style={{fontSize: 10, fontWeight: '500', marginRight: 10}}>Cumpon</Text>
				        </View>	   
						<TouchableOpacity onPress={() => {}} style={[styles.button, styles.buttonBg]}>
							<Text style={styles.buttonText}>Finalizar</Text>
						</TouchableOpacity>																										        										        			        	
			        </View>
				</View>
			</ScrollView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#FFFFFF'
	},
	button:{
        width: 332,
        height: 54,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        borderRadius: 6,
        marginBottom: 20,
        marginTop: 20
    },
	buttonBg:{
		backgroundColor: '#ff3c36'
	},
	buttonText:{
		fontWeight: 'bold',
		fontSize: 20,
		color: '#FFF'
	},	
	backButton:{
		alignSelf: 'flex-start'
	},
	content:{
		margin: 30
	},
	backText:{
		width: 26,
		height: 26,
		margin: 5,
		color: '#CCC'
	},
	header:{
		marginBottom: 10
	},
	headerText:{
		marginTop: 10,
		fontSize: 14,
		fontWeight: 'bold',
		color: '#3e4a59',
		marginBottom: 10
	},
	headerName:{
		marginTop: 10,
		fontSize: 24,
		fontWeight: 'bold',
		color: '#4760e9'
	},
	card:{
		width: 141,
		height: 95,
		borderRadius: 20,
	    shadowColor: "#CCC",
	    shadowOffset: { width: 0, height: 2 },
	    shadowOpacity: 1,
	    shadowRadius: 5.5,
	    elevation: 5,
		justifyContent: 'space-around',
		alignItems: 'center',
		marginRight: 10		
	},
	cardTitle:{
		marginTop: 6,
		fontSize: 16,
		fontWeight: 'bold',
		color: '#4760e9'
	},
	cardDays:{
		fontSize: 12,
		fontWeight: '500',
		color: '#3e4a59'
	},
	cardMoney:{
		marginBottom: 3,
		fontSize: 12,
		fontWeight: 'bold',
		color: '#68e781'
	},
	store:{
		marginTop: 20
	},
	storeCol:{
		flexDirection: 'row',
		justifyContent: 'space-between'
	},
	storeColText:{
		fontSize: 16,
		fontWeight: 'bold',
		color: '#3e4a59'	
	},
	storeColPrice:{
		fontSize: 14,
		fontWeight: 'bold',
		color: '#68e781'
	},
	storeTitle:{
		fontSize: 24,
		fontWeight: 'bold',
		color: '#3e4a59'
	},
	cumponArea:{
		width: 171,
		height: 50,
		borderRadius: 20,
	    shadowColor: "#000",
	    shadowOffset: { width: 0, height: 2 },
	    shadowOpacity: 1,
	    shadowRadius: 5.5,
	    elevation: 2,
	    flexDirection: 'row',
		justifyContent: 'space-around',
		alignItems: 'center',
		marginBottom: 10
	},
	cardAvatar:{
		margin: 10,
		height: 160,
		backgroundColor: '#FFF',
		borderRadius: 10,
	    shadowColor: "#000",
	    shadowOffset: { width: 0, height: 2 },
	    shadowOpacity: 1,
	    shadowRadius: 5.5,
	    elevation: 10,
	    width: 143		
	},
	headerAvatar:{
		backgroundColor: '#fdfdfd',
		width: '100%',
		height: 120,
		borderTopLeftRadius: 10,
		borderTopRightRadius: 10,
	},
	img:{
		borderTopLeftRadius: 10,
		borderTopRightRadius: 10,
		width: 143
	},
	footer:{
		flexDirection: 'row',
		justifyContent: 'space-around',
		alignItems: 'center',
		marginTop: 3
	},
	name:{
		fontWeight: 'bold',
		fontSize: 11
	},
	username:{
		color: '#9898e3',
		fontSize: 11
	}
});

const mapStateToProps = (state) => {
	return {
		cep: state.checkout.cep,
		rua: state.checkout.rua,
		numero: state.checkout.numero,
		complemento: state.checkout.complemento,
		bairro: state.checkout.bairro,
		cidade: state.checkout.cidade,
		uf: state.checkout.uf,
		like: state.profile.like,
		name: state.profile.name,
		username: state.profile.username
	}
}

const CheckoutOtherConnect = connect(mapStateToProps, {})(CheckoutOther);
export default CheckoutOtherConnect;