import React, {Component} from 'react';
import {Text, View, StyleSheet, TouchableOpacity, Image, ScrollView, BackHandler} from 'react-native';
import {connect} from 'react-redux';
import {NavigationActions, StackActions} from 'react-navigation';
import BackButton from '../components/BackButton';

class ProfileUser extends Component {

    constructor(props) {
        super(props);

        this.state = {
            info: this.props.navigation.getParam("info")
        };

        this.goHome = this.goHome.bind(this);
        this.abrirStore = this.abrirStore.bind(this);
    }

    async componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.goHome);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.goHome);
    }

    goHome() {
        const navigateAction = NavigationActions.navigate({
            routeName: 'HomeTab',
            action: NavigationActions.navigate({routeName: 'Home'}),
        });

        this.props.navigation.dispatch(navigateAction);
        return true;
    }

    abrirStore() {
        const navigateAction = NavigationActions.navigate({
            routeName: 'HomeTab',
            action: NavigationActions.navigate({routeName: 'Gifts', params: {escolha: 'outra'}}),
        });

        this.props.navigation.dispatch(navigateAction);
    }


    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <BackButton return={() => this.goHome()}/>
                    <TouchableOpacity onPress={() => {
                    }} style={{marginRight: 10, marginTop: 10}}>
                        <Image source={require('../assets/perfil/heart.png')} style={{width: 21, height: 18}} />
                    </TouchableOpacity>
                </View>
                <ScrollView>
                    <View style={styles.avatarImage}>
                        <Image source={{uri: "https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg"}}
                               style={{width: 120, height: 120, borderRadius: 10}}/>
                        <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={styles.avatarName}>{this.state.info.name}</Text>
                        </View>
                        <Text style={styles.avatarUser}>@{this.state.info.login}</Text>
                        <Text style={{fontSize: 12, color: '#b4b8bd'}}>{this.props.qtdRecebidos} Recebidos
                            | {this.props.qtdEnviados} Enviados</Text>
                    </View>
                    <View style={styles.bio}>
                        <View style={{width: '86%', alignSelf: 'center'}}>
                            <Text style={styles.titleBio}>Bio</Text>
                            <Text style={styles.bodyBio}>{this.state.info.spirit.biography}</Text>
                        </View>
                    </View>

                    <View style={styles.grid}>
                        <View style={[styles.col, {alignSelf: 'flex-start'}]}>
                            <View style={styles.colArea}>
                                <Text style={styles.colTitle}>Signo</Text>
                                <Text style={styles.colText}>{this.state.info.spirit.horoscopo}</Text>
                            </View>
                            <View style={styles.colArea}>
                                <Text style={styles.colTitle}>Foco atual</Text>
                                <Text style={styles.colText}>{this.state.info.spirit.focus_activity}</Text>
                            </View>
                            <View style={styles.colArea}>
                                <Text style={styles.colTitle}>Pessoa generosa</Text>
                                <Text style={styles.colText}>{this.state.info.spirit.genre}</Text>
                            </View>
                        </View>
                        <View style={styles.col}>
                            <View style={styles.colArea}>
                                <Text style={styles.colTitle}>Estilo musical</Text>
                                <Text style={styles.colText}>{this.state.info.spirit.music_style[0]}</Text>
                            </View>
                            <View style={styles.colArea}>
                                <Text style={styles.colTitle}>Pessoa romântica</Text>
                                <Text style={styles.colText}>{this.state.info.spirit.romance}</Text>
                            </View>
                            <View style={styles.colArea}>
                                <Text style={styles.colTitle}>Interesses</Text>
                                <View style={{flexDirection: 'row', flexWrap: 'wrap', width: '99%'}}>
                                    {this.state.info.spirit.interests.map(value => {
                                        return(
                                            <Text style={styles.tagText}>{value}</Text>
                                        )
                                    })}
                                </View>
                            </View>
                        </View>
                    </View>
                    <TouchableOpacity onPress={this.abrirStore} style={[styles.button, styles.buttonBg]}>
                        <Text style={styles.buttonText}>Presentear</Text>
                    </TouchableOpacity>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFFFFF'
    },
    header: {
        justifyContent: 'flex-end',
        flexDirection: 'row',
        alignItems: 'flex-end',
        marginBottom: 30
    },
    settings: {
        width: 30,
        height: 30,
        marginRight: 10,
        marginTop: 10,
        color: '#b2b7bd'
    },
    avatarImage: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    avatarName: {
        fontWeight: 'bold',
        color: '#3e4a59',
        fontSize: 20,
        marginTop: 2,
        marginBottom: 2
    },
    avatarUser: {
        color: '#9898e3',
        fontSize: 11
    },
    bio: {
        width: '89%',
        marginTop: 30,
        marginBottom: 30,
        alignSelf: 'center'
    },
    titleBio: {
        color: '#3e4a59',
        fontWeight: 'bold'
    },
    bodyBio: {
        textAlign: 'justify',
        color: '#b1b9c2'
    },
    grid: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        marginTop: 20
    },
    tagArea: {
        flexWrap: 'wrap',
        flexDirection: 'row',
        width: '99%'
    },
    tag: {
        backgroundColor: '#445ee9',
        borderRadius: 10,
        marginBottom: 3,
        marginRight: 3
    },
    tagText: {
        color: '#FFF',
        backgroundColor: '#445ee9',
        fontSize: 11,
        paddingRight: 7,
        paddingBottom: 3,
        paddingLeft: 7,
        paddingTop: 3,
        borderRadius: 20,
        marginBottom: 5,
        marginRight: 5
    },
    colArea: {
        marginBottom: 10
    },
    colTitle: {
        marginBottom: 3,
        color: '#3e4a59',
        fontWeight: 'bold'
    },
    colText: {
        color: '#b1b9c2'
    },
    button: {
        width: 332,
        height: 54,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        borderRadius: 6,
        marginBottom: 20,
        marginTop: 20
    },
    buttonBg: {
        backgroundColor: '#ff3c36'
    },
    buttonText: {
        fontWeight: 'bold',
        fontSize: 20,
        color: '#FFF'
    },
    backText: {
        width: 26,
        height: 26,
        margin: 5,
        color: '#CCC'
    },
    col: {
        marginLeft: 47
    }
});

const mapStateToProps = (state) => {
    return {
        status: state.auth.status,
        name: state.profile.name,
        qtdRecebidos: state.profile.qtdRecebidos,
        qtdEnviados: state.profile.qtdEnviados,
        username: state.profile.username,
        bio: state.profile.bio,
        signo: state.profile.signo,
        usuario: state.profile.usuario,
        focoatual: state.profile.focoatual,
        generosa: state.profile.generosa,
        romantica: state.profile.romantica,
        musical: state.profile.musical
    }
};

const ProfileUserConnect = connect(mapStateToProps, {})(ProfileUser);
export default ProfileUserConnect;