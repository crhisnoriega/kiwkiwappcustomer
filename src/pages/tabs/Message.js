import React, { Component } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, FlatList, BackHandler, Image, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import { Icon } from 'native-base';
import { NavigationActions, StackActions } from 'react-navigation';
import PresenteadoSvg from '../../components/PresenteadoSvg';
import ChatVazio from '../../components/ChatVazio';
import ChatList from '../../components/ChatList';
import { criarChat, getChatList } from '../../actions/ChatActions';

class Message extends Component {

	constructor(props) {
		super(props);

		this.state = {
			msgs: [

			],
			loading: true
		};

		this.contatoClick = this.contatoClick.bind(this);
		this.openCamera = this.openCamera.bind(this);
		this.conversaClick = this.conversaClick.bind(this);
		this.criarConversa = this.criarConversa.bind(this);		
	}

    componentDidMount() {
	    BackHandler.addEventListener('hardwareBackPress', () => {
	      this.props.navigation.goBack(); // works best when the goBack is async
	      return true;
	    });
		this.props.getChatList(this.props.uid, () => {
			this.setState({loading:false});
		});	    
	}	

	componentDidUnmount() {
		this.setState({loading:true});
	}	

	criarConversa() {
		this.props.criarChat(this.props.uid, 2);
	}


	contatoClick(item) {
		this.props.navigation.dispatch(StackActions.reset({
			index:0,
			actions:[
				NavigationActions.navigate({routeName:'Chat'})
			]
		}));
	}	

	openCamera() {
		this.props.navigation.dispatch(StackActions.reset({
			index:0,
			actions:[
				NavigationActions.navigate({routeName:'QrCode'})
			]
		}));		
	}

	conversaClick(data) {
		this.props.navigation.dispatch(StackActions.reset({
			index:0,
			actions:[
				NavigationActions.navigate({routeName:'Chat', params: {title:data.otherName, 'id': data.id}})
			]
		}));	
	}

	render() {

		return (
			<View style={styles.container}>
				<View style={styles.content}>
					<Text style={styles.msgText}>Mensagens</Text>
					<TouchableOpacity onPress={this.openCamera}>
						<View style={styles.button}>
							<View style={{marginRight: 5}}>
								<Image source={require('../../assets/plus.png')} style={{width: 15, height: 15}} />
							</View>
							<Text style={styles.buttonText}>Fui presenteado</Text>
						</View>
					</TouchableOpacity>
					{this.state.loading && <ActivityIndicator size="large" />}
				</View>
					<View>
					{this.props.chats.length == 0 &&
						<View style={{alignSelf: 'center'}}>
							<Image source={require('../../assets/chat-vazio.png')} style={{width: 250,height: 250}} />
						</View>
					}

						<View>							
							<FlatList 
								data={this.props.chats}
								renderItem={({item}) => <ChatList data={item} onPress={this.conversaClick} /> }
							/>
						</View>
					</View>		
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#FFFFFF'
	},	
	content:{
		margin: 30,
		justifyContent: 'center',
		alignItems: 'center'		
	},
	button:{
		flexDirection: 'row', 
		justifyContent: 'center', 
		alignItems: 'center',
		backgroundColor: '#FFF',
		width: 150,
		height: 35,
		borderRadius: 10,
	    shadowColor: "#000",
	    shadowOffset: { width: 0, height: 2 },
	    shadowOpacity: 1,
	    shadowRadius: 5.5,
	    elevation: 10		
	},
	buttonText:{
		color: '#325eed'
	},
	buttonIcon:{
		color: '#FFF', 
		marginRight: 5, 
		fontSize: 15, 
		backgroundColor: '#325eed', 
		borderRadius: 20
	},
	msgText:{
		marginBottom: 40,
		marginTop: 40,
		color: '#3e4a59',
		fontWeight: 'bold',
		fontSize: 24
	}
});

const mapStateToProps = (state) => {
	return {	
		uid: state.auth.id,
		chats: state.chat.chats
	}
}

const MessageConnect = connect(mapStateToProps, {criarChat, getChatList})(Message);
export default MessageConnect;