import React, { Component } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, FlatList, BackHandler, Image } from 'react-native';
import { connect } from 'react-redux';
import { Icon } from 'native-base';
import NotificationItem from '../../components/NotificationItem';

class Notification extends Component {

	constructor(props) {
		super(props);

		this.state = {
			list: [

			]
		};
	}

    componentDidMount() {
	    BackHandler.addEventListener('hardwareBackPress', () => {
	      this.props.navigation.goBack(); // works best when the goBack is async
	      return true;
	    });
	}	

	render() {

		return (
			<View style={this.state.list == '' ? styles.container2 : styles.container}>
				<View style={styles.content}>
					<View style={styles.cardNotification}>
						{this.state.list.length > 0 &&
							<FlatList data={this.state.list} renderItem={({item}) => <NotificationItem data={item} /> } />
						}

						{this.state.list.length == '' &&
							<View style={styles.notiEmpty}>
								<View style={{marginBottom: 40}}>
									<Text style={styles.notiText}>Você não possui</Text>
									<Text style={styles.notiText}>nenhuma nova notificação</Text>
								</View>
								<View>
									<Image source={require('../../assets/ballons.png')} style={{width: 320, height: 268}} />
								</View>
							</View>
						}
					</View>
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#FFFFFF'
	},
	container2:{
		flex: 1,
		backgroundColor: '#FFFFFF',
		justifyContent: 'center',
		alignItems: 'center'
	},
	content:{
		margin: 25
	},
	notiEmpty:{

	},
	notiText:{
		fontSize: 20,
		fontWeight: 'bold',
		textAlign: 'center',
		color: '#46515f'
	}
});

const mapStateToProps = (state) => {
	return {
		
	}
}

const NotificationConnect = connect(mapStateToProps, {})(Notification);
export default NotificationConnect;