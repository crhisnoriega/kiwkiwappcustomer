import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  Platform,
  ScrollView,
  KeyboardAvoidingView,
  TextInput,
  Alert,
  FlatList
} from "react-native";
import { connect } from "react-redux";

import * as Http from "../../util/api";

import { ListItem, SearchBar } from "react-native-elements";

import {
  RNSlidingButton,
  SlideDirection
} from "../../components/RNSlidingButton";

import {
  Container,
  Header,
  Item,
  Input,
  Icon,
  Button,
  Text2,
  List
} from "native-base";

import FeedItem from "../../components/FeedItem";
import PickerProductDescription from "../../components/PickerProductDescription";

import { NavigationActions, StackActions } from "react-navigation";
import {
  requestMyProfile,
  handlerId,
  handlerLogin,
  handlerPassword,
  handlerName,
  handlerEmail,
  handlerAccount,
  handlerCreatedAt,
  handler_v,
  handlerToken,
  handlerTokenFirebase,
  handlerSpirit
} from "../../actions/MyProfileAction";
import {
  ADD_PROFILES,
  handlerData,
  searchProfile
} from "../../actions/HomeActions";
import Spinner from "react-native-spinkit";

//var bcrypt = require('bcryptjs');

class Menu extends Component {
  constructor(props) {
    super(props);

    this.itemQtde = {};
    this.itemDescr = {};

    this.state = {
      search: "",
      list: [],
      loading: true,
      username: "",
      pesquisa: false,
      data: [],
      products: [],
      menuTitle: ""
    };

    this.busca = this.busca.bind(this);
    this.openProfile = this.openProfile.bind(this);

    window.rootTabNavigator = this.props.navigation;
  }

  componentDidMount() {
    Http.get("/product/categories?establishmentid=string2", {}, result => {
      this.setState({ data: result.data });
    });
  }

  busca(event) {
    this.setState({ search: event.nativeEvent.text });
  }

  openProfile(data) {
    this.props.navigation.dispatch(
      StackActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({
            routeName: "ProfileUser",
            params: { info: data }
          })
        ]
      })
    );
  }

  setText(txt) {
    if (txt === "") {
      this.setState({ display: 1 });
    } else {
      this.setState({ display: 0 });
    }
  }

  onSlideRight = () => {
    Alert.alert("Pagamento efetuado com sucesso");
    this.onCreateAndSendOrder();
  };

  onItemSelect = item => {
    Http.get(
      "/product/search?query=%25%25&categoryid=" + item.id,
      {},
      result => {
        this.setState({ products: result.data });
      }
    );
    this.setState({ menuTitle: item.name });
  };

  onCounterChange = (item, value) => {
    this.itemQtde[item.id] = value;
    this.itemDescr[item.id] = item;
  };

  onCreateAndSendOrder = () => {
    var order = {};
    order.itens = [];

    Object.keys(this.itemDescr).forEach(productId => {
      var product = this.itemDescr[productId];
      var qtde = this.itemQtde[productId];

      if (qtde != 0) {
        order.itens.push({
          productId: product.id,
          name: product.name,
          unit: qtde,
          unitPrice: product.price,
          quantity: qtde,
          totalPrice: product.price * qtde
        });
      }
    });

    order.ownerUser = "tester";
    order.date = new Date();

    Http.post("/orders", order, {}, result => {});
  };

  render() {
    var itens = this.state.products.map(product => {
      return (
        <PickerProductDescription
          product={product}
          onCounterChange={this.onCounterChange}
        />
      );
    });

    return (
      <View style={styles.container}>
        <View style={styles.headerArea}>
          <Item
            rounded
            error={this.props.errorPass}
            success={this.props.successPass}
            style={styles.myinput}
          >
            <Icon name="ios-arrow-back" style={{ color: "#FA807C" }} />

            <Icon name="ios-search" style={{ color: "#FA807C" }} />
            <Input
              value={this.props.password}
              tintColor="#FFF"
              onChangeText={this.props.changePassword}
              secureTextEntry={this.props.PassHide}
              autoCapitalize="none"
              getRef={input => {
                this.passInputRef = input;
              }}
              onSubmitEditing={() => this.requestLogin()}
              placeholder="Bar ou balada"
              textAlign={"center"}
            />
            <Icon name="ios-menu" style={{ color: "#FA807C" }} />
          </Item>
        </View>

        <View style={styles.titleArea}>
          <Text style={{ fontSize: 24 }}>Nome do Bar</Text>
        </View>

        <View style={styles.titleAreaRow}>
          <FlatList
            horizontal={true}
            data={this.state.data}
            renderItem={({ item }) => (
              <TouchableOpacity onPress={() => this.onItemSelect(item)}>
                <ListItem
                  hideChevron
                  noBorder
                  style={{ borderBottomWidth: 0 }}
                  title={item.name}
                />
              </TouchableOpacity>
            )}
          />
        </View>

        <View style={styles.formArea}>
          <Text>{this.state.menuTitle}</Text>
          <ScrollView style={{ width: "90%" }}>{itens}</ScrollView>
        </View>

        <View style={styles.bottomArea}>
          <TouchableOpacity
            style={{
              height: 50,
              marginRight: 5,
              marginLeft: 10,
              width: "50%",
              alignItems: "center",
              justifyContent: "center",
              alignSelf: "center",
              borderRadius: 30,
              backgroundColor: "#F9817B"
            }}
            disabled={this.state.buttonLogin}
          >
            <Text style={styles.buttonText}>Limpar</Text>
          </TouchableOpacity>

          <RNSlidingButton
            style={[
              { width: "50%", marginLeft: 5, borderRadius: 30, marginLeft: 10 },
              styles.buttonBg
            ]}
            height={50}
            slideDirection={SlideDirection.RIGHT}
            onSlidingSuccess={this.onSlideRight}
          >
            <View>
              <Icon
                name="ios-play"
                style={{ marginLeft: 50, color: "white" }}
              />
            </View>
          </RNSlidingButton>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#e5e5e5"
  },

  headerArea: {
    height: 110,
    marginRight: 10,
    marginLeft: 10,
    marginTop: 5,
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 8,
    backgroundColor: "white"
  },

  titleArea: {
    height: 70,
    width: "95%",
    alignItems: "center",
    justifyContent: "center",
    marginLeft: 10,
    marginRight: 10,
    marginTop: 5,
    borderRadius: 8,
    backgroundColor: "white"
  },

  titleAreaRow: {
    height: 80,
    width: "95%",
    justifyContent: "space-between",
    alignItems: "center",
    marginLeft: 10,
    marginRight: 10,
    marginTop: 5,
    borderRadius: 8,
    backgroundColor: "white",
    flexDirection: "row"
  },

  formArea: {
    flex: 5,
    width: "95%",
    alignItems: "center",
    justifyContent: "flex-start",
    margin: 10,
    borderRadius: 8,
    backgroundColor: "white"
  },

  descriptionArea: {
    flex: 2,
    width: "95%",
    alignItems: "center",
    justifyContent: "center",
    margin: 10,
    borderRadius: 8,
    backgroundColor: "white"
  },

  bottomArea: {
    height: 60,
    width: 200,
    width: "95%",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 8,
    backgroundColor: "white",
    flexDirection: "row"
  },

  myinput: {
    alignItems: "center",
    justifyContent: "end",
    backgroundColor: "#e5e5e5",
    width: "100%",
    height: 29,
    fontSize: 20,
    fontWeight: "bold"
  },

  myinputicon: {
    height: 25
  },
  button: {
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "center",
    borderRadius: 30,
    marginBottom: 5
  },
  buttonBg: {
    backgroundColor: "#FA807C"
  },
  buttonText: {
    fontWeight: "bold",
    fontSize: 15,
    color: "#FFF"
  },
  titleText: {
    fontSize: 17,
    fontWeight: "normal",
    textAlign: "center",
    color: "#ffffff"
  }
});

const mapStateToProps = state => {
  return {
    status: state.auth.status,
    nome: state.MyProfile.name,
    list: state.Home.list,
    search: state.Home.search
  };
};

const MenuConnect = connect(
  mapStateToProps,
  {
    requestMyProfile,
    handlerId,
    handlerLogin,
    searchProfile,
    handlerPassword,
    handlerName,
    handlerEmail,
    handlerAccount,
    handlerCreatedAt,
    handler_v,
    handlerToken,
    handlerTokenFirebase,
    handlerSpirit,
    ADD_PROFILES,
    handlerData
  }
)(Menu);
export default MenuConnect;
