import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  Platform,
  ScrollView,
  KeyboardAvoidingView,
  TextInput,
  Alert
} from "react-native";
import { connect } from "react-redux";

import {
  RNSlidingButton,
  SlideDirection
} from "../../components/RNSlidingButton";

import {
  Container,
  Header,
  Item,
  Input,
  Icon,
  Button,
  Text2,
  List
} from "native-base";

import FeedItem from "../../components/FeedItem";
import PickerProductItem from "../../components/PickerProductItem";

import { NavigationActions, StackActions } from "react-navigation";
import {
  requestMyProfile,
  handlerId,
  handlerLogin,
  handlerPassword,
  handlerName,
  handlerEmail,
  handlerAccount,
  handlerCreatedAt,
  handler_v,
  handlerToken,
  handlerTokenFirebase,
  handlerSpirit
} from "../../actions/MyProfileAction";
import {
  ADD_PROFILES,
  handlerData,
  searchProfile
} from "../../actions/HomeActions";
import Spinner from "react-native-spinkit";

//var bcrypt = require('bcryptjs');

class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      search: "",
      list: [],
      loading: true,
      username: "",
      pesquisa: false
    };

    this.busca = this.busca.bind(this);
    this.openProfile = this.openProfile.bind(this);

    window.rootTabNavigator = this.props.navigation;
  }

  async componentWillMount() {
    this.setState({
      username: await AsyncStorage.getItem("Username")
    });
  }

  async componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", () => {
      return true;
    });

    this.props.ADD_PROFILES(result => {
      this.props.handlerData(result);

      this.setState({
        loading: false
      });
    });
  }

  busca(event) {
    this.setState({ search: event.nativeEvent.text });
  }

  openProfile(data) {
    this.props.navigation.dispatch(
      StackActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({
            routeName: "ProfileUser",
            params: { info: data }
          })
        ]
      })
    );
  }

  setText(txt) {
    if (txt === "") {
      this.setState({ display: 1 });
    } else {
      this.setState({ display: 0 });
    }
  }

  onSlideRight = () => {
    Alert.alert("Pagamento efetuado com sucesso");
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.headerArea}>
          <Item
            rounded
            error={this.props.errorPass}
            success={this.props.successPass}
            style={styles.myinput}
          >
            <Icon name="ios-arrow-back" style={{ color: "#FA807C" }} />

            <Icon name="ios-search" style={{ color: "#FA807C" }} />
            <Input
              value={this.props.password}
              tintColor="#FFF"
              onChangeText={this.props.changePassword}
              secureTextEntry={this.props.PassHide}
              autoCapitalize="none"
              getRef={input => {
                this.passInputRef = input;
              }}
              onSubmitEditing={() => this.requestLogin()}
              placeholder="Bar ou balada"
              textAlign={"center"}
            />
            <Icon name="ios-menu" style={{ color: "#FA807C" }} />
          </Item>
        </View>

        <View style={styles.titleArea}>
          <Text style={{ fontSize: 24 }}>Nome do Bar</Text>
        </View>

        <View style={[styles.titleAreaRow, { backgroundColor: "#FA807C" }]}>
          <Image
            source={require("../../assets/navbar/cesta.png")}
            style={{
              width: 35,
              height: 30,
              marginLeft: 20,
              justifyContent: "flex-start",
              color: "white",
              tintColor: "white"
            }}
          />
          <Text style={{ color: "white", fontSize: 30, fontWeight: "bold" }}>
            Minha Cesta
          </Text>
          <Image
            source={require("../../assets/navbar/cesta.png")}
            style={{
              width: 35,
              height: 30,
              marginRight: 20,
              justifyContent: "flex-end",
              color: "white",
              tintColor: "white"
            }}
          />
        </View>

        <View style={styles.formArea}>
          <ScrollView style={{ width: "90%" }}>
            <PickerProductItem style={{ width: "90%" }} />
            <PickerProductItem style={{ width: "90%" }} />
            <PickerProductItem style={{ width: "90%" }} />
          </ScrollView>
        </View>

        <View style={styles.bottomArea}>
          <TouchableOpacity
            style={{
              height: 50,
              marginRight: 5,
              marginLeft: 10,
              width: "50%",
              alignItems: "center",
              justifyContent: "center",
              alignSelf: "center",
              borderRadius: 30,
              backgroundColor: "#F9817B"
            }}
            disabled={this.state.buttonLogin}
          >
            <Text style={styles.buttonText}>Limpar</Text>
          </TouchableOpacity>

          <RNSlidingButton
            style={[
              { width: "50%", marginLeft: 5, borderRadius: 30, marginLeft: 10 },
              styles.buttonBg
            ]}
            height={50}
            slideDirection={SlideDirection.RIGHT}
            onSlidingSuccess={this.onSlideRight}
          >
            <View>
              <Icon
                name="ios-play"
                style={{ marginLeft: 50, color: "white" }}
              />
            </View>
          </RNSlidingButton>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#e5e5e5"
  },

  headerArea: {
    height: 110,
    marginRight: 10,
    marginLeft: 10,
    marginTop: 5,
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 8,
    backgroundColor: "white"
  },

  titleArea: {
    height: 70,
    width: "95%",
    alignItems: "center",
    justifyContent: "center",
    marginLeft: 10,
    marginRight: 10,
    marginTop: 5,
    borderRadius: 8,
    backgroundColor: "white"
  },

  titleAreaRow: {
    height: 80,
    width: "95%",
    justifyContent: "space-between",
    alignItems: "center",
    marginLeft: 10,
    marginRight: 10,
    marginTop: 5,
    borderRadius: 8,
    backgroundColor: "white",
    flexDirection: "row"
  },

  formArea: {
    flex: 5,
    width: "95%",
    alignItems: "center",
    justifyContent: "flex-start",
    margin: 10,
    borderRadius: 8,
    backgroundColor: "white"
  },

  descriptionArea: {
    flex: 2,
    width: "95%",
    alignItems: "center",
    justifyContent: "center",
    margin: 10,
    borderRadius: 8,
    backgroundColor: "white"
  },

  bottomArea: {
    height: 60,
    width: 200,
    width: "95%",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 8,
    backgroundColor: "white",
    flexDirection: "row"
  },

  myinput: {
    alignItems: "center",
    justifyContent: "end",
    backgroundColor: "#e5e5e5",
    width: "100%",
    height: 29,
    fontSize: 20,
    fontWeight: "bold"
  },

  myinputicon: {
    height: 25
  },
  button: {
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "center",
    borderRadius: 30,
    marginBottom: 5
  },
  buttonBg: {
    backgroundColor: "#FA807C"
  },
  buttonText: {
    fontWeight: "bold",
    fontSize: 15,
    color: "#FFF"
  },
  titleText: {
    fontSize: 17,
    fontWeight: "normal",
    textAlign: "center",
    color: "#ffffff"
  }
});

const mapStateToProps = state => {
  return {
    status: state.auth.status,
    nome: state.MyProfile.name,
    list: state.Home.list,
    search: state.Home.search
  };
};

const HomeConnect = connect(
  mapStateToProps,
  {
    requestMyProfile,
    handlerId,
    handlerLogin,
    searchProfile,
    handlerPassword,
    handlerName,
    handlerEmail,
    handlerAccount,
    handlerCreatedAt,
    handler_v,
    handlerToken,
    handlerTokenFirebase,
    handlerSpirit,
    ADD_PROFILES,
    handlerData
  }
)(Home);
export default HomeConnect;
