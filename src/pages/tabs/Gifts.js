import React, { Component } from 'react';
import { Text, View, Image, StyleSheet, TouchableOpacity, Alert, FlatList, ScrollView, BackHandler, Modal, TextInput, KeyboardAvoidingView, Platform, Keyboard } from 'react-native';
import { connect } from 'react-redux';
import { NavigationActions, StackActions } from 'react-navigation';
import { Icon, Item, Input, Label, Textarea, Container } from 'native-base';
import StoreItem from '../../components/gift/StoreItem';
import FeedItemStore from '../../components/gift/FeedItemStore';
import EstiloItem from '../../components/gift/EstiloItem';
import ImgItem from '../../components/gift/ImgItem';
import GiftStore from '../../components/gift/GiftStore';
import AnimeItem from '../../components/gift/AnimeItem';
import GameItem from '../../components/gift/GameItem';
import { changeMsg, changeName, changeGenero, changeIdade } from '../../actions/GiftActions';
import Verifysvg from '../../components/Verifysvg';
import LikeOn from '../../components/likeOn';

class Gifts extends Component {

	constructor(props) {
		super(props);

		this.state = {
			search:'',
			store:[
				{ key:1,title: 'Geek Store', checked: false },
				{ key:2,title: 'Nerd Kart', checked: false },
				{ key:3,title: 'Example', checked: false }
			],
			anime:[
				{key:1, name:'Naruto', checked: false},
				{key:2, name:'Attack on Titan', checked: false},
				{key:3, name:'Bleach', checked: false},
				{key:4, name:'Dragon Ball Z', checked: false},
				{key:5, name:'Cavaleiros do Zodiaco', checked: false},
				{key:6, name:'One Piece', checked: false},
				{key:7, name:'Hunter x Hunter', checked: false},
				{key:8, name:'Outro', checked: false}
			],
			game:[
				{key: 1, name: 'WOW', checked: false}
			],
			img: [
				{key:1},
				{key:1},
				{key:1},
				{key:1}
			],			
			list: [
				{id: 1, img:'', name: 'Aline Silveira', username:'@alinesilveira', like: true, genero: 'Feminino'},
				{id: 2, img:'', name: 'Rodrigo Almeida', username:'@rodrigoalmeida', like: true, genero: 'Masculino'}
			],
			tagEstilo: [
				{key: 1, name:'Musica', checked: false},
				{key: 2, name:'Leitura', checked: false},
				{key: 3, name:'Geek & Nerd', checked: false},
				{key: 4, name:'Flores', checked: false},
				{key: 5, name:'Papelaria', checked: false},
				{key: 6, name:'Decoração', checked: false},
				{key: 7, name:'Beleza', checked: false},
				{key: 8, name:'Doces', checked: false}
			],
			msg: [
				{key: 1, name: 'Parabéns', checked: false}
			],
			price: [
				{key: 1, name: '100,00', checked: false},
				{key: 2, name: '150,00', checked: false},
				{key: 3, name: '200,00', checked: false}
			],
			presenteado: this.props.navigation.getParam('escolha'),
			bgGift1: '#FFF',
			bgGift2: '#FFF',
			colorMe: '#3e4a59',
			colorOther: '#3e4a59',
			found: '',
			genero: 'none',
			bgNome: '#FFF',
			nome: '#3e4a59',
			idade: 'none',
			colorGenero: '#3e4a59',
			bgGenero: '#FFF',
			bgIdade: '#FFF',
			colorIdade: '#3e4a59',
			style: 'none',
			selecionado: '',
			storeView: 'none',
			animeFav: 'none',
			modalVisible:false,
			titleStore: '',
			gameLike: 'none',
			msgSpecial: 0,
			buttonView: 'none',
			priceView: 'none',
			style2: 'flex',
			selectEstilo: '',
			selectAnime: '',
			selectGame: '',
			selectMsg: '',
			selectPrice: '',
			selectStore: '',
			borderStore: '#445ee9',
			keyStore: '',
			selectFeed: '',
			borderFeed: '#445ee9',
			colorNameFound: '#3e4a59',
			displayFound: 'none',
			width: 0,
			widthIdade: 50,
			widthGenero: 70,
			NameFocus: false,
			FeedSearch: 'flex'
		};

		this.backProfile = this.backProfile.bind(this);
		this.busca = this.busca.bind(this);
		this.goStore = this.goStore.bind(this);
		this.goCheckoutOther = this.goCheckoutOther.bind(this);
		this.giftMe = this.giftMe.bind(this);
		this.giftOther = this.giftOther.bind(this);
		this.notfound = this.notfound.bind(this);
		this.setGenero = this.setGenero.bind(this);
		this.setIdade = this.setIdade.bind(this);
		this.setStyle = this.setStyle.bind(this);		
		this.style = this.style.bind(this);
		this.selectStyle = this.selectStyle.bind(this);
		this.removeStyle = this.removeStyle.bind(this);
		this.selecionar = this.selecionar.bind(this);
		this.selecionarLoja = this.selecionarLoja.bind(this);
	    this.abrirModal = this.abrirModal.bind(this);
	    this.fecharModal = this.fecharModal.bind(this);
	    this.handleCloseModal = this.handleCloseModal.bind(this);
	    this.selectAnime = this.selectAnime.bind(this);
	    this.selectGame = this.selectGame.bind(this);
	    this.selectMsg = this.selectMsg.bind(this);
	    this.selectPrice = this.selectPrice.bind(this);
	    this.selectStore = this.selectStore.bind(this);
	    this.selectFeed = this.selectFeed.bind(this);
	}

	selectFeed(item) {
	   let s = this.state;
	   data = s.list;
	   s.selectFeed =  item;
	   for (var i in data) {
	     if (data[i].id == item) {
		        data[i].checked = true;		        
		        break;
		     }
		}
		s.list = data;
		s.borderFeed = 'red';
		s.selectEstilo = '';
		s.storeView = 'none';
		s.style = 'flex';
		s.animeFav = 'none'; 
		s.gameLike = 'none'; 
		s.buttonView = 'none';
		s.priceView ='none';
		s.genero ='none';
		s.idade ='none'; 
		s.buttonView = 'none';
		s.msgSpecial = 0; 
		s.selectStore = '';
		s.selectAnime = '';
		s.selectGame = '';
		s.selectMsg = '';
		s.selectPrice = '';					
		this.setState(s);	
	}

	selectStore(item) {
	   let s = this.state;
	   data = s.store;
	   s.selectStore =  item;
	   for (var i in data) {
	     if (data[i].key == item) {
		        data[i].checked = true;		        
		        break;
		     }

		}
		s.store = data;
		s.borderStore = 'red';
		s.animeFav = 'flex';
		s.gameLike = 'none'; 
		s.buttonView = 'none';
		s.priceView ='none';
		s.genero ='none';
		s.idade ='none'; 		
		s.selectAnime = '';
		s.selectGame = '';
		s.selectMsg = '';
		s.selectPrice = '';			
		this.setState(s);	
		this.fecharModal();	
	}

	selectPrice(item) {
	   let s = this.state;
	   data = s.price;
	   s.selectPrice =  item;
	   for (var i in data) {
	     if (data[i].key == item) {
		        data[i].checked = true;		        
		        break;
		     }
		}
		s.price = data;
		s.buttonView = 'flex';
		this.setState(s);	

	}

	selectMsg() {
	   let s = this.state;
	   s.priceView = 'flex';
	   this.setState(s);		
	}

	selectStyle(item) {
	   let s = this.state;
	   if(this.state.selectEstilo.length != ''){
			s.selectAnime = '';
			s.selectStore = '',
			s.selectGame = '';
			s.selectMsg = '';
			s.selectPrice = '';
			s.storeView = 'none';
			s.animeFav = 'none';
			s.msgSpecial = 0;
			s.buttonView = 'none';
			s.priceView = 'none';	
		   data = s.tagEstilo;
		   s.selectEstilo =  item;
		   for (var i in data) {
		     if (data[i].key == item) {
			        data[i].checked = true;		        
			        break;
			     }
			}
			s.tagEstilo = data;
			s.storeView = 'flex';			
	   } else {
		   data = s.tagEstilo;
		   s.selectEstilo =  item;
		   for (var i in data) {
		     if (data[i].key == item) {
			        data[i].checked = true;		        
			        break;
			     }
			}
			s.tagEstilo = data;
			s.storeView = 'flex';	   	
	   }
	   this.setState(s);
	}	

	selectAnime(item) {
	   let s = this.state;
	   data = s.anime;
	   s.selectAnime =  item;
	   for (var i in data) {
	     if (data[i].key == item) {
		        data[i].checked = true;		        
		        break;
		     }
		}
		s.anime = data;
		s.gameLike = 'flex';
		this.setState(s);
	}

	selectGame(item) {
	   let s = this.state;
	   data = s.game;
	   s.selectGame =  item;
	   for (var i in data) {
	     if (data[i].key == item) {
		        data[i].checked = true;		        
		        break;
		     }
		}
		s.game = data;
		s.msgSpecial = 1;
		this.setState(s);
	}			

	abrirModal(item) {
	    let s = this.state;
	    s.titleStore = item.title;
	    s.keyStore = item.key;
	    s.modalVisible = true;
	    this.setState(s);
	}

	fecharModal() {
	    let s = this.state;
	    s.modalVisible = false;
	    this.setState(s);
	}

	handleCloseModal () {
	    this.setState({ showModal: false });
	}	

    componentDidMount() {
	    BackHandler.addEventListener('hardwareBackPress', () => {
	      this.props.navigation.goBack(); // works best when the goBack is async
	      return true;
	    });
	}	

	// componentDidUpdate() {
	// 	 if(this.props.idade.length > 3) {
	// 			alert('Tamanho maximo excedido, digite corretamente');
	// 			this.props.idade = '';
	// 	}		
	// }	

	selecionar(data) {
		let s = this.state;
		s.selecionado = data.key;
		s.storeView = 'flex';
		s.style2 = 'none';
		this.setState(s);
	}

	selecionarLoja() {
		let s = this.state;
		s.animeFav = 'flex';
		this.setState(s);
		this.fecharModal();
	}	

	removeStyle() {
		this.setState({style: 'none'});
	}

	setIdade() {
			let s = this.state;
			s.colorGenero = '#FFF';
			s.bgGenero = '#3e4a59';
			s.idade = 'flex';
			this.setState(s);
	}

	setGenero() {
		if(this.props.name.length > 7){
			let s = this.state;
			s.genero = 'flex';
			s.nome = '#FFF';
			s.bgNome = '#3e4a59';
			s.colorNameFound = '#FFF';
			this.setState(s);
		} else {
		    Alert.alert(
            'Aviso',
            'Digite um nome completo',
            [
                {text: 'Entendi', style: 'cancel'},
            ],
            {cancelable: false}

           )
		}
	}

	setStyle() {
		if(this.props.idade.length > 0) {
			let s = this.state;
			s.colorIdade = '#FFF';
			s.bgIdade = '#3e4a59';
			s.style = 'flex';
			this.setState(s);
		} else {
			alert('Preencha a idade para continuar');
		}
	}

	style() {
		this.setState({style: 'flex'});
	}

	notfound() {
		if(this.state.found == '') {
			this.setState(
					{
					found: 'not', 
					FeedSearch: 'none', 
					storeView: 'none', 
					animeFav: 'none', 
					gameLike: 'none', 
					buttonView: 'none',
					priceView:'none',
					genero:'none',
					idade:'none', 
					style: 'none',
					buttonView: 'none',
					msgSpecial: 0, 
					selectFeed: '', 
					selectStore: '',
					selectAnime: '',
					selectGame: '',
					selectMsg: '',
					selectPrice: ''					
					}
				);						
		}
		
	}

	goCheckoutOther() {
		this.props.navigation.dispatch(StackActions.reset({
			index:0,
			actions:[
				NavigationActions.navigate({routeName:'CheckoutOther', params: {back: true}})
			]
		}));
	}

	goStore(item) {
		this.props.navigation.dispatch(StackActions.reset({
			index:0,
			actions:[
				NavigationActions.navigate({routeName:'Store', params: {'data':item, 'select': () => this.selecionarLoja(item, this.state.presenteado)}})
			]
		}));		
	}

	backProfile() {
		const navigateAction = NavigationActions.navigate({
		  routeName: 'HomeTab',

		  action: NavigationActions.navigate({ routeName: 'Profile' }),
		});

		this.props.navigation.dispatch(navigateAction);			
	}

	busca(event) {
	    this.setState({search: event.nativeEvent.text});
	}

	setText(txt) {
		if(txt == '') {
			this.setState({display: 1})
		} else {
		this.setState({display: 0})
		}
	}	

	giftMe(colorBg, color, choose) {
		
		if(this.state.bgGift1 == '#FFF') {
			let s = this.state;
			s.colorMe = color;
			s.bgGift1 = colorBg;
			s.presenteado = choose;
			s.bgGift2 = '#FFF';
			s.colorOther = '#3e4a59';
			s.selectEstilo = '';
			s.selectAnime = '';
			s.selectGame = '';
			s.selectMsg = '';
			s.selectPrice = '';
			s.storeView = 'none';			
			s.animeFav = 'none';
			s.gameLike = 'none';
			s.style = 'none';
			s.msgSpecial = 0;
			s.buttonView = 'none';
			s.priceView = 'none';	
			s.genero = 'none';
			s.idade = 'none';	
			s.found = '';
			s.displayFound = 'none';			
			this.setState(s);
		}
	}



	giftOther(colorBg, color, choose) {
		if(this.state.bgGift2 == '#FFF') {
			let s = this.state;
			s.colorOther = color;
			s.bgGift2 = colorBg;
			s.presenteado = choose;
			s.FeedSearch = 'flex';
			s.bgGift1 = '#FFF';			
			s.colorMe = '#3e4a59';
			s.selectEstilo = '';
			s.selectAnime = '';
			s.selectGame = '';
			s.selectMsg = '';
			s.selectPrice = '';	
			s.storeView = 'none';
			s.animeFav = 'none';
			s.gameLike = 'none';
			s.msgSpecial = 0;
			s.buttonView = 'none';
			s.priceView = 'none';
			s.genero = 'none';
			s.idade = 'none';	
			s.found = '';	
			s.displayFound = 'flex';						
			this.setState(s);
		}
	}

  _handleSizeChange = event => {
    this.setState({
      width: event.nativeEvent.contentSize.width
    });
  };

    _handleSizeChangeGenero = event => {
    this.setState({
      widthGenero: event.nativeEvent.contentSize.width
    });
  };

    _handleSizeChangeIdade = event => {
    this.setState({
      widthIdade: event.nativeEvent.contentSize.width
    });
  };	

render() {

		return (
			<Container style={styles.container}>
				<Text>Snap&Pay</Text>			
			</Container>	
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#FFFFFF'
	},
	notFound:{
		textDecorationLine: 'underline',
		marginBottom: 20,
		marginTop: 20,
		fontSize: 14,
		fontWeight: 'bold',
		color: '#3e4a59'
	},
	header:{
		paddingLeft: 37,
		paddingTop: 37,
		paddingBottom: 37,
		paddingRight: 37,
		justifyContent: 'center',
		alignItems: 'center'
	},
	item:{
		width: 300
	},
	input:{
		backgroundColor: '#FFFFFF',
		height: 40		
	},	
	backButton:{
		alignSelf: 'flex-start'
	},
	content:{
		margin: 30
	},
	backText:{
		width: 26,
		height: 26,
		margin: 5,
		color: '#CCC'
	},
	question:{
		backgroundColor: '#445ee9',
		borderTopRightRadius: 20,
		borderBottomLeftRadius: 20,
		borderBottomRightRadius: 20,
		marginBottom: 10
	},
	questionText:{
		color: '#FFF',
		fontWeight: '300',
		fontSize: 12,
		padding: 10
	},
	answer:{
		flexDirection: 'row',
		alignSelf: 'flex-end',
		marginBottom: 20
	},
	buttonChoose:{
		borderRadius: 20,
		borderWidth: 1, 
		borderColor: '#3e4a59'
	},
	buttonChoose2:{
		borderTopLeftRadius: 20,
		borderTopRightRadius: 20,
		borderBottomLeftRadius: 20,
		borderWidth: 1, 
		borderColor: '#3e4a59'			
	},
	buttonChooseText:{
		paddingBottom: 10,
		paddingLeft: 13,
		paddingRight: 13,
		paddingTop: 10,
		fontWeight: '300',
		fontSize: 12		
	},
	formArea:{
		flexDirection: 'row',
		flexWrap: 'wrap',
		marginBottom: 10		
	},
	tagItem:{
		width: 99,
		height: 35,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#FFFFFF',
		borderRadius: 20,
		borderWidth: 1,
		borderColor: '#b5bbdf',
		marginRight: 4,
		marginBottom: 10
	},
	tagItemBig:{
		height: 35,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#FFFFFF',
		borderRadius: 20,
		borderWidth: 1,
		borderColor: '#b5bbdf',
		marginRight: 4,
		marginBottom: 10
	},
	tagText:{
		fontWeight: '300',
		fontSize: 12,
		paddingBottom: 10,
		paddingLeft: 13,
		paddingRight: 13,
		paddingTop: 10		

	}, 
	storeContent:{
		flexDirection: 'row' 
	},
	button:{
        width: 332,
        height: 54,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        borderRadius: 6,
        marginBottom: 20,
        marginTop: 20
    },
	buttonBg:{
		backgroundColor: '#ff3c36'
	},
	buttonText:{
		fontWeight: 'bold',
		fontSize: 20,
		color: '#FFF'
	},
	feed:{
		flexDirection: 'row',
		flexWrap: 'wrap',
		backgroundColor: '#fdfdfd',
		justifyContent:'flex-start'
	},
	feedGame:{
		flexDirection: 'row',
		flexWrap: 'wrap',
		backgroundColor: '#fdfdfd',
		justifyContent:'flex-end'		
	},
	headerModal:{
		justifyContent: 'center',
		alignItems: 'center'
	},
	headerModalText:{
		marginTop: 10,
		marginBottom: 20,
		textAlign: 'center',
		fontSize: 24,
		fontWeight: 'bold',
		color: '#3e4a59'
	},
	whoAreUs:{
		justifyContent: 'center',
		alignItems: 'center'
	},
	whoAreUsTitle:{
		color: '#3e4a59',
		fontSize: 14,
		fontWeight: 'bold',
		marginBottom: 5
	},
	whoAreUsBody:{
		fontSize: 12,
		fontWeight: '300',
		color: '#b1b9c2',
		marginBottom: 10
	},
	whoAreUsCity:{
		fontSize: 12,
		color: '#b1b9c2',
		fontWeight: '300',
		marginBottom: 20
	},
	bodyMsg:{
		borderRadius: 10,
		backgroundColor: '#ffeca7',
		color: '#000',
		borderColor: '#FFF',
	    height: 150,
	    justifyContent: "flex-start"
	},
	card:{
		width: 141,
		height: 68,
		alignItems: 'center',
		justifyContent: 'center',
		borderRadius: 20,
		borderWidth: 1,
		marginRight: 10
	},
	cardText:{
		color: '#445ee9',
		fontWeight: 'bold',
		fontSize: 14,
		marginBottom: 5
	},
	cardButton:{
		backgroundColor: '#3e4a59',
		borderRadius: 20		
	},
	cardButtonText:{
		paddingBottom: 4,
		paddingTop: 4,
		paddingRight: 8,
		paddingLeft: 8,
		color: '#FFFFFF',
		fontWeight: '500',
		fontSize: 12
	},
	cardFeed:{
		margin: 10,
		width: '88%',
		height: 160,
		backgroundColor: '#FFF',
		borderRadius: 10,
	    shadowColor: "#000",
	    shadowOffset: { width: 0, height: 2 },
	    shadowOpacity: 1,
	    shadowRadius: 5.5,
	    elevation: 10		
	},
	headerFeed:{
		backgroundColor: '#fdfdfd',
		width: '100%',
		height: 120,
		borderTopLeftRadius: 10,
		borderTopRightRadius: 10,
	},
	imgFeed:{
		borderTopLeftRadius: 10,
		borderTopRightRadius: 10,
		width: 143
	},
	footerFeed:{
		flexDirection: 'row',
		justifyContent: 'space-around',
		alignItems: 'center',
		marginTop: 3
	},
	name:{
		fontWeight: 'bold',
		fontSize: 11
	},
	username:{
		color: '#9898e3',
		fontSize: 11
	},
	storeContainer:{
		width: 192,
		height: 190,
		backgroundColor: '#fdfdfd',
		alignItems: 'center'
	},
	itemFound:{
		flex: 1
	},
	inputFound:{
		fontWeight: '300',
		fontSize: 15,
		alignSelf: 'flex-start',
		borderTopLeftRadius: 20,
		borderTopRightRadius: 20,
		borderBottomLeftRadius: 20,
		borderWidth: 1, 
		borderColor: '#3e4a59'		
	},
  textAreaContainer: {
    padding: 5
  },
  textArea: {
    height: 150,
    justifyContent: "flex-start",
    alignItems: 'flex-start',
    backgroundColor: '#CCC'
  }						
});

const mapStateToProps = (state) => {
	return {
		msg: state.gift.msg,
		name: state.gift.name,
		genero: state.gift.genero,
		idade: state.gift.idade
	}
}

const GiftsConnect = connect(mapStateToProps, { changeMsg, changeName, changeGenero, changeIdade })(Gifts);
export default GiftsConnect;