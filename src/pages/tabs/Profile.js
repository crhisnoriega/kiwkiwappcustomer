import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    ScrollView,
    Button,
    BackHandler,
    AsyncStorage
} from 'react-native';
import { connect } from 'react-redux';
import { logout } from '../../actions/AuthActions';
import { NavigationActions, StackActions } from 'react-navigation';
import { Icon } from 'native-base';
import VerifyProfile from '../../components/VerifyProfile';

class Profile extends Component {

	constructor(props) {
		super(props);

		this.state = {
			opacityMore: 0,
            data: "",
			profile_image: ""
		};

		this.abrirConfig = this.abrirConfig.bind(this);
		this.abrirStore = this.abrirStore.bind(this);
	    this.abrirModal = this.abrirModal.bind(this);
	    this.goCompras = this.goCompras.bind(this);
	    this.goSuporte = this.goSuporte.bind(this);
	    this.doLogout = this.doLogout.bind(this);
	}

    async componentDidMount() {
        this.setState({
			profile_image: await AsyncStorage.getItem("Profile_Image")
		});

	    BackHandler.addEventListener('hardwareBackPress', () => {
	      this.props.navigation.goBack(); // works best when the goBack is async
	      return true;
	    });
	}

	goCompras() {
		this.props.navigation.dispatch(StackActions.reset({
			index:0,
			actions:[
				NavigationActions.navigate({routeName:'Compras'})
			]
		}));
		this.setState({opacityMore:0});
	}

	goSuporte() {
		this.props.navigation.dispatch(StackActions.reset({
			index:0,
			actions:[
				NavigationActions.navigate({routeName:'Suporte'})
			]
		}));
		this.setState({opacityMore:0});
	}

	abrirModal() {
	    let s = this.state;
	    if(this.state.opacityMore == 0) {
	    	s.opacityMore = 1;
	    	this.setState(s);
	    }else {
	    	s.opacityMore = 0;
	    	this.setState(s);
	    }

	  }

	abrirConfig() {
		this.props.navigation.dispatch(StackActions.reset({
			index:0,
			actions:[
				NavigationActions.navigate({routeName:'Config'})
			]
		}));
	}

	doLogout() {
		this.props.logout();
		this.props.navigation.dispatch(StackActions.reset({
			index:0,
			actions:[
				NavigationActions.navigate({routeName:'Welcome'})
			]
		}));
	}

	abrirStore() {
		this.props.navigation.navigate('Gifts', {escolha:'eu'});
	}

	mapInterests() {
        if (this.props.interests !== []) {

        	this.props.interests.map(value => {
                return (
                    <Text style={styles.tagText}>{value}</Text>
                )
            });

            return (
                <Text style={styles.tagText}>Não definido.</Text>
            )
        }
	}

	render() {

		return (
			<Container style={styles.container}>
				<Text>Meu Bar</Text>			
			</Container>	
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#FFFFFF'
	},
	header:{
		justifyContent: 'space-between',
		alignItems: 'center',
		flexDirection: 'row',
		marginBottom: 30,
		width: '100%'
	},
	settings:{
		width: 30,
		height: 30,
		marginRight: 10,
		marginTop: 10,
		color: '#b2b7bd'
	},
	avatarImage:{
		justifyContent: 'center',
		alignItems: 'center'
	},
	avatarName:{
		fontWeight: 'bold',
		color: '#3e4a59',
		fontSize: 20,
		marginTop: 2,
		marginBottom: 2
	},
	avatarUser:{
		color: '#9898e3',
		fontSize: 11
	},
	bio:{
		width: '89%',
		marginTop: 30,
		marginBottom: 30,
		alignSelf: 'center'
	},
	titleBio:{
		color: '#3e4a59',
		fontWeight: 'bold'
	},
	bodyBio:{
		textAlign: 'justify',
		color: '#b1b9c2'
	},
	grid:{
		flexDirection: 'row',
		justifyContent: 'space-around',
		alignItems: 'center',
		marginTop: 20
	},
	colArea:{
		marginBottom: 10
	},
	colTitle:{
		marginBottom: 3,
		color: '#3e4a59',
		fontWeight: 'bold'
	},
	colText:{
		color: '#b1b9c2'
	},
    button:{
        width: 332,
        height: 54,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        borderRadius: 6,
        marginBottom: 20,
        marginTop: 20
    },
	buttonBg:{
		backgroundColor: '#ff3c36'
	},
	buttonText:{
		fontWeight: 'bold',
		fontSize: 20,
		color: '#FFF'
	},
	modalContent:{
		backgroundColor: '#FFF',
		borderRadius: 10,
	    shadowColor: "#000",
	    shadowOffset: { width: 0, height: 2 },
	    shadowOpacity: 1,
	    shadowRadius: 5.5,
	    elevation: 20,
	    width: 110
	},
	modalHeader:{
		backgroundColor: '#FFF'
	},
	col:{
		//marginLeft: 47
	},
	tagText:{
		color: '#FFF',
		backgroundColor: '#445ee9',
		fontSize: 11,
		paddingRight: 7,
		paddingBottom: 3,
		paddingLeft: 7,
		paddingTop: 3,
		borderRadius: 20,
		marginBottom: 5,
		marginRight: 5
	}
});

const mapStateToProps = (state) => {
	return {
		status: state.auth.status,
		usuario: state.MyProfile.login,
		name: state.MyProfile.name,
		qtdRecebidos: state.profile.qtdRecebidos,
		qtdEnviados: state.profile.qtdEnviados,
		bio: state.MyProfile.spirit.biography,
		signo: state.MyProfile.spirit.horoscopo,
		focoatual: state.MyProfile.spirit.focus_activity,
		generosa: state.MyProfile.spirit.genre,
		romantica: state.MyProfile.spirit.romance,
		musical: state.MyProfile.spirit.music_style[0],
		interests: state.MyProfile.spirit.interests
	}
};

const ProfileConnect = connect(mapStateToProps, { logout })(Profile);
export default ProfileConnect;