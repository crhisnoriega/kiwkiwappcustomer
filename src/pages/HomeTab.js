import React, { Component } from "react";
import { View, Image } from "react-native";
import { createBottomTabNavigator } from "react-navigation";
import { Icon } from "native-base";

import Home from "./tabs/Home";
import Gifts from "./tabs/Gifts";
import Basket from "./tabs/Basket";
import Profile from "./tabs/Profile";
import Menu from "./tabs/Menu";
//import TabBarComponent from './TabBarComponent.js'

const HomeTabs = createBottomTabNavigator(
  {
    Home: {
      screen: Home,
      navigationOptions: {
        tabBarIcon: opt => {
          if (opt.focused) {
            return (
              <Image
                source={require("../assets/navbar/inicio.png")}
                style={{ width: 30, height: 30 }}
              />
            );
          } else {
            return (
              <Image
                source={require("../assets/navbar/inicio.png")}
                style={{ width: 30, height: 30 }}
              />
            );
          }
        }
      }
    },

    Gifts: {
      screen: Gifts,
      navigationOptions: {
        tabBarIcon: opt => {
          if (opt.focused) {
            return (
              <Image
                source={require("../assets/navbar/snap_pay.png")}
                style={{ width: 30, height: 30 }}
              />
            );
          } else {
            return (
              <Image
                source={require("../assets/navbar/snap_pay.png")}
                style={{ width: 30, height: 30 }}
              />
            );
          }
        }
      }
    },

    Cesta: {
      screen: Basket,
      navigationOptions: {
        tabBarIcon: opt => {
          if (opt.focused) {
            return (
              <Image
                source={require("../assets/navbar/cesta.png")}
                style={{ width: 30, height: 30 }}
              />
            );
          } else {
            return (
              <Image
                source={require("../assets/navbar/cesta.png")}
                style={{ width: 30, height: 30 }}
              />
            );
          }
        }
      }
    },

    Profile: {
      screen: Menu,
      navigationOptions: {
        tabBarIcon: opt => {
          if (opt.focused) {
            return (
              <Image
                source={require("../assets/navbar/meu_bar.png")}
                style={{ width: 30, height: 30 }}
              />
            );
          } else {
            return (
              <Image
                source={require("../assets/navbar/meu_bar.png")}
                style={{ width: 30, height: 30 }}
              />
            );
          }
        }
      }
    }
  },
  {
    tabBarOptions: {
      showLabel: false,
      style: {
        height: 80
      }
    }
  }
);

export default HomeTabs;
