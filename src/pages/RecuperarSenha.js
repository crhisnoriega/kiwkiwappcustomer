import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, Platform, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import { Item, Label, Input, Form, Content, Icon } from 'native-base';
import { changeEmail, changePassword, submitLogin, showPass } from '../actions/AuthActions';
import { NavigationActions, StackActions } from 'react-navigation';
import BackButton from "../components/BackButton";


export class RecuperarSenha extends Component {

	constructor(props) {
		super(props);

		this.state = {};

		this.verifyStatus = this.verifyStatus.bind(this);
	}

	verifyStatus() {
		if(this.props.status === 1){
		this.props.navigation.dispatch(StackActions.reset({
			index:0,
			actions:[
				NavigationActions.navigate({routeName:'HomeTab'})
			]
		}));			
		}
	}

	componentDidUpdate() {
		this.verifyStatus();
	}	
	
	render() {

		return (
			<ScrollView style={{backgroundColor: '#FFF'}}>
				<View style={styles.container}>
			        <View style={styles.bgImage}>
			          <Image source={require('../assets/auth/wave-grey.png')} style={{width: 278, height: 380 }} />
			        </View>
			        <BackButton return={() => this.props.navigation.goBack()}/>
			        <View style={styles.logoArea}>
			          <View>
			          	<Image source={require('../assets/auth/enigmou-logo-black.png')} style={{width: 206, height: 53 }} />
			          </View>
			        </View>
			        <View>
			        	<Text style={styles.text}>Informe seu endereço de e-mail cadastrado</Text>
			        	<Text style={[styles.text, { textAlign: 'center' }]}>para enviarmos a redefinição de senha.</Text>
			        </View>       
			        <View style={styles.formArea}>			
			          <Item floatingLabel success={this.props.successEmail} error={this.props.errorEmail}>
			            <Label style={styles.formLabel}>Endereço de E-mail</Label>
			            <Input autoCapitalize="none" />
			            <Icon 
			            type="FontAwesome" 
			            name={this.props.email != "" ? "check-circle" : "" } 
			            style={this.props.styleEmail} />
			          </Item>	            
			        </View>
			        <View style={styles.buttonArea}>
			          <View style={styles.buttonContent}>
			        <TouchableOpacity onPress={() => this.props.submitLogin(this.props.email, this.props.password) } style={[styles.button, styles.buttonBg]}>
			          <Text style={styles.buttonText}>Enviar</Text>
			        </TouchableOpacity>	
			      </View>											
			      </View>
			</View>
		</ScrollView>
		);

	}

}

const styles = StyleSheet.create({
	container:{
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: '#FFF'
	},
	logoArea:{
		alignItems: 'center',
		justifyContent: 'center',
		height: 150
	},
	formArea:{
		width: 315,
		height: 171,
		marginTop: 40
	},
	formLabel:{
		fontSize: 10
	},
	formItem:{
		marginTop: 15
	},
	forgetText:{
		marginTop: 10, 
		textAlign: 'right',
		color: '#3b5998'		
	},	
	buttonArea:{
		width: '100%',
		marginTop: 30
	},
	buttonContent:{
		marginTop: 5
	},
	button:{
		width: 315,
		height: 50,
		alignItems: 'center',
		justifyContent: 'center',
		alignSelf: 'center',
		borderRadius: 6,
    	marginBottom: 5
	},
  buttonMedia:{
		width: 315,
		height: 50,
		alignItems: 'center',
		justifyContent: 'center',
		alignSelf: 'center',
		borderRadius: 6,
    	backgroundColor: '#1f86f4'
  },
	buttonText:{
		fontWeight: 'bold',
		fontSize: 15,
		color: '#FFF'
	},
	signupButton:{
		marginTop: 20,
		marginBottom: 20
	},
	signupText:{
		fontWeight: 'bold',
		color: '#8b9dc3',
		textAlign: 'center'
	},
	buttonBg:{
		backgroundColor: '#ff3c36'
	},
	backButton:{
		alignSelf: 'flex-start'
	},
	backText:{
		width: 26,
		height: 26,
		margin: 5,
		color: '#CCC'
	},
    bgImage:{
        position: 'absolute',
        top: 0,
        right: 0
    }
});

const mapStateToProps = (state) => {
	return {
		email:state.auth.email,
		password:state.auth.password,
		successEmail:state.auth.successEmail,
		errorEmail:state.auth.errorEmail,
	    successPass:state.auth.successPass,
	    errorPass:state.auth.errorPass,
	    styleEmail:state.auth.styleEmail,
	    styleSenha:state.auth.styleSenha,
	    PassHide:state.auth.PassHide,
	    status:state.auth.status
	};
};

const RecuperarSenhaConnect = connect(mapStateToProps, {  })(RecuperarSenha);
export default RecuperarSenhaConnect;
