import React, {Component} from 'react';
import {View, StyleSheet, Text, ScrollView, TouchableOpacity, AsyncStorage, Alert} from 'react-native';
import {Icon, Item, Picker, Label} from 'native-base';
import {connect} from 'react-redux';
import BackButton from '../components/BackButton';
import {
    requestMyProfile,
    handlerId,
    handlerLogin,
    handlerPassword,
    handlerName,
    handlerEmail,
    handlerAccount,
    handlerCreatedAt,
    handler_v,
    handlerToken,
    handlerTokenFirebase,
    handlerSpirit
} from '../actions/MyProfileAction';
import * as Http from '../util/api';
import Spinner from 'react-native-spinkit';
import { TagSelect } from 'react-native-tag-select';

class DefinePerso extends Component {

    constructor(props) {
        super(props);

        this.state = {
            selected: "Aquário"
        };

        this.onValueChange = this.onValueChange.bind(this);
    }

    selectInt(item) {
        let array = [];
        for(let i in item){
            array.push(item[i].label); 
        }
        alert(JSON.stringify(array));
    }

    selectFc(item) {
        let array = [];
        for(let i in item){
            array.push(item[i].label); 
        }
        alert(JSON.stringify(array));
    }

    selectEs(item) {
        let array = [];
        for(let i in item){
            array.push(item[i].label); 
        }
        alert(JSON.stringify(array));    
    }

    onValueChange(value, type) {
        switch (type) {
            case "signo":
                this.setState({
                    signo: value
                });
                break;
            case "genero":
                this.setState({
                    generosa: value
                });
                break;
            case "romantica":
                this.setState({
                    romantica: value
                });
                break;
        }

    }

    async saveChanges() {
        this.setState({
            loading: true
        });

        let token = await AsyncStorage.getItem("token");

        let data = JSON.stringify({
            biography: this.props.bio,
            personality: "string",
            horoscopo: this.state.signo,
            generous: this.props.generosa,
            romance: this.props.romantica,
            focus_activity: this.state.focus,
            music_style: this.state.musical,
            interests: this.state.interressesSelects,
            birth: this.props.birth,
            fone1: this.props.fone,
            genre: this.props.genero
        });

        let options = {
            headers: {"token": token}
        };

        await Http.post("/register_profile", data, options, callback => {
            const {success} = callback.data;

            this.setState({
                loading: false
            });

            if (success === true) {
                Alert.alert("Sucesso!", "Dados salvos com sucesso.");
            }
            else {
                Alert.alert("Opa!", "Ocorreu um erro ao salvar os dados.");
            }
        });
    }


    render() {

    const data = [
      { id: 1, label: 'Leitura' },
      { id: 2, label: 'Flores' },
      { id: 3, label: 'Decoração' },
      { id: 4, label: 'Chocolate' },
      { id: 5, label: 'Bebidas' },
    ];

    const data2 = [
      { id: 1, label: 'Trabalho' },
      { id: 2, label: 'Estudo' },
      { id: 3, label: 'Lazer' },
      { id: 4, label: 'Familia' },
      { id: 5, label: 'Espiritual' },
    ];

    const data3 = [
      { id: 1, label: 'Rock' },
      { id: 2, label: 'Samba' },
      { id: 3, label: 'MPB' },
      { id: 4, label: 'Sertanejo' },
      { id: 5, label: 'Funk' },
    ];   

        return (
            <ScrollView>
                <View style={styles.container}>
                    <BackButton return={() => this.props.navigation.goBack()}/>
                    <View style={styles.content}>
                        <View style={styles.formSelect}>
                            <View style={styles.formPicker}>
                                <Label style={[styles.formLabel, {marginLeft: 8}]}>Signo</Label>
                                <Picker
                                    mode="dropdown"
                                    iosIcon={<Icon name="ios-arrow-down-outline"/>}
                                    selectedValue={this.state.signo}
                                    onValueChange={(value) => this.onValueChange(value, "signo")}
                                >
                                    <Picker.Item label="Aquário" value="Aquário"/>
                                    <Picker.Item label="Peixes" value="Peixes"/>
                                    <Picker.Item label="Áries" value="Áries"/>
                                    <Picker.Item label="Touro" value="Touro"/>
                                    <Picker.Item label="Gêmeos" value="Gêmeos"/>
                                    <Picker.Item label="Câncer" value="Câncer"/>
                                    <Picker.Item label="Leão" value="Leão"/>
                                    <Picker.Item label="Virgem" value="Virgem"/>
                                    <Picker.Item label="Libra" value="Libra"/>
                                    <Picker.Item label="Escorpião" value="Escorpião"/>
                                    <Picker.Item label="Sagitário" value="Sagitário"/>
                                    <Picker.Item label="Capricórnio" value="Capricórnio"/>
                                </Picker>
                            </View>
                            <View style={styles.formPicker}>
                                <Label style={[styles.formLabel, {marginLeft: 8}]}>Pessoa Generosa</Label>
                                <Picker
                                    mode="dropdown"
                                    iosIcon={<Icon name="ios-arrow-down-outline"/>}
                                    selectedValue={this.state.generosa}
                                    onValueChange={(value) => this.onValueChange(value, "genero")}
                                >
                                    <Picker.Item label="Exagerado" value="Exagerado"/>
                                    <Picker.Item label="Muito" value="Muito"/>
                                    <Picker.Item label="Quase nada" value="Quase nada"/>
                                    <Picker.Item label="Inexistente" value="Inexistente"/>
                                </Picker>
                            </View>
                            <View style={styles.formPicker}>
                                <Label style={[styles.formLabel, {marginLeft: 8}]}>Pessoa romântica</Label>
                                <Picker
                                    mode="dropdown"
                                    iosIcon={<Icon name="ios-arrow-down-outline"/>}
                                    selectedValue={this.state.romantica}
                                    onValueChange={(value) => this.onValueChange(value, "romantica")}
                                >
                                    <Picker.Item label="Exagerado" value="Exagerado"/>
                                    <Picker.Item label="Muito" value="Muito"/>
                                    <Picker.Item label="Quase nada" value="Quase nada"/>
                                    <Picker.Item label="Inexistente" value="Inexistente"/>
                                </Picker>
                            </View>
                        </View>
                        <View>
                            <Text style={styles.formTag}>Foco Atual</Text>
                            <View style={{marginTop: 10}}>
                            <TagSelect
                              data={data2}
                              max={1}
                              ref={(tag1) => {
                                this.tag1 = tag1;
                              }}
                              onMaxError={() => {
                                Alert.alert('Ops', 'Maximo uma escolha');
                              }}                              
                              itemStyle={styles.tagItem}
                              itemLabelStyle={styles.label}
                              itemStyleSelected={styles.itemSelected}
                              itemLabelStyleSelected={styles.labelSelected}                              
                            />
                            </View>
                        </View>
                        <View>
                            <Text style={styles.formTag}>Estilo Musical</Text>
                            <View style={{marginTop: 10}}>
                            <TagSelect
                              data={data3}
                              max={1}
                              ref={(tag2) => {
                                this.tag2 = tag2;
                              }}
                              onMaxError={() => {
                                Alert.alert('Ops', 'Maximo uma escolha');
                              }}                              
                              itemStyle={styles.tagItem}
                              itemLabelStyle={styles.label}
                              itemStyleSelected={styles.itemSelected}
                              itemLabelStyleSelected={styles.labelSelected}                              
                            />
                            </View>
                        </View>
                        <View>
                            <Text style={styles.formTag}>Interesses</Text>
                            <View style={{marginTop: 10}}>
                            <TagSelect
                              data={data}
                              max={3}
                              ref={(tag) => {
                                this.tag = tag;
                              }}
                              onMaxError={() => {
                                Alert.alert('Ops', 'Maximo três escolhas');
                              }}                              
                              itemStyle={styles.tagItem}
                              itemLabelStyle={styles.label}
                              itemStyleSelected={styles.itemSelected}
                              itemLabelStyleSelected={styles.labelSelected}                              
                            />
                            </View>
                        </View>                       
                        <TouchableOpacity onPress={() => {
                            //this.selectInt(this.tag.itemsSelected);
                            //this.selectFc(this.tag1.itemsSelected);
                            //this.selectEs(this.tag2.itemsSelected);
                        }} style={[styles.button, styles.buttonBg]}>
                            <Text style={styles.buttonText}>Salvar</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFFFFF'
    },
    backButton: {
        alignSelf: 'flex-start'
    },
    backText: {
        width: 26,
        height: 26,
        margin: 5,
        color: '#CCC'
    },
    content: {
        margin: 30
    },
    headerText: {
        marginBottom: 10,
        fontSize: 14,
        color: '#656d78',
        fontWeight: 'bold'
    },
    formLabel: {
        fontSize: 12,
        fontWeight: 'bold'
    },
    formPicker: {
        marginTop: 10,
        marginLeft: -5,
        borderBottomWidth: 1,
        borderBottomColor: '#CCC'
    },
    formTag: {
        marginTop: 10
    },
    formSelect: {
        marginLeft: -5
    },
    formArea: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginTop: 10
    },
    tagItem: {
        height: 25,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFFFFF',
        borderRadius: 20,
        borderWidth: 1,
        borderColor: '#b5bbdf'
    },
    tagText: {
        fontWeight: 'bold'
    },
    button: {
        width: 332,
        height: 54,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        borderRadius: 6,
        marginBottom: 20,
        marginTop: 20
    },
    buttonBg: {
        backgroundColor: '#ff3c36'
    },
    buttonText: {
        fontWeight: 'bold',
        fontSize: 20,
        color: '#FFF'
    },
      item: {
        borderWidth: 1,
        borderColor: '#333',    
        backgroundColor: '#FFF',
      },
      label: {
        color: '#333',
        fontWeight: 'bold',
        fontSize: 15
      },
      itemSelected: {
        backgroundColor: '#b5bbdf',
        borderWidth: 0
      },
      labelSelected: {
        color: '#FFF',
        fontWeight: 'bold',
        fontSize: 15        
      }    
});

const mapStateToProps = (state) => {
    return {
        focoatual: state.MyProfile.spirit.focus_activity,
        generosa: state.MyProfile.spirit.generous,
        romantica: state.MyProfile.spirit.romance,
        musical: state.MyProfile.spirit.music_style[0],
        interests: state.MyProfile.spirit.interests,
        horoscopo: state.MyProfile.spirit.horoscopo,
    }
};

const DefinePersoConnect = connect(mapStateToProps, {
    requestMyProfile,
    handlerId,
    handlerLogin,
    handlerPassword,
    handlerName,
    handlerEmail,
    handlerAccount,
    handlerCreatedAt,
    handler_v,
    handlerToken,
    handlerTokenFirebase,
    handlerSpirit
})(DefinePerso);
export default DefinePersoConnect;