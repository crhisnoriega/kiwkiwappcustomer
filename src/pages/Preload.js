import React, { Component } from "react";
import { View, Image } from "react-native";
import { NavigationActions, StackActions } from "react-navigation";
import { connect } from "react-redux";
import { checkLogin } from "../actions/AuthActions";

const styles = {
  viewStyles: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#FA807C"
  },
  textStyles: {
    color: "white",
    fontSize: 40,
    fontWeight: "bold"
  },
  bg: {
    width:"100%",
    height: 100
  }
};

class Preload extends Component {
  constructor(props) {
    super(props);

    this.state = {};
    this.verifyStatus = this.verifyStatus.bind(this);

    window.uidClient = this.props._id;
  }

  verifyStatus() {
    switch (this.props.status) {
      case 1:
        this.props.navigation.dispatch(
          StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: "HomeTab" })]
          })
        );

        break;
      case 2:
        this.props.navigation.dispatch(
          StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: "Welcome" })]
          })
        );

        break;
    }
  }

  performTimeConsumingTask = async() => {
    return new Promise(resolve =>
      setTimeout(() => {
        resolve("result");
      }, 5000)
    );
  };

  async componentDidMount() {
    this.props.checkLogin();

    // Preload data from an external API
    // Preload data using AsyncStorage
    const data = await this.performTimeConsumingTask();

    if (data !== null) {
      this.props.navigation.dispatch(
        StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: "Login" })]
        })
      );
    }
  }

  componentDidUpdate() {
    // this.verifyStatus();
  }

  render() {
    return (
      <View style={styles.viewStyles}>
        <Image source={require("../assets/kiwkiw.1.png")}  />
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    status: state.auth.status,
    _id: state.MyProfile._id
  };
};

const PreloadConnect = connect(
  mapStateToProps, { checkLogin }
)(Preload);
export default PreloadConnect;
