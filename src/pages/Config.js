import React, {Component} from 'react';
import {Text, View, StyleSheet,BackHandler, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import AbaPerfil from './abasConfig/AbaPerfil';
import AbaEndereco from './abasConfig/AbaEndereco';
import DefinePerso from './DefinePerso';
import { Container, Header, Content, Tab, Tabs } from 'native-base';
import {NavigationActions} from 'react-navigation';

class Config extends Component {

    constructor(props) {
        super(props);

        this.state = {
            selected: "key0",
            foto: null
        };

        this.backProfile = this.backProfile.bind(this);
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.backProfile);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.backProfile);
    }


    backProfile() {
        const navigateAction = NavigationActions.navigate({
            routeName: 'HomeTab',

            action: NavigationActions.navigate({routeName: 'Profile'}),
        });

        this.props.navigation.dispatch(navigateAction);
        return true;
    }    



    render() {       
        return (                      
             <Container>
                    <View style={{alignSelf: 'center', marginTop: 20, marginBottom: 20}}>
                            <Text style={[styles.headerText, {textAlign: 'center'}]}>Complete o seu perfil</Text>
                            <Text style={styles.headerText}>para receber presentes!</Text>
                    </View>
                    <Tabs tabBarUnderlineStyle={{backgroundColor: '#0d63f2'}} style={{borderWidth: 0}}>
                      <Tab tabStyle={{backgroundColor: '#FFF'}} activeTabStyle={{backgroundColor: '#FFF'}} textStyle={{color: '#a6a6a6'}} activeTextStyle={{color: '#0d63f2'}} heading="Perfil">
                        <AbaPerfil />
                      </Tab>
                      <Tab tabStyle={{backgroundColor: '#FFF'}} activeTabStyle={{backgroundColor: '#FFF'}} textStyle={{color: '#a6a6a6'}} activeTextStyle={{color: '#0d63f2'}} heading="Endereço">
                        <AbaEndereco />
                      </Tab>
                      <Tab tabStyle={{backgroundColor: '#FFF'}} activeTabStyle={{backgroundColor: '#FFF'}} textStyle={{color: '#a6a6a6'}} activeTextStyle={{color: '#0d63f2'}} heading="Personalidade">
                        <DefinePerso />
                      </Tab>
                    </Tabs>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 40
  },
    headerText: {
        color: '#4b5461',
        marginBottom: 5,
        fontWeight: 'bold'
    }  
});

const mapStateToProps = (state) => {
    return {

    }
}

const ConfigConnect = connect(mapStateToProps, { })(Config);
export default ConfigConnect;	