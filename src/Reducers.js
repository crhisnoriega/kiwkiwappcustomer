import { combineReducers } from 'redux';
import AuthReducer from './reducers/AuthReducer';
import RegisterReducer from './reducers/RegisterReducer';
import ProfileReducer from './reducers/ProfileReducer';
import CheckoutReducer from './reducers/CheckoutReducer';
import ComprasReducer from './reducers/ComprasReducer';
import ConfigReducer from './reducers/ConfigReducer';
import GiftReducer from './reducers/GiftReducer';
import ChatReducer from './reducers/ChatReducer';
import MyProfileReducer from './reducers/MyProfileReducer';
import HomeReducer from './reducers/HomeReducer';

const Reducers = combineReducers({
	auth: AuthReducer,
	register: RegisterReducer,
	profile: ProfileReducer,
	checkout: CheckoutReducer,
	compras: ComprasReducer,
	config: ConfigReducer,
	gift: GiftReducer,
	chat: ChatReducer,
	MyProfile: MyProfileReducer,
	Home: HomeReducer
});

export default Reducers;