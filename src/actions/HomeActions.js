import * as Http from '../util/api';
import { AsyncStorage } from 'react-native';

export const ADD_PROFILES = (result) => {
    return async (dispatch) => {
        const data = JSON.stringify({
            "filter": "",
            "justfavs": true,
            "justcompletes": false
        });

        let token = await AsyncStorage.getItem("token");

        let options = {
            headers: { "token" : token }
        };

        await Http.post("/profiles", data, options, callback => {
            result(callback.data);

            dispatch({
                type: "ADD_PROFILES",
                payload: null
            });
        });
    }
};

export const handlerData = (data) => {
    return {
        type: "handlerData",
        payload: {
            data
        }
    }
};

// export const searchProfile = (data, result) => {
//     return async (dispatch) => {
//         let token = await AsyncStorage.getItem("token");

//         let data = JSON.stringify({
//             "filter": data,
//             "justfavs": false,
//             "justcompletes": false
//         });

//         let options = {
//             headers: { 
//                 "token" : token
//              }
//         };

//         await Http.post("/profiles", data, options, callback => {
//             result(callback);

//            // alert(JSON.stringify(callback));

//         });
//         // alert(data);    
//     }
// };

export const searchProfile = (login) => {
    return async (dispatch) => {
        let token = await AsyncStorage.getItem("token");

        let data = JSON.stringify({
            "filter": login,
            "justfavs": false,
            "justcompletes": false
        });

        let options = {
            headers: { "token" : token }
        };

        let data_image = JSON.stringify({
            name: await AsyncStorage.getItem("Username")
        });
            await Http.post("/profiles", data, options, callback => {
                // result(callback)


                dispatch({
                    type: "handlerData",
                    payload: {
                        list:callback
                    }
                });
            });
    }
};