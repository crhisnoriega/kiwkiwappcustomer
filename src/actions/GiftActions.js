import * as Http from '../util/api';

export const changeMsg = (msg) => {
    return {
        type: 'changeMsg',
        payload: {
            msg: msg
        }
    }
};

export const changeName = (name) => {
    return {
        type: 'changeName',
        payload: {
            name: name
        }
    }
};

export const changeGenero = (genero) => {
    return {
        type: 'changeGenero',
        payload: {
            genero: genero
        }
    }
};

export const changeIdade = (idade) => {
    return {
        type: 'changeIdade',
        payload: {
            idade: idade
        }
    }
};