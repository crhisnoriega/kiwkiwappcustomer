import validator from 'validator';
import * as Http from '../util/api';
import {AsyncStorage, Platform, Alert } from 'react-native';

export const HandlerName = (name) => {
    return {
        type: "HandlerName",
        payload: {
            name
        }
    }
};

export const HandlerEmail = (email) => {
        return {
            type: "HandlerEmail",
            payload: {
                email:email
            }
        }    
};

export const submitRegister = (name, email, username, password, fireToken) => {
    return async (dispatch) => {
        let data = JSON.stringify({
            login: username,
            name: name,
            password: password,
            email: email
        });

        let options = {
            headers: {"token": "$2a$04$q8Fa0cgSFleixq0XMlmEyuC0VuIgI10PhJF1zifRw3M9Q3pGY.oSa"}
        };

        let login_data = JSON.stringify({
            login: username,
            password: password,
            devicetype: Platform.OS,
            token_firebase: 'dsadsadasdas'
        });

       // alert( JSON.stringify(token_firebase));
        //Todo: Não está fazendo login
        await Http.post("/register", data, options, async callback => {
            const { success, error } = callback.data;

            // alert(JSON.stringify(callback.data))

            if (success === true) {
                await Http.post("/login", login_data, options, result => {
                    const { success, token } = result.data;

                    AsyncStorage.setItem("token", token);
                    AsyncStorage.setItem("@UserData", data);
                    AsyncStorage.setItem("Username", username);

                    if (success === true) {
                        dispatch({
                            type:'changeStatus',
                            payload:{
                                status:1
                            }
                        });
                    }
                    else {
                        alert('Usuario não cadastrado');
                    }
                });
            }
            else {
                Alert.alert(
                    'Aviso',
                    error,
                    [
                        {text: 'Entendi', style: 'cancel'},
                    ],
                    { cancelable: false }
                )
            }
        });
    }
};

export const HandlerUsername = (username) => {
    return {
        type: "HandlerUsername",
        payload: {
            username
        }
    }
};

export const HandlerPassword = (password) => {
    return {
        type: "HandlerPassword",
        payload: {
            password
        }
    }
};