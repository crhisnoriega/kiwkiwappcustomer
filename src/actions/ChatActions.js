import firebase from '../util/FirebaseApi';

export const criarChat = (userUid1, userUid2) => {
	return (dispatch) => {
		let name1 = 'Maria';
		let name2 = 'Aline';
		//Criando o proprio chat
		let newChat = firebase.database().ref('chats').push();
		newChat.child('members').child(userUid1).set({
			id:userUid1
		});
		newChat.child('members').child(userUid2).set({
			id:userUid2
		});		

		const monthNames = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho",
		  "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"
		];		

		let currentDate = '';
		let cDate = new Date();	 

		currentDate = cDate.getDate()+' de '+monthNames[cDate.getMonth()]+' de '+cDate.getFullYear();	

		// Associando ao envolvidos
		let chatId = newChat.key;

			firebase.database().ref('users').child(userUid1).child('chats')
				.child(chatId).set({
				id:chatId,
				title:name1,
				other:userUid2,
				otherName:name2,
				date:currentDate
			});			


			firebase.database().ref('users').child(userUid2).child('chats')
				.child(chatId).set({
				id:chatId,
				title:name2,
				other:userUid1,
				otherName:name1,
				date:currentDate
			});						
	};
};

export const getChatList = (userUid, callback) => {
	return (dispatch) => {

		firebase.database().ref('users').child(userUid).child('chats').on('value', (snapshot) => {
			let chats = [];

			snapshot.forEach((childItem) => {
			  chats.push({
			  	key:childItem.key,
			  	id:childItem.val().id,
			  	date:childItem.val().date,
			  	title:childItem.val().title,
			  	other:childItem.val().other,
			  	otherName:childItem.val().otherName
			  });
			});

			callback();

			dispatch({
				type:'setChatList',
				payload:{
					chats:chats
				}
			});

		});

	};
};

export const sendImage = (blob, progressCallback, successCallback) => {
	return (dispatch) => {

		let tmpKey = firebase.database().ref('chats').push().key;

		let fbimage = firebase.storage().ref('images').child(tmpKey);

		fbimage.put(blob, {contentType:'image/jpeg'})
				.on('state_changed', 
				progressCallback,
				(error) => {
					alert(error.code);
				},
				() => {
					fbimage.getDownloadURL().then((url) => {
						successCallback(url);
					});
					
				})
	}
};

export const sendMessage = (msgType, msgContent, author, activeChat) => {
	return (dispatch) => {
		let msgid = firebase.database().ref('chats').child(activeChat).child('messages').push();

		let currentDate = '';
		let cDate = new Date();	 

		currentDate = cDate.getFullYear()+'-'+(cDate.getMonth()+1)+'-'+cDate.getDate();
		currentDate	+= ' ';
		currentDate += cDate.getHours()+':'+cDate.getMinutes()+':'+cDate.getSeconds() + cDate.getMilliseconds();	

		switch(msgType) {
			case 'text' :
			msgid.set({
				msgType:'text',
				m:msgContent,
				uid:author,
				name: 'Aline Silveira',
				date:currentDate
			});
			break;
			case 'image':		
			msgid.set({
				msgType:'image',				
				imgSource:msgContent,
				uid:author,
				name: 'Aline Silveira',
				date:currentDate
			});				
			break;
		}
	};
};

export const monitorChat = (activeChat) => {
	return(dispatch) => {
		firebase.database().ref('chats').child(activeChat).child('messages').orderByChild('date').on('value', (snapshot) => { 
			let arrayMsg = [];

			snapshot.forEach((childItem) => {
				switch(childItem.val().msgType) {
					case 'text':
					  arrayMsg.push({
					  	key:childItem.key,
					  	date:childItem.val().date,
					  	msgType:'text',
					  	m:childItem.val().m,
					  	uid:childItem.val().uid
					  });
						break;
					case 'image':
					  arrayMsg.push({
					  	key:childItem.key,
					  	date:childItem.val().date,
					  	msgType:'image',
					  	imgSource:childItem.val().imgSource,
					  	uid:childItem.val().uid
					  });
						break;

				}

			});

			dispatch({
				type:'setActiveChatMessage',
				payload:{
					activeChatMessages:arrayMsg
				}
			});
		});
	};
};

export const monitorChatOff = (activeChat) => {
	return(dispatch) => {
		firebase.database().ref('chats').child(activeChat).child('messages').off();
	};
};