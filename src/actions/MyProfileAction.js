import * as Http from '../util/api';
import { AsyncStorage } from 'react-native';

export const handlerId = (id) => {
    return {
        type: "handlerId",
        payload: {
            id:id
        }
    }
};

export const handlerGenero = (genero) => {
    return {
        type: "handlerId",
        payload: {
            genero:genero
        }
    }
};

export const handlerLogin = (login) => {
    return {
        type: "handlerLogin",
        payload: {
            login:login
        }
    }
};

export const handlerUser = (user) => {
    return {
        type: "handlerUser",
        payload: {
            user:user
        }
    }
};

export const handlerPassword = (password) => {
    return {
        type: "handlerPassword",
        payload: {
            password:password
        }
    }
};

export const handlerName = (name) => {
    return {
        type: "handlerName",
        payload: {
            name:name
        }
    }
};

export const handlerEmail = (email) => {
    return {
        type: "handlerEmail",
        payload: {
            email:email
        }
    }
};

export const handlerBio = (bio) => {
    return {
        type: "handlerBio",
        payload: {
            bio
        }
    }
}

export const handlerPerso = (personality) => {
    return {
        type: "handlerPerso",
        payload: {
            personality
        }
    }
}

export const handlerHoro = (horoscopo) => {
    return {
        type: "handlerHoro",
        payload: {
            horoscopo
        }
    }
}

export const handlerRoma = (romance) => {
    return {
        type: "handlerRoma",
        payload: {
            romance
        }
    }
}

export const handlerGene = (generous) => {
    return {
        type: "handlerHoro",
        payload: {
            generous
        }
    }
}

export const handlerGenre = (genre) => {
    return {
        type: "handlerGenre",
        payload: {
            genre
        }
    }
}

export const handlerBirth = (birth) => {
    return {
        type: "handlerBirth",
        payload: {
            birth
        }
    }
}

export const handlerFone = (fone1) => {
    return {
        type: "handlerFone",
        payload: {
            fone1
        }
    }
}

export const handlerAccount = (account_id) => {
    return {
        type: "handlerAccount",
        payload: {
            account_id:account_id
        }
    }
};

export const handlerCreatedAt = (createdAt) => {
    return {
        type: "handlerCreatedAt",
        payload: {
            createdAt:createdAt
        }
    }
};

export const handler_v = (__v) => {
    return {
        type: "handler_v",
        payload: {
            __v:__v
        }
    }
};

export const handlerToken = (token) => {
    return {
        type: "handlerToken",
        payload: {
            token:token
        }
    }
};

export const handlerTokenFirebase = (token_firebase) => {
    return {
        type: "handlerTokenFirebase",
        payload: {
            token_firebase:token_firebase
        }
    }
};

export const handlerSpirit = (spirit) => {
    return {
        type: "handlerSpirit",
        payload: {
            spirit:spirit
        }
    }
};

export const requestMyProfile = (login, result) => {
    return async (dispatch) => {
        let token = await AsyncStorage.getItem("token");

        let data = JSON.stringify({
            "filter": login,
            "justfavs": false,
            "justcompletes": false
        });

        let options = {
            headers: { "token" : token }
        };

        let data_image = JSON.stringify({
            name: await AsyncStorage.getItem("Username")
        });

        await Promise.all([
            await Http.post("/profiles", data, options, callback => {
                result(callback);
                

                dispatch({
                    type: "requestMyProfile",
                    payload: null
                });
            }),
            await Http.post("/retrieve_image", data_image, options, callback => {
                const Profile_Image = callback.data[0].content;

                AsyncStorage.setItem("Profile_Image", Profile_Image);
            })
        ])
    }
};