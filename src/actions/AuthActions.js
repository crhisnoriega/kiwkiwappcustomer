import validator from 'validator';
import * as Http from '../util/api';
import { AsyncStorage, Platform, Alert } from 'react-native';

export const changeEmail = (email) => {
    return {
        type: 'changeEmail',
        payload: {
            email: email
        }
    }
}

export const changePassword = (password) => {
    return {
        type: 'changePassword',
        payload: {
            password: password
        }
    }
}

export const checkLogin = () => {
    return (dispatch) => {
        AsyncStorage.getItem('token')
            .then((data) => {
                if (data != null && data != '') {
                    dispatch({
                        type: 'changeStatus',
                        payload: {
                            status: 1
                        }
                    });

                }
                else {
                    dispatch({
                        type: 'changeStatus',
                        payload: {
                            status: 2
                        }
                    });
                }
            })
            .catch((error) => {
                dispatch({
                    type: 'changeStatus',
                    payload: {
                        status: 2
                    }
                });
            })
    }
};

export const onblurEmail = (email) => {
    return (dispatch) => {

    }
}

export const showPass = (PassHide) => {
    return (dispatch) => {
        if (PassHide) {
            PassHide = false;
        }
        else {
            PassHide = true;
        }

        dispatch({
            type: 'PassHide',
            payload: {
                PassHide: PassHide
            }
        });

    }
};

//Todo: uncomment this code when api starts again
export const submitLogin = (email, password, fireToken, result) => {
    return async(dispatch) => {
        // dispatch({
        //     type: "changeStatus",
        //     payload: {
        //         status: 1
        //     }
        // });
        if (validator.isEmail(email) && email != "") {
            Alert.alert(
                'Aviso',
                'Login apenas com nome do usuario (em processo com o email', [
                    { text: 'Entendi', style: 'cancel' },
                ], { cancelable: false }
            )
        }
        else {
            if (email.includes('@')) {
                Alert.alert(
                    'Aviso',
                    'Nome de usuário inválido.', [
                        { text: 'Entendi', style: 'cancel' },
                    ], { cancelable: false }
                )
            }
            else if (!email.includes('@') && email.length < 3) {
                alert("E-mail inválido.");
            }
            else {

                let data = JSON.stringify({
                    login: email,
                    password: password,
                    devicetype: Platform.OS,
                    token_firebase: fireToken
                });

                dispatch({
                    type: "changeStatus",
                    payload: {
                        status: 1
                    }
                });
            }
        }
    }
};

export const logout = () => {
    return (dispatch) => {
        AsyncStorage.removeItem("token");

        dispatch({
            type: 'changeStatus',
            payload: {
                status: 2
            }
        });

    }
};
