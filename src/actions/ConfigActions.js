import * as Http from '../util/api';
import { AsyncStorage } from 'react-native';

export const changeName = (name) => {
    return {
        type: 'changeName',
        payload: {
            name: name
        }
    }
};

export const changeEmail = (email) => {
    return {
        type: 'changeEmail',
        payload: {
            email: email
        }
    }
};

export const changeUser = (user) => {
    return {
        type: 'changeUser',
        payload: {
            user: user
        }
    }
};

export const changeAniver = (aniversario) => {
    return {
        type: 'changeAniver',
        payload: {
            aniversario: aniversario
        }
    }
};

export const changeBio = (bio) => {
    return {
        type: 'changeBio',
        payload: {
            bio: bio
        }
    }
};

export const changePass = (password) => {
    return {
        type: 'changePass',
        payload: {
            password: password
        }
    }
};

export const changeTel = (tel) => {
    return {
        type: 'changeTel',
        payload: {
            tel: tel
        }
    }
};

export const changeGenero = (genero) => {
    return {
        type: 'changeGenero',
        payload: {
            genero: genero
        }
    }
};

export const changeCep = (cep) => {
    return {
        type: 'changeCep',
        payload: {
            cep: cep
        }
    }
};

export const changeRua = (rua) => {
    return {
        type: 'changeRua',
        payload: {
            rua: rua
        }
    }
};

export const changeNumero = (numero) => {
    return {
        type: 'changeNumero',
        payload: {
            numero: numero
        }
    }
};

export const changeComplemento = (complemento) => {
    return {
        type: 'changeComplemento',
        payload: {
            complemento: complemento
        }
    }
};

export const changeBairro = (bairro) => {
    return {
        type: 'changeBairro',
        payload: {
            bairro: bairro
        }
    }
};

export const changeCidade = (cidade) => {
    return {
        type: 'changeCidade',
        payload: {
            cidade: cidade
        }
    }
};

export const changeUf = (uf) => {
    return {
        type: 'changeUf',
        payload: {
            uf: uf
        }
    }
};

export const saveImage = (username, base64, result) => {
    return async (dispatch) => {
        let token = await AsyncStorage.getItem("token");

        let options = {
            headers: { "token" : token }
        };

        let data = JSON.stringify({
            name: username,
            image: base64
        });

        await Http.post("/save_image", data, options, callback => {
            result(callback);

            return dispatch({
                type: "saveImage",
                payload: null
            });
        });
    }
};

export const savePerfil = (genero, birth, result) => {
    return async (dispatch) => {
        let token = await AsyncStorage.getItem("token");

        let options = {
            headers: { "token" : token }
        };

        let data = JSON.stringify({
            genre: genero,
            birth: birth
        });

        await Http.post("/register_profile", data, options, callback => {
            result(callback);

            alert(JSON.stringify(callback.data));

            dispatch({
                type: "savePerfil",
                payload: {
                    genero: genero,
                    birth: birth
                }
            });
        });
    }
};