import React, { Component } from 'react';
import { createStackNavigator } from 'react-navigation';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';

import Reducers from './src/Reducers';
import Welcome from './src/pages/Welcome';
import Login from './src/pages/Login';
import Register from './src/pages/Register';
import Preload from './src/pages/Preload';
import HomeTab from './src/pages/HomeTab';
import Config from './src/pages/Config';
import ProfileUser from './src/pages/ProfileUser';
import DefinePerso from './src/pages/DefinePerso';
import Checkout from './src/pages/Checkout';
import CheckoutOther from './src/pages/CheckoutOther';
import Compras from './src/pages/Compras';
import Suporte from './src/pages/Suporte';
import RecuperarSenha from './src/pages/RecuperarSenha';
import Chat from './src/pages/Chat';
import QrCode from './src/pages/QrCode';
import ViewMessage from './src/pages/ViewMessage';
import firebase from 'react-native-firebase';

global.Symbol = require('core-js/es6/symbol');
require('core-js/fn/symbol/iterator');
require('core-js/fn/map');
require('core-js/fn/set');
require('core-js/fn/array/find');

console.disableYellowBox = true;

let store = createStore(Reducers, applyMiddleware(ReduxThunk));

const AppNavigator = createStackNavigator({
  Preload:{
    screen: Preload
  },
  Welcome: {
    screen: Welcome
  },
  Login: {
    screen: Login
  },
  Register: {
    screen: Register
  },
  HomeTab:{
    screen: HomeTab
  },
  Config:{
    screen: Config
  },
  ProfileUser:{
    screen: ProfileUser
  },
  DefinePerso:{
    screen: DefinePerso
  },
  Checkout:{
    screen: Checkout
  },
  CheckoutOther:{
    screen: CheckoutOther
  },
  Compras:{
    screen: Compras
  },
  Suporte:{
    screen: Suporte
  },
  RecuperarSenha:{
    screen: RecuperarSenha
  },
  Chat:{
    screen: Chat
  },
  QrCode:{
    screen: QrCode
  },
  ViewMessage:{
    screen: ViewMessage
  }
},{
  navigationOptions:{
    header:null
  }
});

export default class App extends Component {
  async componentDidMount() {      
        const notificationOpen: NotificationOpen = await firebase.notifications().getInitialNotification();
        if (notificationOpen) {
            const action = notificationOpen.action;
            const notification: Notification = notificationOpen.notification;
        } 
        const channel = new firebase.notifications.Android.Channel('test-channel', 'Test Channel', firebase.notifications.Android.Importance.Max)
                .setDescription('My apps test channel');
// Create the channel
        firebase.notifications().android.createChannel(channel);
        this.notificationDisplayedListener = firebase.notifications().onNotificationDisplayed((notification: Notification) => {
            // Process your notification as required
            // ANDROID: Remote notifications do not contain the channel ID. You will have to specify this manually if you'd like to re-display the notification.
        });
        this.notificationListener = firebase.notifications().onNotification((notification: Notification) => {
            // Process your notification as required
            notification
                .android.setChannelId('test-channel')
                .android.setSmallIcon('ic_launcher');
            firebase.notifications()
                .displayNotification(notification);
            
        });
        this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen: NotificationOpen) => {
            // Get the action triggered by the notification being opened
            const action = notificationOpen.action;
            // Get information about the notification that was opened
            const notification: Notification = notificationOpen.notification;
            var seen = [];
            // alert(JSON.stringify(notification.data, function(key, val) {
            //     if (val != null && typeof val == "object") {
            //         if (seen.indexOf(val) >= 0) {
            //             return;
            //         }
            //         seen.push(val);
            //     }
            //     return val;
            // }));            
            firebase.notifications().removeDeliveredNotification(notification.notificationId);
            
        });
    }
    componentWillUnmount() {
        this.notificationDisplayedListener();
        this.notificationListener();
        this.notificationOpenedListener();
    }

  render() {
    return (
      <Provider store={store}>
        <AppNavigator />
      </Provider>
    );
  }
}